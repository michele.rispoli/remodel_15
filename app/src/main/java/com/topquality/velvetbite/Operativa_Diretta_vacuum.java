package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import android_serialport_api.SerialPort;

import static android.media.MediaExtractor.MetricsConstants.FORMAT;

public class Operativa_Diretta_vacuum extends Activity {

    /*******************************************SERIALE**********************************/

    private static final String TAG = "MainActivity2";
    String msg = null;
    LogFile log = new LogFile();
    String tolog;
    String nome_trattamento_db;

    Button min1;
    Button add1;
    Button min_i;
    Button min_p;
    Button min_d;
    Button add_i;
    Button add_p;
    Button add_d;

    TextView text1;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Operativa_Diretta_vacuum.ReadThread mReadThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();

    private boolean m_bShowDateType = false;

    private SeekBar seekBar;
    private TextView textView;
    private TextView textView_i;
    private TextView textView_p;
    private TextView textView_d;
    public int tempo = 0;
    public int impulso = 1;
    public int pausa = 1;
    public int depressione = 0;
    int tempo_sel = 0;
    int k = 1;
    String hex;

    Operativa_Diretta_vacuum.MyCount counter;
    Long s1;

    String start_trat = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";
    String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";
    String pausa_tosend = "3E-FE-01-58-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-F4-CC-3C";
    String riprendi = "3E-FE-01-59-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-C5-3C-3C";

    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Operativa_Diretta_vacuum.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    long startTime;
    long endTime;
    long tempoTrattamento;
    long tempocontatori;
    long update_contatori;
    TextView mmHg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.op_dir_vacuum);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0) {
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        } else if (reseller == 1) {
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }

        SendMsg("3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C");

        /*contatori utilizzo macchina*/
        final SharedPreferences res = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor resEditor = res.edit();
        startTime = System.currentTimeMillis();
        resEditor.putLong("Start time", startTime);
        /*contatori utilizzo macchina*/

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        pacchetto_da_inviare("14", "04-00", 0); //mod vacuumrea

        TextView titolo = findViewById(R.id.textView4);
        titolo.setText(prefs.getString("Nome Trattamento", getString(R.string.imm_dir)));

        textView_i = findViewById(R.id.textView_i);
        textView_p = findViewById(R.id.textView_p);
        textView_d = findViewById(R.id.textView_d);

        min_i = (Button) findViewById(R.id.min_i);
        min_p = (Button) findViewById(R.id.min_p);
        min_d = (Button) findViewById(R.id.min_d);
        add_i = (Button) findViewById(R.id.add_i);
        add_p = (Button) findViewById(R.id.add_p);
        add_d = (Button) findViewById(R.id.add_d);

        textView_i = findViewById(R.id.textView_i);
        textView_p = findViewById(R.id.textView_p);
        textView_d = findViewById(R.id.textView_d);
        impulso = prefs.getInt("Impulso", 5);
        pausa = prefs.getInt("Pausa", 1);
        depressione = prefs.getInt("Depressione", 5);
        textView_i.setText(impulso + "");
        textView_p.setText(pausa + "");
        textView_d.setText("-" + depressione + "0");

        tempo_sel = prefs.getInt("Tempo", 0);
        text1 = (TextView) findViewById(R.id.count_down);
        final int tempo_count = tempo_sel * 60000;
        counter = new Operativa_Diretta_vacuum.MyCount(tempo_count, 1000);
        counter.start();

        String hex_impulso = Integer.toHexString(impulso).toUpperCase();
        String hex_pausa = Integer.toHexString(pausa).toUpperCase();

        pacchetto_da_inviare("2E", hex_impulso + "-00", 0); //fase attiva (in sec)
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pacchetto_da_inviare("30", hex_pausa + "-00", 0); //fase passiva (in sec)
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        calcola_depressione(depressione);
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SendMsg(start_trat);

        min_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (impulso == 5) {
                    impulso = 5;
                    textView_i.setText("5");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec)
                } else if (10 >= impulso) {
                    impulso = impulso - 1;
                    textView_i.setText(impulso + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec)
                } else if (10 <= impulso && impulso <= 60) {
                    impulso = impulso - 5;
                    textView_i.setText("" + impulso + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec) "0"+ +"-00"
                } else if (60 < impulso && impulso <= 300) {
                    impulso = impulso - 60;
                    textView_i.setText(impulso + ""); //String.valueOf(k) + ":00
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec)
                }
            }
        });

        add_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 <= impulso && impulso < 10) {
                    impulso = impulso + 1;
                    textView_i.setText(impulso + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec)
                } else if (10 <= impulso && impulso < 60) {
                    impulso = impulso + 5;
                    textView_i.setText(impulso + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec) "0"+   +"-00"
                } else if (60 <= impulso && impulso < 300) {
                    k++;
                    impulso = impulso + 60;
                    textView_i.setText(impulso + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    hex = Integer.toHexString(impulso).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("2E", hex + "-00", 0); //fase attiva (in sec)

                }
            }
        });
        min_p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pausa == 1) {
                    pausa = 1;
                    textView_p.setText("1");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Pausa", pausa);
                    prefsEditor.commit();
                    hex = Integer.toHexString(pausa).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("30", "0" + hex + "-00", 0); // fase passiva (in sec)
                } else if (2 <= pausa) {
                    pausa = pausa - 1;
                    textView_p.setText(pausa + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Pausa", pausa);
                    prefsEditor.commit();
                    hex = Integer.toHexString(pausa).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("30", "0" + hex + "-00", 0); // fase passiva (in sec)
                }
            }
        });

        add_p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 <= pausa && pausa < 10) {
                    pausa = pausa + 1;
                    textView_p.setText(pausa + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Pausa", pausa);
                    prefsEditor.commit();
                    hex = Integer.toHexString(pausa).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("30", "0" + hex + "-00", 0); // fase passiva (in sec)
                } else if (pausa >= 10) {
                    pausa = 10;
                    textView_p.setText(10 + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Pausa", 10);
                    prefsEditor.commit();
                    hex = Integer.toHexString(pausa).toUpperCase();
                    System.out.println("HEX = " + hex + "-00");
                    pacchetto_da_inviare("30", "0" + hex + "-00", 0); // fase passiva (in sec)
                }
            }
        });

        min_d.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (depressione == 5) {
                    depressione = 5;
                    textView_d.setText("-" + depressione + "0");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Depressione", depressione);
                    prefsEditor.commit();
                    calcola_depressione(depressione);
                } else if (6 <= depressione) {
                    depressione = depressione - 1;
                    textView_d.setText("-" + depressione + "0");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Depressione", depressione);
                    prefsEditor.commit();
                    calcola_depressione(depressione);
                }
            }
        });

        add_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (5 <= depressione && depressione < 20) {
                    depressione = depressione + 1;
                    textView_d.setText("-" + depressione + "0");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Depressione", depressione);
                    prefsEditor.commit();
                    calcola_depressione(depressione);
                } else if (depressione >= 20) {
                    depressione = 20;
                    textView_d.setText("-" + depressione + "0");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Depressione", 10);
                    prefsEditor.commit();
                    calcola_depressione(depressione);
                }
            }
        });

        Button stop = findViewById(R.id.stop);

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
            }
        });
    }


    public void calcola_depressione(int val) {
        // calcolo del valore negativo in hex per la depressione //
        //val = 200;
        int val_calc = val * 10;
        int calc_val = (65535 - val_calc) + 1;
        String val_to_send = (Integer.toHexString(calc_val)).toUpperCase();
        String send_val = val_to_send.substring(2, 4) + "-" + val_to_send.substring(0, 2);
        System.out.println("VALLL " + val_to_send + "->" + send_val);
        pacchetto_da_inviare("2A", send_val, 0); //depressione (tra -50 e -200mmHg)
        // calcolo del valore negativo in hex per la depressione //
    }


    public class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);

            //pacchetto_da_inviare("36", "00-00", 0);
        }

        @Override
        public void onFinish() {
            text1.setText("00:00");
            stop1();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            s1 = millisUntilFinished;
            int seconds = (int) (s1 / 1000 % 60);
            int minutes = (int) (s1 / 60000 % 60);

            text1.setText("" + minutes + ":" + seconds);

            String sec = seconds + "";

            if (sec.length() == 1) {
                sec = "0" + seconds;
            } else {
                sec = seconds + "";
            }
            text1.setText("" + minutes + ":" + sec);
        }
    }

    /* cio che avviene se clicco stop */
    private void stop() {
        SendMsg(pausa_tosend);
        counter.cancel();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.pausa);
        dialog.setCancelable(false);
        TextView txt2 = (TextView) dialog.findViewById(R.id.title);
        txt2.setText(R.string.pausa);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.pausa_tratt1);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = new Operativa_Diretta_vacuum.MyCount(s1, 1000);
                SendMsg(start_trat);
                counter.start();
                dialog.dismiss();
            }
        });
        final Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                counter.cancel();
                SendMsg(stop_mio);
                try {
                    Thread.sleep(30);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }

                SendMsg(stop_mio);
                dialog.dismiss();
                closeSerialPort();
                tolog = "Vacuum";
                log.Log(tolog, Operativa_Diretta_vacuum.this);
                Context context = getBaseContext();

                contatori();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
                counter.cancel();
                finish();
            }
        });
        dialog.show();
    }


    /* cio che avviene allo scadere del tempo */
    public void stop1() {
        SendMsg(stop_mio);
        contatori();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.stop);
        dialog.setTitle(R.string.fine_tratt);
        dialog.setCancelable(false);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.fine_tratt1);
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setText(R.string.fine_tratt3);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        });
        dialog.show();
    }


    public void contatori() {
        endTime = System.currentTimeMillis();
        long millisDiff = endTime - startTime;
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        prefsEditor.putLong("Tempo parziale vac", millisDiff);
        prefsEditor.commit();
        int seconds = (int) (millisDiff / 1000 % 60);
        int minutes = (int) (millisDiff / 60000 % 60);
        int hours = (int) (millisDiff / 3600000 % 24);
        int days = (int) (millisDiff / 86400000);

        Long tempoOLD = reserved.getLong("TEMPO UTILIZZO VAC", 0);
        Long tempoTOT = tempoOLD + millisDiff;
        prefsEditor.putLong("TEMPO UTILIZZO VAC", tempoTOT);
        prefsEditor.commit();
        int seconds1 = (int) (tempoTrattamento % 60);
        int minutes1 = (int) (tempoTrattamento / 60);
        String tolog1 = minutes1 + " M " + seconds1 + "S";
        tolog = nome_trattamento_db + "Tempo= " + tolog1;
        log.Log(tolog, Operativa_Diretta_vacuum.this);

        System.out.println(days + " days, ");
        System.out.println(hours + " hours, ");
        System.out.println(minutes + " minutes, ");
        System.out.println(seconds + " seconds");
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
/*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
    public String pacchetto_da_inviare(String reg, String mess, int comando) {
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi = "51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot = null;

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        System.out.println("lenght impulso = " + lung_mess);
        int byte_val = 0;
        if (lung_mess / 2 == 4) {
            byte_val = 2;
        } else if (lung_mess == 6 || lung_mess == 5) {
            byte_val = 3;
        }
        else{
            byte_val = 2;
        }

        int lung_mess1 = messaggio.length();
        if (lung_mess1 == 2 || lung_mess == 5) {
            msg_tot = messaggio;
            System.out.println("CHE HAI????????????????????????????" + msg_tot);
        } else {
            msg_tot = "0" + messaggio;
            System.out.println("CHE HAI????????????????????????????" + msg_tot);
        }

        String byteUtilizzabili = "0" + byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0) {
            azione = scrivi;
        } else if (to_do == 1) {
            azione = leggi;
        } else if (to_do == 2) {
            azione = avvia;
        } else if (to_do == 3) {
            azione = ferma;
        }

        //numero di byte 00 per completare il pacchetto
        int bitZero = 14;

        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero - 1); i++) {
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        if (msg_tot.length() == 7) {
            data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot.substring(0, 2) + "-" + msg_tot.substring(2, 4) + stringa_zero;
            System.out.println("DATA TO SEND IMPULSO 300 = " + data_to_send);
        } else {
            data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;
        }
        System.out.println("stringa = " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" + crc16 + "-" + stop;
        System.out.println("!!!!!!!!!!!!!  - " + pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: " + pacchettocompleto + "\n");
            SendMsg(pacchettocompleto);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }

    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto) {
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte[] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow = Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length() == 1) {
            bLow = "0" + bLow;
        } else {
            bLow = bLow;
        }
        String bHigh = Integer.toHexString(byteStr[1] & 0xFF);
        if (bHigh.length() == 1) {
            bHigh = "0" + bHigh;
        } else {
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0], byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow + "-" + bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }


    /******************************************SERIALE *******************************************/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onPause in");
        super.onPause();
        Log.i(TAG, "==>onPause out");
    }

    /*@Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onStop in");
        super.onStop();
        Log.i(TAG, "==>onStop out");
    }*/

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onDestroy in");
        closeSerialPort();
        super.onDestroy();
        Log.i(TAG, "==>onDestroy out");
    }

    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG+" LEGGO da thread: " + to_check);
                        visualizzammHg(to_check);
                    } catch (IOException e) {

                    }
                }
            }
        }
    }

    public void visualizzammHg(String a) {
        if (a.contains(" 52 03 2C ")) {
            runOnUiThread(new Runnable() {
                public void run() {
                    TextView mmHg = findViewById(R.id.mmHg);
                    String cut1 = a.substring(18, 20);
                    String cut2 = a.substring(21, 23);
                    System.out.println("Tempo = " + cut1 + cut2);
                    String myString = cut2 + cut1;
                    //int foo = Integer.parseInt(myString);
                    short s = (short) Integer.parseInt(myString, 16);
                    System.out.println(s + "--------------");
                    mmHg.setText("" + s + " mmHg");
                }
            });
        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {


        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        } catch (IOException e) {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /************************************SERIALE*************************************************/


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        counter.cancel();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);

    }

    public void home(View view) {
        counter.cancel();
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}
