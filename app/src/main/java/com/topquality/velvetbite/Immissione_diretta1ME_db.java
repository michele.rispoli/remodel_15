package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.Locale;

import android_serialport_api.SerialPort;

public class Immissione_diretta1ME_db  extends Activity {


    /***********************SERIALE ********************************/


    private static final String TAG = "MR-RF-DB";
    String msg = null;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;
    private SendThread mSendThread;
    private TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();


    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    String freq_1000 = "3E-FE-01-51-04-52-00-00-01-00-00-00-00-00-00-00-00-00-00-00-00-E8-34-3C";
    String freq_500 = "3E-FE-01-51-04-52-00-80-00-00-00-00-00-00-00-00-00-00-00-00-00-F5-26-3C"; // occhio che è 1mhz
    String manipolo_corpo = "3E-FE-01-51-02-48-05-00-00-00-00-00-00-00-00-00-00-00-00-00-00-6F-54-3C";
    String manipolo_viso_bipo = "3E-FE-01-51-02-48-07-00-00-00-00-00-00-00-00-00-00-00-00-00-00-B6-19-3C";
    String manipolo_viso_mono = "3E-FE-01-51-02-48-06-00-00-00-00-00-00-00-00-00-00-00-00-00-00-4A-B7-3C";
    String radiofrequenza = "3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C";
    String config2 = "3E-FE-01-51-02-62-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D9-76-3C";
    String pedaliera_config = "3E-FE-01-51-02-61-02-00-00-00-00-00-00-00-00-00-00-00-00-00-00-4D-D3-3C";
    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";
    String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";
    //potenza 10
    String pot0 = "3E-FE-01-51-03-36-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-CB-49-3C";
    //potenza 10
    String pot1 = "3E-FE-01-51-03-36-0A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-57-23-3C";
    //potenza 20
    String pot2 = "3E-FE-01-51-03-36-14-00-00-00-00-00-00-00-00-00-00-00-00-00-00-F3-9C-3C";
    //potenza 30
    String pot3 = "3E-FE-01-51-03-36-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-6F-F6-3C";
    //potenza 40
    String pot4 = "3E-FE-01-51-03-36-28-00-00-00-00-00-00-00-00-00-00-00-00-00-00-9A-F3-3C";
    //potenza 50
    String pot5 = "3E-FE-01-51-03-36-32-00-00-00-00-00-00-00-00-00-00-00-00-00-00-8C-D7-3C";
    //potenza 60
    String pot6 = "3E-FE-01-51-03-36-3C-00-00-00-00-00-00-00-00-00-00-00-00-00-00-A2-26-3C";
    //potenza 70
    String pot7 = "3E-FE-01-51-03-36-46-00-00-00-00-00-00-00-00-00-00-00-00-00-00-A9-B5-3C";
    //potenza 80
    String pot8 = "3E-FE-01-51-03-36-50-00-00-00-00-00-00-00-00-00-00-00-00-00-00-48-2D-3C";
    //potenza 90
    String pot9 = "3E-FE-01-51-03-36-5A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D4-47-3C";
    //potenza 100
    String pot10 = "3E-FE-01-51-03-36-64-00-00-00-00-00-00-00-00-00-00-00-00-00-00-64-65-3C";



    /***********************SERIALE ********************************/

    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    int polarita = 0;
    private SeekBar seekBar;
    private TextView textView;
    public int tempo = 0;
    int ck_polarita = 0;
    int ck_tipologia = 0;
    int ck_frequenza = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    int tempo_feed = 0;
    int tempo_tot_feed = 0;
    Handler updateBarHandler;
    Button select_manipolo;


    String locale;


    /* campi db */
    String nome_trattamento_db;
    int tempo_db;
    int preseton_db;
    int presetoff_db;
    int freq_db;
    String tipologia_db;
    String polarita_db;
    String manipolo_db;
    String piastra_db;
    String pedaliera_db;
    int durata_feed_db;
    int potenza_feed_db;
    TextView textView4;
    Button set_mono;
    Button set_bipo;
    Button set_cap;
    Button set_res;
    Button set_freq1;
    Button set_freq2;

    /* campi db*/

    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Immissione_diretta1ME_db.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        locale = Locale.getDefault().getLanguage();
        setContentView(R.layout.immissione_diretta1_db);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
        textView4 = (TextView) findViewById(R.id.textView4);
        select_manipolo = (Button) findViewById(R.id.select_manipolo);
        set_mono = (Button) findViewById(R.id.polarita);
        set_bipo = (Button) findViewById(R.id.bipo);
        set_cap = (Button) findViewById(R.id.tipologia);
        set_res = (Button) findViewById(R.id.res);
        set_freq1 = (Button) findViewById(R.id.frequenza);
        set_freq2 = (Button) findViewById(R.id.freq2);


        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        nome_trattamento_db = prefs.getString("Nome Trattamento", null);
        tempo_db = prefs.getInt("TEMPO", 0);

        tempo = tempo_db;
        preseton_db = prefs.getInt("PRESET_ON", 0);
        presetoff_db = prefs.getInt("PRESET_OFF", 0);
        freq_db = prefs.getInt("FREQUENZA", 0);
        tipologia_db  = prefs.getString("TIPOLOGIA", null);
        polarita_db  = prefs.getString("POLARITA", null);
        manipolo_db  = prefs.getString("MANIPOLO", null);
        piastra_db = prefs.getString("PIASTRA", null);
        durata_feed_db = prefs.getInt("DURATA FEED", 0);
        potenza_feed_db = prefs.getInt("POTENZA FEED",0);
        textView4.setText(nome_trattamento_db); //nome del trattamento scelto
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Tempo", tempo);
        prefsEditor.commit();

        select_manipolo.setEnabled(false);
        set_bipo.setEnabled(false);
        set_mono.setEnabled(false);
        set_cap.setEnabled(false);
        set_res.setEnabled(false);
        set_freq1.setEnabled(false);
        set_freq2.setEnabled(false);

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Immissione_diretta1ME_db.this, R.raw.errore, 0);
        spoolId = spool.load(Immissione_diretta1ME_db.this, R.raw.errore, 0);


        initializeVariables();
        volume();

        /***********************SERIALE ********************************/


        pacchetto_da_inviare("4D", "01", 0);
        pacchetto_da_inviare("30", ""+preseton_db, 0);      //invio la durata dell'impulso preso dal DB
        pacchetto_da_inviare("32", ""+presetoff_db, 0);     //invio la durata della pausa presa dal db

        identifica_manipolo();
        identifica_configurazione();
        /***********************SERIALE ********************************/
        manipolo = 0;
        int leng = prefs.getInt("LINGUA", 1);

        vol_audio = prefs.getInt("Volume", 1);
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);
        seekBar.setProgress(tempo_db);
        textView.setText(tempo_db+":00");
        min.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (tempo <= 5) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo - 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
                return true;
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                }
            }
        });
        add.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (tempo >= 55){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                } else {
                    tempo = tempo + 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                }
                return true;
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                } else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                }else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                }
            }
        });

        /* GESTIONE SEEKBAR */
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = tempo_db;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }
        });
    }


    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG+" LEGGO da thread: " + to_check);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {

        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }
    /***********************SERIALE ********************************/


    /* GESTIONE VOLUME */
    public void volume() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
    }

    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
    }

    public void identifica_manipolo(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.select_manipolo);
        Resources res = getResources();
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (manipolo_db.equals("rfpm")){
            System.out.println("MAnipolo 1");
            select_manipolo.setBackgroundResource(R.drawable.rfpm);
            set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
            set_bipo.setBackgroundResource(R.drawable.btn_dx_dis);
            set_bipo.setActivated(false);
            select_manipolo.setText(" ");
            ck_polarita = 0;
            //prefsEditor.putInt("Manipolo", 1);
            prefsEditor.putInt("CK_polarita", 0);
            prefsEditor.putInt("Polarita", 1);
            prefsEditor.putInt("Elettrodo", 1);
            prefsEditor.putInt("Manipolo", 2);
            prefsEditor.putInt("Pedaliera", 1);
            manipolo = 2;
            prefsEditor.commit();
            pacchetto_da_inviare("48", "06", 0);


        }else if(manipolo_db.equals("rfpb")){
            System.out.println("MAnipolo 2");
            select_manipolo.setBackgroundResource(R.drawable.rfpb);
            set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
            set_mono.setBackgroundResource(R.drawable.btn_sx_dis);
            set_mono.setActivated(false);
            select_manipolo.setText(" ");
            prefsEditor.putInt("Polarita", 2);
            ck_polarita = 1;
            prefsEditor.putInt("Manipolo", 1);
            prefsEditor.putInt("Elettrodo", 2);
            prefsEditor.putInt("CK_polarita", 1);
            prefsEditor.putInt("Pedaliera", 1);
            manipolo = 1;
            prefsEditor.commit();
            pacchetto_da_inviare("48", "07", 0);

            String t = getString(R.string.RFpb);
            System.out.println(t);
            //t1(t, .QUEUE_FLUSH, null);

        }else if(manipolo_db.equals("rfg")){
            System.out.println("MAnipolo 3");
            select_manipolo.setBackgroundResource(R.drawable.rfg);
            set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
            set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
            select_manipolo.setText(" ");
            prefsEditor.putInt("CK_polarita", 0);
            prefsEditor.putInt("Elettrodo", 1);
            prefsEditor.putInt("Manipolo", 3);
            prefsEditor.putInt("Pedaliera", 1);
            manipolo = 3;
            prefsEditor.commit();
            pacchetto_da_inviare("48", "05", 0);
            String t2 = getString(R.string.RFg);
            System.out.println(t2);
            //t1(t2, .QUEUE_FLUSH, null);

        }else if(manipolo_db.equals("x")){
            System.out.println("MAnipolo x");
            select_manipolo.setEnabled(true);
        }
    }

    public void identifica_configurazione(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        /* setto la polarita'*/
        if(polarita_db.equals("mono")){
            set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
            set_bipo.setBackgroundResource(R.drawable.btn_dx_dis);
            set_bipo.setActivated(false);

            prefsEditor.putInt("Polarita", 1);
            prefsEditor.commit();
        }else if (polarita_db.equals("bipo")){
            set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
            set_mono.setBackgroundResource(R.drawable.btn_sx_dis);
            set_mono.setActivated(false);
            select_manipolo.setText(" ");
            prefsEditor.putInt("Polarita", 2);
            prefsEditor.commit();
        }else if(polarita_db.equals("x")){
            set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
            set_mono.setBackgroundResource(R.drawable.btn_sx_blu);
            set_bipo.setEnabled(true);
            set_mono.setEnabled(true);
            prefsEditor.putInt("Polarita", 0);
        }

        /*setto la tipologia */
        if(tipologia_db.equals("cap")){
            set_res.setBackgroundResource(R.drawable.btn_dx_dis);
            set_cap.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_tipologia = 1;
            prefsEditor.putInt("Tipologia", 1);
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();


        }else if(tipologia_db.equals("res")){

            set_res.setBackgroundResource(R.drawable.btn_dx_ar);
            set_cap.setBackgroundResource(R.drawable.btn_sx_dis);
            prefsEditor.putInt("Tipologia", 2);
            ck_tipologia = 0;
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();

        }else if(tipologia_db.equals("x")){ // da far scegliere all'utente
            set_res.setBackgroundResource(R.drawable.btn_dx_blu);
            set_cap.setBackgroundResource(R.drawable.btn_sx_blu);
            prefsEditor.putInt("Tipologia", 0);
            ck_tipologia = 0;
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();
            set_cap.setEnabled(true);
            set_res.setEnabled(true);

        }

        /* setto la frequenza operativa */
        if (freq_db == 1){ // 1Mhz
            set_freq2.setBackgroundResource(R.drawable.btn_dx_ar);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_dis);
            ck_frequenza = 1;
            SendMsg(freq_1000);
            prefsEditor.putInt("Frequenza", 2);
            prefsEditor.putInt("CK_frequenza", ck_frequenza);
            prefsEditor.commit();

        }else if (freq_db == 2){ //0.5 Mhz
            set_freq2.setBackgroundResource(R.drawable.btn_dx_dis);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_frequenza = 0;
            SendMsg(freq_500);
            prefsEditor.putInt("Frequenza", 1);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.freq1s);
            System.out.println(t);

        }else if (freq_db == 0){ // da far scegliere all'utente
            set_freq2.setBackgroundResource(R.drawable.btn_dx_blu);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_blu);
            set_freq1.setEnabled(true);
            set_freq2.setEnabled(true);
            ck_frequenza = 0;
            prefsEditor.putInt("Frequenza", 0);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();

        }
    }

    public void SelezionaManipolo(View view) {
        SelezionaManipolo1();
    }

    public void SelezionaManipolo1() {

        final Button set_manipolo = (Button) findViewById(R.id.select_manipolo);
        final Button set_mono = (Button) findViewById(R.id.mono);
        final Button set_bipo = (Button) findViewById(R.id.bipo);
        final Button elettrodo = (Button) findViewById(R.id.select_elettrodo);
        final Button pedaliera = (Button) findViewById(R.id.select_manipolo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.manipolo3);
        dialog.setCancelable(true);
        Button mod1 = (Button) dialog.findViewById(R.id.button1); //on + off
        if (locale.equals("it")) {
            mod1.setBackgroundResource(R.drawable.rfpb);
        }else if (locale.equals("en")){
            mod1.setBackgroundResource(R.drawable.rfpb);
        }
        Button mod2 = (Button) dialog.findViewById(R.id.button2); //on - off (continua)
        if (locale.equals("it")) {
            mod2.setBackgroundResource(R.drawable.rfpm);
        }else if (locale.equals("en")){
            mod2.setBackgroundResource(R.drawable.rfpm);
        }

        Button mod3 = (Button) dialog.findViewById(R.id.button3); //on - off (continua)
        if (locale.equals("it")) {
            mod3.setBackgroundResource(R.drawable.rfg);
        }else if (locale.equals("en")){
            mod3.setBackgroundResource(R.drawable.rfg);
        }

        mod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locale.equals("it")) {
                    set_manipolo.setBackgroundResource(R.drawable.rfpb);
                }else if (locale.equals("en")){
                    set_manipolo.setBackgroundResource(R.drawable.rfpb);
                }
                set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
                set_mono.setBackgroundResource(R.drawable.btn_sx_dis);
                elettrodo.setBackgroundResource(R.drawable.na);
                set_mono.setActivated(false);
                set_manipolo.setText(" ");
                elettrodo.setText(" ");
                prefsEditor.putInt("Polarita", 2);
                ck_polarita = 1;
                prefsEditor.putInt("Manipolo", 1);
                prefsEditor.putInt("Elettrodo", 2);
                prefsEditor.putInt("CK_polarita", 1);
                prefsEditor.putInt("Pedaliera", 1);
                manipolo = 1;
                prefsEditor.commit();
                SendMsg(manipolo_viso_bipo);

                String t = getString(R.string.RFpb);
                System.out.println(t);
                dialog.dismiss();
            }
        });
        mod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locale.equals("it")) {
                    set_manipolo.setBackgroundResource(R.drawable.rfpm);
                }else if (locale.equals("en")){
                    set_manipolo.setBackgroundResource(R.drawable.rfpm);
                }
                set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
                set_bipo.setBackgroundResource(R.drawable.btn_dx_dis);
                elettrodo.setBackgroundResource(R.drawable.rfe);
                set_bipo.setActivated(false);
                set_manipolo.setText(" ");
                elettrodo.setText(" ");
                ck_polarita = 0;
                prefsEditor.putInt("Manipolo", 1);
                prefsEditor.putInt("CK_polarita", 0);
                prefsEditor.putInt("Polarita", 1);
                prefsEditor.putInt("Elettrodo", 1);
                prefsEditor.putInt("Manipolo", 2);
                prefsEditor.putInt("Pedaliera", 1);
                manipolo = 2;
                prefsEditor.commit();
                SendMsg(manipolo_viso_mono);
                String t1 = getString(R.string.RFpm);
                System.out.println(t1);
                dialog.dismiss();
            }
        });
        mod3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locale.equals("it")) {
                    set_manipolo.setBackgroundResource(R.drawable.rfg);
                }else if (locale.equals("en")){
                    set_manipolo.setBackgroundResource(R.drawable.rfg);
                }
                set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
                set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
                elettrodo.setBackgroundResource(R.drawable.rfe);
                set_manipolo.setText(" ");
                elettrodo.setText(" ");
                ck_polarita = 0;
                prefsEditor.putInt("Manipolo", 1);
                prefsEditor.putInt("CK_polarita", 0);
                prefsEditor.putInt("Elettrodo", 1);
                prefsEditor.putInt("Manipolo", 3);
                prefsEditor.putInt("Polarita", 1);
                prefsEditor.putInt("Pedaliera", 1);
                manipolo = 3;
                prefsEditor.commit();
                SendMsg(manipolo_corpo);
                String t2 = getString(R.string.RFg);
                System.out.println(t2);
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public void monopolare(View view) {
        final Button set_mono = (Button) findViewById(R.id.mono);
        final Button set_bipo = (Button) findViewById(R.id.bipo);
        final Button elettrodo = (Button) findViewById(R.id.select_elettrodo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);
        ck_polarita = 0;
        if (manipolo == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.select_manipolo), Toast.LENGTH_SHORT).show();
            String t = getString(R.string.select_manipolo);
            System.out.println(t);
            elettrodo.setText(" ");
        } else if (manipolo == 1) {
            elettrodo.setBackgroundResource(R.drawable.na);
            elettrodo.setText(" ");
        } else if (manipolo == 2) {
            elettrodo.setBackgroundResource(R.drawable.rfe);
            elettrodo.setText(" ");
        } else if (manipolo == 3) {
            SharedPreferences.Editor prefsEditor = prefs.edit();
            if (ck_polarita == 0) {
                System.out.println("Sono monopolare - rimango tale");
                set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
                set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
                elettrodo.setBackgroundResource(R.drawable.rfe);
                elettrodo.setText(" ");
                prefsEditor.putInt("Polarita", 1);
                prefsEditor.putInt("CK_polarita", ck_polarita);
                prefsEditor.commit();
                String t = getString(R.string.mono);
                System.out.println(t);
            } else if (ck_polarita == 1) {
                System.out.println("sono bipolare - cambio");
                set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
                set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
                elettrodo.setBackgroundResource(R.drawable.rfe);
                elettrodo.setText(" ");
                prefsEditor.putInt("Polarita", 1);
                prefsEditor.putInt("CK_polarita", ck_polarita);
                prefsEditor.commit();
                String t = getString(R.string.bipo);
                System.out.println(t);
            }
        }
    }

    public void bipolare(View view) {
        final Button set_mono = (Button) findViewById(R.id.mono);
        final Button set_bipo = (Button) findViewById(R.id.bipo);
        final Button elettrodo = (Button) findViewById(R.id.select_elettrodo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);
        elettrodo.setText(" ");
        ck_polarita = 1;
        if (manipolo == 0) {
            Toast.makeText(getApplicationContext(), "Seleziona il manipolo!!!", Toast.LENGTH_SHORT).show();
        } else if (manipolo == 1) {
            elettrodo.setBackgroundResource(R.drawable.na);
            elettrodo.setText(" ");
        } else if (manipolo == 2) {
            elettrodo.setBackgroundResource(R.drawable.rfe);
            elettrodo.setText(" ");
        } else if (manipolo == 3) {
            SharedPreferences.Editor prefsEditor = prefs.edit();
            if (ck_polarita == 1) {
                System.out.println("Sono bipolare - rimango tale");
                elettrodo.setBackgroundResource(R.drawable.na);
                elettrodo.setText(" ");
                set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
                set_mono.setBackgroundResource(R.drawable.btn_sx_blu);
                ck_polarita = 1;
                prefsEditor.putInt("Polarita", 2);
                prefsEditor.putInt("CK_polarita", ck_polarita);
                prefsEditor.commit();
                String t = getString(R.string.bipo);
                System.out.println(t);
            } else if (ck_polarita == 0) {
                System.out.println("sono monopolare - cambio");
                elettrodo.setBackgroundResource(R.drawable.na);
                elettrodo.setText(" ");
                set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
                set_mono.setBackgroundResource(R.drawable.btn_sx_blu);
                ck_polarita = 0;
                prefsEditor.putInt("Polarita", 2);
                prefsEditor.putInt("CK_polarita", ck_polarita);
                prefsEditor.commit();
                String t = getString(R.string.mono);
                System.out.println(t);
            }
        }
    }

    public void capacitivo(View view) {
        final Button set_cap = (Button) findViewById(R.id.cap);
        final Button set_res = (Button) findViewById(R.id.res);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);
        ck_tipologia = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_tipologia == 0) {
            System.out.println("Sono capacitivo - rimango tale");
            set_res.setBackgroundResource(R.drawable.btn_dx_blu);
            set_cap.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_tipologia = 0;
            prefsEditor.putInt("Tipologia", 1);
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.cap);
            System.out.println(t);
        } else if (ck_tipologia == 1) {
            System.out.println("sono resistivo - cambio");
            set_res.setBackgroundResource(R.drawable.btn_dx_blu);
            set_cap.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_tipologia = 1;
            prefsEditor.putInt("Tipologia", 1);
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.res);
            System.out.println(t);
        }
    }

    public void resistivo(View view) {
        final Button set_cap = (Button) findViewById(R.id.cap);
        final Button set_res = (Button) findViewById(R.id.res);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);
        ck_tipologia = 1;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_tipologia == 1) {
            System.out.println("Sono resistivo - rimango tale");
            set_res.setBackgroundResource(R.drawable.btn_dx_ar);
            set_cap.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_tipologia = 1;
            prefsEditor.putInt("Tipologia", 2);
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.res);
            System.out.println(t);
        } else if (ck_tipologia == 0) {
            System.out.println("sono capacitivo - cambio");
            set_res.setBackgroundResource(R.drawable.btn_dx_ar);
            set_cap.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_tipologia = 0;
            prefsEditor.putInt("Tipologia", 2);
            prefsEditor.putInt("CK_tipologia", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.cap);
        }
    }


    public void freq1(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        ck_frequenza = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_frequenza == 0) {
            System.out.println("Sono freq1 - rimango tale");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_blu);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_frequenza = 0;
            SendMsg(freq_500);
            prefsEditor.putInt("Frequenza", 1);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.freq1s);
            System.out.println(t);
        } else if (ck_frequenza == 1) {
            System.out.println("sono freq2 - cambio");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_blu);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_frequenza = 1;
            prefsEditor.putInt("Frequenza", 1);
            prefsEditor.putInt("CK_frequenza", ck_frequenza);
            prefsEditor.commit();
            SendMsg(freq_500);
            String t = getString(R.string.freq2s);
            System.out.println(t);
        }
    }

    public void freq2(View view) {
        final Button set_freq1 = (Button) findViewById(R.id.freq1);
        final Button set_freq2 = (Button) findViewById(R.id.freq2);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        ck_frequenza = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_frequenza == 1) {
            System.out.println("Sono freq1 - rimango tale");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_ar);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_frequenza = 1;
            prefsEditor.putInt("Frequenza", 2);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();
            SendMsg(freq_1000);
            String t = getString(R.string.freq1s);
            System.out.println(t);
        } else if (ck_frequenza == 0) {
            System.out.println("sono freq2 - cambio");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_ar);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_frequenza = 0;
            SendMsg(freq_1000);
            prefsEditor.putInt("Frequenza", 2);
            prefsEditor.putInt("CK_frequenza", ck_frequenza);
            prefsEditor.commit();

            String t = getString(R.string.freq2s);
            System.out.println(t);
        }
    }


    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        int manipolo_scelto = prefs.getInt("Manipolo", 0);
        int elettrodo_scelto = prefs.getInt("Elettrodo", 0);
        int polarita_scelta = prefs.getInt("Polarita", 0);
        int capacita_scelta = prefs.getInt("Tipologia", 0);
        int piastra_scelta = prefs.getInt("Pedaliera", 0);
        int frequenza_scelta = prefs.getInt("Frequenza", 0);
        int dur_impul_sel = preseton_db;
        int dur_pausa_sel = presetoff_db;
        System.out.println("T =" +tempo_trattamento +
                " \nmanipolo=" + manipolo_scelto +
                " \nelettrodo=" + elettrodo_scelto +
                " \npolarita=" + polarita_scelta +
                " \ncapacita=" + capacita_scelta +
                " \npiastra=" + piastra_scelta +
                " \nfrequenza=" + frequenza_scelta +
                " \ndur_impulso" + dur_impul_sel +
                " \ndur_pausa" + dur_pausa_sel);


        if (tempo_trattamento == 0 || manipolo_scelto == 0 || elettrodo_scelto == 0 || polarita_scelta == 0 || capacita_scelta == 0 || piastra_scelta == 0 || frequenza_scelta == 0 || dur_impul_sel ==0 || dur_pausa_sel==0) {
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            final float f = actualVolume / maxVolume;
            System.out.println("OOOOKKKKK " + f);
            spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                    spool.play(spoolId, f, f, 1, 0, 1);
                }
            });
            spool.play(spoolId, f, f, 1, 0, 1);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {
            ShowDialog();
        }
    }

    public void ShowDialog(){
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.feedback);
        dialog.setCancelable(true);
        creaPacchettoStart();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Tempo_feed", 2);
        prefsEditor.commit();

        final TextView item1 = (TextView) dialog.findViewById(R.id.txtItem1);
        SeekBar seek1 = (SeekBar) dialog.findViewById(R.id.seekBar1);
        seek1.setMax(58);
        seek1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int prog = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                prog = progress;
                System.out.println("PROG= " + prog);
                tempo_feed = prog + 2;
                System.out.println("tempo feed= " + tempo_feed);
                item1.setText(tempo_feed + " secondi");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo_feed", tempo_feed);
                int a = prefs.getInt("Tempo_feed", 2);
                System.out.println("a= " + a);
                prefsEditor.commit();
            }

            public void onStartTrackingTouch(SeekBar arg0) {
                System.out.println("PROG= " + prog);
                tempo_feed = prog + 2;
                System.out.println("tempo feed= " + tempo_feed);
                item1.setText(tempo_feed + " secondi");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo_feed", tempo_feed);
                int a = prefs.getInt("Tempo_feed", 2);
                System.out.println("a= " + a);
                prefsEditor.commit();
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                System.out.println("PROG= " + prog);
                tempo_feed = prog + 2;
                System.out.println("tempo feed= " + tempo_feed);
                item1.setText(tempo_feed + " secondi");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo_feed", tempo_feed);
                int a = prefs.getInt("Tempo_feed", 2);
                System.out.println("a= " + a);
                prefsEditor.commit();
            }
        });

        Button start = (Button) dialog.findViewById(R.id.button1); //no
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalcolaFeed();
            }
        });

        dialog.show();
    }

    private void creaPacchettoStart() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int manipolo_scelto = prefs.getInt("Manipolo", 0);
        int elettrodo_scelto = prefs.getInt("Elettrodo", 0);
        int polarita_scelta = prefs.getInt("Polarita", 0);
        int capacita_scelta = prefs.getInt("Tipologia", 0);
        int piastra_scelta = prefs.getInt("Pedaliera", 0);
        int frequenza_scelta = prefs.getInt("Frequenza", 0);
        int dur_impul_sel = prefs.getInt("Dur_impulso", 0);
        int dur_pausa_sel = prefs.getInt("Dur_pausa", 0);
        int valore = 0;

        // PACCHETTO 0 - individua il tipo di trattamento
        // 77 1 - radiofreq                77 2 - elettroporazione
        //PACCHETTO 1:manipolo - vado a scrivere nel reg. 72
        if (manipolo_scelto == 1) {
            // cod. 7
        } else if (manipolo_scelto == 2) {
            //cod 6
        } else if (manipolo_scelto == 3) {
            // cod 5
        }
        //PACCHETTO 2: conf
        if (capacita_scelta == 2) {
            valore = 4;
            hex(valore);
        }
        if (polarita_scelta == 2) {
            valore = valore + 8;
        }
        if (frequenza_scelta == 1) {
            valore = valore + 16;
        }
        System.out.println("Valore = " + valore);
        //PACCHETTO 3: durata impulso - reg 48/49 - (esempio 1000: 48 03 E8 03)
        if (dur_impul_sel == 100) {
        } else if (dur_impul_sel == 250) {
        } else if (dur_impul_sel == 500) {
        } else if (dur_impul_sel == 1000) {
        } else if (dur_impul_sel == 2000) {
        } else if (dur_impul_sel == 3000) {
        }
        //PACCHETTO 4: durata pausa - 50/51
        if (dur_pausa_sel == 100) {
        } else if (dur_pausa_sel == 200) {
        } else if (dur_pausa_sel == 500) {
        } else if (dur_pausa_sel == 750) {
        } else if (dur_pausa_sel == 1000) {
        }

        //PACCHETTO 5: calcolo frequenza - reg 82
        if (frequenza_scelta == 1) {
            //val 32768 esa: 04 82 00 80 00
        } else if (frequenza_scelta == 2) {
            // val 65536 esa: 04 82 00 00 01
        }
        //PACCHETTO 6: potenza feedback - 54/55
        //Pacchetto 7: START
    }

    private void CalcolaFeed(){

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);

        int capacita_scelta = prefs.getInt("Tipologia", 0);
        int polarita_scelta = prefs.getInt("Polarita", 0);
        int frequenza_scelta = prefs.getInt("Frequenza", 0);

        String conf = calcola_conf();
        pacchetto_da_inviare1("45", conf, 0);


        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if(nome_trattamento_db.contains("VISO")){
            SendMsg(pot1);
        }else{
            SendMsg(pot1);
        }
        if (manipolo == 2) {
            pacchetto_da_inviare("36", "05", 0);
        } else {
            SendMsg(pot1);
        }
        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        SendMsg(config2);
        try {
            Thread.sleep(100);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        SendMsg(pedaliera_config);
        try {
            Thread.sleep(100);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        SendMsg(start);
        attesaFeed();
        if (tempo_feed ==0){
            tempo_feed = tempo_feed*1000;

            System.out.println(tempo_tot_feed + "t1");
        } else {
            tempo_feed = tempo_feed*1000;

            System.out.println(tempo_feed + "t2");
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(tempo_feed);
                } catch (Exception e) {

                }
                //ringProgressDialog.dismiss();
                //SendMsg(stop_mio);
                String t = getString(R.string.init_tratt);
                System.out.println(t);

                SharedPreferences.Editor prefsEditor = prefs.edit();
                //prefsEditor.putInt("DURATA FEED", 0);
                //prefsEditor.commit();
                //prefsEditor.putInt("Tempo_feed", 0);
                //prefsEditor.commit();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, Operativa_diretta1_db.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        }).start();
    }

    public void attesaFeed(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        tempo_feed = prefs.getInt("Tempo_feed", 2);
        if (tempo_feed == 0){
            tempo_feed = 2;
        }
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {

                Dialog dialog = new Dialog(Immissione_diretta1ME_db.this, R.style.FullHeightDialog);
                dialog.setContentView(R.layout.attesafeed);
                dialog.setCancelable(true);
                dialog.show();
            }
        };
        handler.postDelayed(r, tempo_feed);


    }


    public int hex (int i) {
        int to_calc = i;
        String hex = Integer.toHexString(to_calc);
        System.out.println("hex" + hex);
        int foo = Integer.parseInt(hex);
        return foo;
    }


    /*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String messaggio = traduci_messaggio(mess);// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String pacchetto_da_inviare1( String reg, String mess, int comando){
        String messaggio = mess;// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String traduci_messaggio(String string){
        String messaggio = null;
        int foo = Integer.parseInt(string);

        byte[] byteStr = new byte[8];
        System.out.println(Integer.toHexString(foo));
        byteStr[0] = (byte) ((foo & 0x00ff));
        byteStr[1] = (byte) ((foo & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else if (bLow.length()==0){
            bLow = "00";
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        messaggio = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(messaggio.toUpperCase() + " CALCOLO FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return messaggio.toUpperCase();
    }
    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive
        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        //System.out.println(bLow + " è giusto anch questo ? \n");
        //System.out.println(bHigh + " è giusto anch questo 4444? \n");
        //System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }

    public String calcola_conf() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        //BIT 0 - a
        int pedaleONoff = prefs.getInt("Pedale_ON-OFF", 0); // non implementato ancora -> 0=off 1=on
        //BIT 1 - b
        int funz_pedale = prefs.getInt("Funzionamento_pedale", 0); // non implementato -> 0=premo_ON rilascio_OFF; 1=premo_ON premo_OFF
        //BIT 2 - c
        int capacita_scelta = prefs.getInt("Tipologia", 0); //0= cap 1= res
        //BIT 3 - d
        int polarita_scelta = prefs.getInt("Polarita", 0);  //0=mono 1 = bipo
        //BIT 4 - e
        int frequenza_scelta = prefs.getInt("Frequenza", 0); //0= 1mhz 1=500khz
        // BIT 5 - f
        int attiva_pompa = 0; //attiva la pompa peristatica per l'elettroporazione??????????????????????????
        //BIT 6 - g
        int senso_pompa = 0; // inverte il senso della pompa ??????????????????????????????
        //BIT 7 - h
        int controllo_alimentazione = 0; // attiva il controllo dell'alimentazione dei finali RF/US



        int h = 0;
        int g = 0;
        int f = 0;
        int e = 0;
        if (frequenza_scelta == 1){
            e = 1;
        }else if (frequenza_scelta == 2){
            e = 0;
        }
        int d=0;
        if (polarita_scelta == 1){
            d = 0;
        }else if (polarita_scelta == 2){
            d = 1;
        }
        int c=0;
        if (capacita_scelta == 1){
            c = 0;
        }else if (capacita_scelta == 2){
            c = 1;
        }
        int b = 0;
        int a =0;

        String mess = ""+h+g+f+e+d+c+b+a;
        System.out.println(mess);
        int prova = Integer.parseInt(mess);
        Integer.toHexString(prova);
        String ciao = Integer.toString(prova);
        int y = 0;
        int z = 0;
        int x = 0;
        for(int i = ciao.length()-1; i >= 0;i--){
            z = Integer.parseInt(ciao.substring(i,i+1));
            y = (int)(y + (z*Math.pow(2, x))); x++;
        }

        String conf_tmp = ""+Integer.toHexString(y);
        String conf;
        if(conf_tmp.length()%2 != 0){
            conf = "0"+conf_tmp;
        }else{
            conf = conf_tmp;
        }
        System.out.println("conf " +conf);
        return conf;
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void esci(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Trattamenti_rf.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}