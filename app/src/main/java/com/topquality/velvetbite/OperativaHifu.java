package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

import android_serialport_api.SerialPort;


public class OperativaHifu extends Activity {

    int count = 0;
    int cont_notestina = 0;
    private TextView Shot;
    private TextView TVlength;
    private TextView TVenergy;
    int shot_test_sess = 0;
    private TextView ShotRimasti;
    int shot_rimasti = 0;
    private Button min1;
    private Button add1;
    private Button min2;
    private Button add2;
    private Button min3;
    private Button add3;
    private Button info_shot;
    int shot = 0; //shot totali della sessione corrente = shot_test1 + shot_test2 + shot_test3 + shot_test4 + shot_test5
    int shot_test1 = 0; // shot della sessione per la testina 1
    int shot_test2 = 0;// shot della sessione per la testina 2
    int shot_test3 = 0;// shot della sessione per la testina 3
    int shot_test4 = 0;// shot della sessione per la testina 4
    int shot_test5 = 0;// shot della sessione per la testina 5
    int shot_t1 = 0; // shot della sessione per la testina 1
    int shot_t2 = 0;// shot della sessione per la testina 2
    int shot_t3 = 0;// shot della sessione per la testina 3
    int shot_t4 = 0;// shot della sessione per la testina 4
    int shot_t5 = 0;// shot della sessione per la testina 5
    int shotRim_t1 = 0;
    int shotRim_t2 = 0;
    int shotRim_t3 = 0;
    int shotRim_t4 = 0;
    int shotRim_t5 = 0;
    int count_tutorial = 0;
    private TextView shot1;
    private TextView shot2;
    private TextView shot3;
    private TextView shot4;
    private TextView shot5;
    /************/
    /* STRINGHE */
    /************/
    /*

    /*Hifu rev2*/
    String start_h2 = "AA-73-03-89-00-8C-CC-33-C3-3C";
    String start_h2a = "AA-72-03-89-00-8C-CC-33-C3-3C";
    String stop1 = "AA-73-03-C7-00-30-CC-33-C3-3C";
    String stop2 = "AA-72-03-C7-00-30-CC-33-C3-3C";
    String potenza_up1 = "AA-73-03-87-01-5C-CC-33-C3-3C";
    String potenza_up2 = "AA-72-03-87-01-5C-CC-33-C3-3C";
    String potenza_down1 = "AA-73-03-87-01-9C-CC-33-C3-3C";
    String potenza_down2 = "AA-72-03-87-01-9C-CC-33-C3-3C";
    String lunghezza_up1 = "AA-73-03-C8-01-53-CC-33-C3-3C";
    String lunghezza_up2 = "AA-72-03-C8-01-53-CC-33-C3-3C";
    String lunghezza_down1 = "AA-73-03-C9-01-9C-CC-33-C3-3C";
    String lunghezza_down2 = "AA-72-03-C9-01-9C-CC-33-C3-3C";

    int volume = 0;
    int vol_audio = 0;
    private TextView TVstep;
    public int lenght = 10;
    public int energy;
    public int step = 25;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;
    TextToSpeech t1;
    TextToSpeech t2;
    int testina;
    ImageView testina_img;
    int code;
    int step_old;
    int energymin;
    int energymax;
    int corpo_on;

    boolean testina_inserita = false;
    boolean chiudi_tutorial = false;

    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";

    /***********************SERIALE*************************/
    private static final String TAG = "OPERATIVA HIFU";
    String msg = null;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private OperativaHifu.ReadThread mReadThread;
    private OperativaHifu.SendThread mSendThread;
    private OperativaHifu.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();

    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }
            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);
        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new OperativaHifu.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    /**************************SERIALE*********************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operativahifu);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        /**************************SERIALE*********************************/
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }

        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });

        SendMsg("3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C");

        SendMsg(start);
        /**************************SERIALE*********************************/

        Shot = (TextView) findViewById(R.id.shot);
        shot1 = (TextView) findViewById(R.id.shot1a);
        shot2 = (TextView) findViewById(R.id.shot2a);
        shot3 = (TextView) findViewById(R.id.shot3a);
        shot4 = (TextView) findViewById(R.id.shot4a);
        shot5 = (TextView) findViewById(R.id.shot5a);

/*        corpo_on = reserved.getInt("CORPO", 0);
        if(corpo_on == 0){
            shot4.setText(getString(R.string.non_ab));
            shot5.setText(getString(R.string.non_ab));
        }*/
        //ShotRimasti = (TextView) findViewById(R.id.shot_rimasti);
        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(OperativaHifu.this, R.raw.errore, 0);
        spoolId = spool.load(OperativaHifu.this, R.raw.errore, 0);
        final SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        code = prefs.getInt("CODE_HIFU", 0);

        testina_img = (ImageView) findViewById(R.id.testina);
        testina = prefs.getInt("Testina", 0);

        /* inizializzazione dei paramentri */

        TVenergy = findViewById(R.id.textViewenergy);
        TVstep = findViewById(R.id.textViewstep);
        min2 = findViewById(R.id.min2);
        min2.setEnabled(false);
        //info_shot = (Button) findViewById(R.id.info1);
        add2 = findViewById(R.id.add2);
        min3 = findViewById(R.id.min3);
        //min3.setEnabled(false);
        add3 = findViewById(R.id.add3);
        add3.setEnabled(false);
        configurazione_iniziale();
        energymin = prefs.getInt("EnergyMIN", 10);
        energymax = prefs.getInt("EnergyMax", 100);
        energy = prefs.getInt("ENERGY", 10);
        prefsEditor.putInt("STEP", 25);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina1", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina2", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina3", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina4", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina5", 0);
        prefsEditor.commit();
        //prefsEditor.putInt("Testina", 0);
        /* gestione del volume */
        vol_audio = prefs.getInt("Volume", 1);
        prefsEditor.commit();
        updateBarHandler = new Handler();

        count_tutorial = 0;
        //tutorial();
        individuaTestina();

        min2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  // lenght -
                add2.setVisibility(View.INVISIBLE);
                min2.setVisibility(View.INVISIBLE);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                min2.setVisibility(View.VISIBLE);
                add2.setVisibility(View.VISIBLE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                energymin = prefs.getInt("EnergyMIN", 10);
                energymax = prefs.getInt("EnergyMax", 55);
                add2.setEnabled(true);
                if (energy == energymin) {
                    //energy = 10;
                    int da_vedere_qz = energy / 100;
                    int da_vedere_re = energy % 100;
                    if (da_vedere_qz == 1 && da_vedere_re == 5) {
                        TVenergy.setText(da_vedere_qz + ",0" + da_vedere_re);
                    } else if (da_vedere_qz == 1 && da_vedere_re == 0) {
                        TVenergy.setText(da_vedere_qz + ",00");
                    } else {
                        TVenergy.setText(da_vedere_qz + "," + da_vedere_re);
                    }
                    min2.setEnabled(false);
                    System.out.println("ENERGY " + energy);
                    prefsEditor.putInt("ENERGY", energy);
                    prefsEditor.commit();

                    pacchetto_da_inviare("2A", "01-00" , 0);
                } else {
                    energy = energy - 5;
                    //add2.setEnabled(false);
                    int da_vedere_qz = energy / 100;
                    int da_vedere_re = energy % 100;

                    if (da_vedere_qz == 1 && da_vedere_re == 5) {
                        TVenergy.setText(da_vedere_qz + ",0" + da_vedere_re);
                    } else if (da_vedere_qz == 1 && da_vedere_re == 0) {
                        TVenergy.setText(da_vedere_qz + ",00");
                    } else {
                        TVenergy.setText(da_vedere_qz + "," + da_vedere_re);
                    }
                    System.out.println("ENERGY " + energy);
                    prefsEditor.putInt("ENERGY", energy);
                    prefsEditor.commit();
                    pacchetto_da_inviare("2A", "01-00" , 0);

                }
            }
        });
        add2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  // lenght -
                add2.setVisibility(View.INVISIBLE);
                min2.setVisibility(View.INVISIBLE);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                min2.setVisibility(View.VISIBLE);
                add2.setVisibility(View.VISIBLE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                energymin = prefs.getInt("EnergyMIN", 10);
                energymax = prefs.getInt("EnergyMax", 55);
                min2.setEnabled(true);
                if (energy < energymax) {

                    energy = energy + 5;
                    int da_vedere_qz = energy / 100;
                    int da_vedere_re = energy % 100;

                    if (da_vedere_qz == 1 && da_vedere_re == 5) {
                        TVenergy.setText(da_vedere_qz + ",0" + da_vedere_re);
                    } else if (da_vedere_qz == 1 && da_vedere_re == 0) {
                        TVenergy.setText(da_vedere_qz + ",00");
                    } else {
                        TVenergy.setText(da_vedere_qz + "," + da_vedere_re);
                    }
                    System.out.println("ENERGY " + energy);
                    prefsEditor.putInt("ENERGY", energy);
                    prefsEditor.commit();

                    pacchetto_da_inviare("2A", "10-00" , 0);


                    //pacchetto_da_inviare(energy, 1);
                } else if (energy >= energymax) {
                   /* add2.setVisibility(View.INVISIBLE);
                    min2.setVisibility(View.INVISIBLE);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    min2.setVisibility(View.VISIBLE);
                    add2.setVisibility(View.INVISIBLE);*/
                    energy = energymax;
                    int da_vedere_qz = energy / 100;
                    int da_vedere_re = energy % 100;

                    if (da_vedere_qz == 1 && da_vedere_re == 5) {
                        TVenergy.setText(da_vedere_qz + ",0" + da_vedere_re);
                    } else if (da_vedere_qz == 1 && da_vedere_re == 0) {
                        TVenergy.setText(da_vedere_qz + ",00");
                    } else {
                        TVenergy.setText(da_vedere_qz + "," + da_vedere_re);
                    }
                    add2.setEnabled(false);
                    System.out.println("ENERGY " + energy);
                    prefsEditor.putInt("ENERGY", energy);
                    prefsEditor.commit();
                    //pacchetto_da_inviare(energy, 1);

                    pacchetto_da_inviare("2A", "10-00" , 0);


                }
            }
        });

        min3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  // lenght -

                SharedPreferences.Editor prefsEditor = prefs.edit();
                add3.setEnabled(true);
                if (step <= 5) {
                    step = 5;
                    min3.setEnabled(false);
                    System.out.println("STEP " + step);
                    stampa_a_video(step);
                    prefsEditor.putInt("STEP", step);
                    prefsEditor.commit();
                    //pacchetto_da_inviare(step, 2);
                    step_old = 0;
                    pacchetto_da_inviare("2B", "01-00" , 0);
                } else {
                    step = step - 1;
                    //add3.setEnabled(false);
                    System.out.println("STEP " + step);
                    stampa_a_video(step);
                    prefsEditor.putInt("STEP", step);
                    prefsEditor.commit();
                    //pacchetto_da_inviare(step, 2);
                    pacchetto_da_inviare("2B", "01-00" , 0);
                }
            }
        });
        add3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  // lenght -
                SharedPreferences.Editor prefsEditor = prefs.edit();
                min3.setEnabled(true);
                if (step < 25) {
                    step = step + 1;
                    System.out.println("STEP " + step);
                    stampa_a_video(step);
                    prefsEditor.putInt("STEP", step);
                    prefsEditor.commit();
                    //pacchetto_da_inviare(step, 2);
                    pacchetto_da_inviare("2B", "10-00" , 0);
                } else if (step >= 25) {
                    step = 25;
                    add3.setEnabled(false);
                    System.out.println("STEP " + step);
                    stampa_a_video(step);
                    prefsEditor.putInt("STEP", step);
                    prefsEditor.commit();
                    //pacchetto_da_inviare(step, 2);
                    step_old = 1;
                }
            }
        });
    }



    public String pacchetto_da_inviare( String reg, String mess, int comando){
        //String messaggio = traduci_messaggio(mess);// messaggio che voglio andare a scrivere
        String messaggio = mess;
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        if (reg.equals("14")){
            byte_val=2;
        }else if (reg.equals("2A")){
            byte_val=2;
            messaggio = mess;
        }else if (reg.equals("2B")){
            byte_val=2;
            messaggio = mess;
        }else if (reg.equals("26")){
            byte_val=3;
            messaggio = mess;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String pacchetto_da_inviare1( String reg, String mess, int comando){
        String messaggio = mess;// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String traduci_messaggio(String string){
        String messaggio = null;
        int foo = Integer.parseInt(string);

        byte[] byteStr = new byte[8];
        System.out.println(Integer.toHexString(foo));
        byteStr[0] = (byte) ((foo & 0x00ff));
        byteStr[1] = (byte) ((foo & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else if (bLow.length()==0){
            bLow = "00";
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        messaggio = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(messaggio.toUpperCase() + " CALCOLO FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return messaggio.toUpperCase();
    }
    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive
        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        //System.out.println(bLow + " è giusto anch questo ? \n");
        //System.out.println(bHigh + " è giusto anch questo 4444? \n");
        //System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }

    public void configurazione_iniziale() {
        TVenergy = findViewById(R.id.textViewenergy);
        TVstep = findViewById(R.id.textViewstep);
        final SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        testina = prefs.getInt("Testina", 0);
        if (testina == 1) { //DS 10-1.5
            testina_img.setImageResource(R.drawable.d415);
            TVenergy.setText("0,10");
            TVstep.setText(" 25 ");
        } else if (testina == 2) { //DS 7-3.0
            testina_img.setImageResource(R.drawable.d430);
            TVenergy.setText("0,75");
            TVstep.setText(" 25 ");
        } else if (testina == 3) { //DS 4-4.5
            testina_img.setImageResource(R.drawable.d445);
            TVenergy.setText("0,75");
            TVstep.setText(" 25 ");
        } else if (testina == 4) { //DS 4-8.0
            testina_img.setImageResource(R.drawable.d478);
            TVenergy.setText("0,75");
            TVstep.setText(" 25 ");
        } else if (testina == 5) { //DS 4-13.0
            testina_img.setImageResource(R.drawable.d413);
            TVenergy.setText("0,75");
            TVstep.setText(" 25 ");
        }
        if (testina == 1) { //DS 10-1.5
            prefsEditor.putInt("EnergyMIN", 10);
            prefsEditor.commit();
            prefsEditor.putInt("EnergyMax", 55);
            prefsEditor.commit();
            prefsEditor.putInt("ENERGY", 10);
            prefsEditor.commit();
            energy = prefs.getInt("ENERGY", 10);
            min2.setEnabled(false);
            add2.setEnabled(true);
        } else {
            prefsEditor.putInt("EnergyMIN", 75);
            prefsEditor.commit();
            prefsEditor.putInt("EnergyMax", 120);
            prefsEditor.commit();
            prefsEditor.putInt("ENERGY", 75);
            prefsEditor.commit();
            energy = prefs.getInt("ENERGY", 10);
            min2.setEnabled(false);
            add2.setEnabled(true);
        }


    }

    /**************************SERIALE*********************************/


    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG + " LEGGO da thread - operativa hifu: " + to_check);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {

        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        } catch (IOException e) {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /**************************SERIALE*********************************/

    private void tutorial() {
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.tutorial);
        dialog.setCancelable(false);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        final ImageView immagine = (ImageView) dialog.findViewById(R.id.imageView4);

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count_tutorial == 0) {
                    immagine.setImageResource(R.drawable.startth2);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    count_tutorial = 1;
                } else {
                    //count_tutorial = 0;
                    if (code == 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        SendMsg(start);
                        //SendMsg(start_h2);
                        /*try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        //SendMsg(start_h2a);*/
                    } else if (code == 1) {

                    }
                    dialog.dismiss();

                }
            }
        });
        dialog.show();
    }

    public void info_shot(View view) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Hifu_val", MODE_PRIVATE);
        shot_t1 = prefs.getInt("ShotTestina1", 0);
        shot_t2 = prefs.getInt("ShotTestina2", 0);
        shot_t3 = prefs.getInt("ShotTestina3", 0);
        shot_t4 = prefs.getInt("ShotTestina4", 0);
        shot_t5 = prefs.getInt("ShotTestina5", 0);

        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.info_shot);
        dialog.setCancelable(true);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        //final ImageView immagine = (ImageView) dialog.findViewById(R.id.imageView4);
        final TextView shot1 = (TextView) dialog.findViewById(R.id.shot1a);
        final TextView shot2 = (TextView) dialog.findViewById(R.id.shot2a);
        final TextView shot3 = (TextView) dialog.findViewById(R.id.shot3a);
        final TextView shot4 = (TextView) dialog.findViewById(R.id.shot4a);
        shot1.setText("" + shot_t1);
        shot2.setText("" + shot_t2);
        shot3.setText("" + shot_t3);
        shot4.setText("" + shot_t5);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void info_shot1() {
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.info_shot);
        dialog.setCancelable(true);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        final ImageView immagine = (ImageView) dialog.findViewById(R.id.imageView4);

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

    }


    public String pacchetto_da_inviare(int mess, int comando) {
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; // dove vado a scrivere? 0-lenght 1-energy 2-step
        String byte1 = "AA";
        String byte2 = "CC";
        String byte3 = "06";
        String byte4 = "83";
        String byte6 = "00"; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String byte789_l = "91-01-00";
        String byte789_e = "38-01-00";
        String byte789_s = "3A-01-00";
        String byte789 = null;
        String val = null;
        int messaggio = mess; // messaggio che voglio andare a scrivere
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0) {
            byte789 = byte789_l;
        } else if (to_do == 1) {
            byte789 = byte789_e;
        } else if (to_do == 2) {
            byte789 = byte789_s;
        }
        //pacchetto sul quale calcolare il crc16
        data_to_send = byte1 + "-" + byte2 + "-" + byte3 + "-" + byte4 + "-" + byte6 + "-" + byte789;

        //valore del crc16
        String val1 = Integer.toHexString(messaggio);
        if (val1.length() == 1) {
            val = "0" + val1;
        } else {
            val = val1;
        }

        System.out.println("VALOREEEEEEEEEEEEE = " + val);

        //System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" + val.toUpperCase();
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: " + pacchettocompleto + "\n");
            SendMsg(pacchettocompleto);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    private void individuaTestina() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
                testina = prefs.getInt("Testina", 0);
                if (testina == 0) { //nessuna testina
                    testina_img.setImageResource(R.drawable.na);
                    System.out.println(testina + "=========> testina NON inseita");
                } else if (testina == 1) { //testina 1.5
                    testina_img.setImageResource(R.drawable.d415);
                    System.out.println(testina + "=========> testina 1.5 inseita");
                } else if (testina == 2) { //testina 3.0
                    testina_img.setImageResource(R.drawable.d430);
                    System.out.println(testina + "=========> testina 3.0 inseita");
                } else if (testina == 3) { //testina 4.5
                    testina_img.setImageResource(R.drawable.d445);
                    System.out.println(testina + "=========> testina 4.5 inseita");
                } else if (testina == 4) { //testina 7/8
                    testina_img.setImageResource(R.drawable.d478);
                    System.out.println(testina + "=========> testina 7/8 inseita");
                } else if (testina == 5) { //testina 13
                    testina_img.setImageResource(R.drawable.d413);
                    System.out.println(testina + "=========> testina 13 inseita");
                }
                configurazione_iniziale();
            }
        });
    }

    /* calcola gli shot della testina nella sessione in corso */
    public void calcola_sottostringa(final String stringa) {
        String val1 = stringa.substring(0, 17);
        //System.out.println("LA stringa d'inizio è: " + val1);

        String val = stringa.substring(18, 23);
        //System.out.println("LA SOTTOSTRINGA è: " + val);

        String nuovaStr = val.replaceAll("[ \\p{Punct}]", "");
        shot_test_sess = Integer.parseInt(nuovaStr, 16);
        System.out.println("il valore  è: " + shot_test_sess);

        String shotRim = stringa.substring(23, 29);

        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        testina = prefs.getInt("Testina", 0);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("Hifu_val", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                int shotRim = shot_rim(stringa);
                shotRim_t1 = prefs.getInt("ShotRIMTestina1", 0);
                shotRim_t2 = prefs.getInt("ShotRIMTestina2", 0);
                shotRim_t3 = prefs.getInt("ShotRIMTestina3", 0);
                shotRim_t4 = prefs.getInt("ShotRIMTestina4", 0);
                shotRim_t5 = prefs.getInt("ShotRIMTestina5", 0);
                //if(shot_test_sess != 0) {
                if (testina == 1 && shotRim != shotRim_t1) { //testina 1.5
                    shot_test1 = prefs.getInt("ShotTestina1", 0);
                    if (shot_test_sess != 0) {
                        shot_t1 = shot_test1 + 1;
                        int shotRim1 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina1", shot_t1);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina1", shotRim1);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    } else {
                        shot_t1 = shot_test1;
                        int shotRim1 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina1", shot_t1);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina1", shotRim1);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    }
                } else if (testina == 2 && shotRim != shotRim_t2) { //testina 3.0
                    shot_test2 = prefs.getInt("ShotTestina2", 0);
                    if (shot_test_sess != 0) {
                        shot_t2 = shot_test2 + 1;
                        int shotRim2 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina2", shot_t2);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina2", shotRim2);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    } else {
                        shot_t2 = shot_test2;
                        int shotRim2 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina2", shot_t2);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina2", shotRim2);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    }

                } else if (testina == 3 && shotRim != shotRim_t3) { //testina 4.5
                    shot_test3 = prefs.getInt("ShotTestina3", 0);
                    if (shot_test_sess != 0) {
                        //shot_test3 = shot_test_sess;
                        shot_t3 = shot_test3 + 1;
                        int shotRim3 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina3", shot_t3);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina3", shotRim3);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    } else {
                        shot_t3 = shot_test3;
                        int shotRim3 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina3", shot_t3);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina3", shotRim3);
                        prefsEditor.commit();
                        setStot();
                        // individuaTestina();
                    }
                } else if (testina == 4 && shotRim != shotRim_t4) { //testina 7/8
                    shot_test4 = prefs.getInt("ShotTestina4", 0);
                    if (shot_test_sess != 0) {
                        shot_t4 = shot_test4 + 1;
                        int shotRim4 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina4", shot_t4);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina4", shotRim4);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    } else {
                        shot_t4 = shot_test4;
                        int shotRim4 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina4", shot_t4);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina4", shotRim4);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    }
                } else if (testina == 5 && shotRim != shotRim_t5) { //testina 13
                    shot_test5 = prefs.getInt("ShotTestina5", 0);
                    if (shot_test_sess != 0) {
                        shot_t5 = shot_test5 + 1;
                        int shotRim5 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina5", shot_t5);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina5", shotRim5);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    } else {
                        shot_t5 = shot_test5;
                        int shotRim5 = shot_rim(stringa);
                        prefsEditor.putInt("ShotTestina5", shot_t5);
                        prefsEditor.commit();
                        prefsEditor.putInt("ShotRIMTestina5", shotRim5);
                        prefsEditor.commit();
                        setStot();
                        //individuaTestina();
                    }
                }
                //}


            }
        });
        // individuaTestina();
    }

    public void setStot() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("Hifu_val", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                shot_t1 = prefs.getInt("ShotTestina1", 0);
                shot_t2 = prefs.getInt("ShotTestina2", 0);
                shot_t3 = prefs.getInt("ShotTestina3", 0);
                shot_t4 = prefs.getInt("ShotTestina4", 0);
                shot_t5 = prefs.getInt("ShotTestina5", 0);
                testina = prefs.getInt("Testina", 0);
                if (testina == 1) {
                    shot_t1 = shot_t1 + 1;
                    prefsEditor.putInt("ShotTestina1", shot_t1);
                    prefsEditor.commit();
                    int stot_tmp = prefs.getInt("ShotTestina1", 0);
                    shot1.setText(Integer.toString(stot_tmp));
                } else if (testina == 2) {
                    shot_t2 = shot_t2 + 1;
                    prefsEditor.putInt("ShotTestina2", shot_t2);
                    prefsEditor.commit();
                    shot2.setText(Integer.toString(shot_t2));
                } else if (testina == 3) {
                    shot_t3 = shot_t3 + 1;
                    prefsEditor.putInt("ShotTestina3", shot_t3);
                    prefsEditor.commit();
                    shot3.setText(Integer.toString(shot_t3));
                } else if (testina == 4) {
                    shot_t4 = 1 + shot_t4;
                    prefsEditor.putInt("ShotTestina4", shot_t4);
                    prefsEditor.commit();
                    shot4.setText(Integer.toString(shot_t4));
                } else if (testina == 5) {
                    shot_t5 = ++shot_t5;
                    prefsEditor.putInt("ShotTestina5", shot_t5);
                    prefsEditor.commit();
                    shot5.setText(Integer.toString(shot_t5));
                }

                shot = shot_t1 + shot_t2 + shot_t3 + shot_t4 + shot_t5;
                System.out.println(shot_t1 + " " + shot_t2 + " " + shot_t3 + " " + shot_t4 + " " + shot_t5);
                Shot.setText(Integer.toString(shot));

            }
        });

    }

    public void conta_spari() {
        SharedPreferences reserved = getSharedPreferences("Reserved", MODE_PRIVATE);
        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        int s1 = prefs.getInt("ShotTestina1", 0);
        int s2 = prefs.getInt("ShotTestina2", 0);
        int s3 = prefs.getInt("ShotTestina3", 0);
        int s4 = prefs.getInt("ShotTestina4", 0);
        int s5 = prefs.getInt("ShotTestina5", 0);
        int tot = s1 + s2 + s3 + s4 + s5;

        int s1_old = reserved.getInt("ShotTestina1_cnt", 0);
        int s2_old = reserved.getInt("ShotTestina2_cnt", 0);
        int s3_old = reserved.getInt("ShotTestina3_cnt", 0);
        int s4_old = reserved.getInt("ShotTestina4_cnt", 0);
        int s5_old = reserved.getInt("ShotTestina5_cnt", 0);
        int tot_old = reserved.getInt("ShotTOT_cnt", 0);

        int new_s1 = s1 + s1_old;
        int new_s2 = s2 + s2_old;
        int new_s3 = s3 + s3_old;
        int new_s4 = s4 + s4_old;
        int new_s5 = s5 + s5_old;
        int new_tot = tot + tot_old;


        prefsEditor.putInt("ShotTestina1_cnt", new_s1);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina2_cnt", new_s2);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina3_cnt", new_s3);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina4_cnt", new_s4);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina5_cnt", new_s5);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTOT_cnt", new_tot);
        prefsEditor.commit();
    }

    /*calcola gli shot totali della testina */
    public void shot_tot(String stringa) {
        String val = stringa.substring(30, 35);
        String nuovaStr = val.replaceAll("[ \\p{Punct}]", "");
        int n = Integer.parseInt(nuovaStr, 16);
    }

    /*calcola gli shot rimasti della testina */
    public int shot_rim(String stringa) {
        String val = stringa.substring(23, 29);
        //System.out.println("LA SOTTOSTRINGA parziale è: " + val);

        String nuovaStr = val.replaceAll("[ \\p{Punct}]", "");
        shot_rimasti = Integer.parseInt(nuovaStr, 16);
        //System.out.println("il valore parziale  è: " + shot_rimasti);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ShotRimasti.setText(Integer.toString(shot_rimasti));

            }
        });
        return shot_rimasti;
    }

    private void analizzaPacchetto(String stringa, int dimensione) {
        String stringa_arrivata = stringa;
        String stringa_pacchetto = null;
        System.out.println("LA STRINGA CHE ANALIZZO è -->" + stringa_arrivata);
        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (stringa_arrivata.contains("3E 01 FE 52 02 25 03 ")) {
            //testina ds 7-3.0
            System.out.println("testina ds 7-3.0");
            prefsEditor.putInt("Testina", 2);
            prefsEditor.commit();
            individuaTestina();

        } else if (stringa_arrivata.contains("3E 01 FE 52 02 25 02 ")) {
            //testina ds 10-1.5
            System.out.println("testina ds 10-1.5");
            prefsEditor.putInt("Testina", 1);
            prefsEditor.commit();
            individuaTestina();

        } else if (stringa_arrivata.contains("3E 01 FE 52 02 25 07 ")) {
            //testina ds 4-8.0
            System.out.println("testina ds 4-8.0");
            prefsEditor.putInt("Testina", 4);
            prefsEditor.commit();
            individuaTestina();

        } else if (stringa_arrivata.contains("3E 01 FE 52 02 25 08 ")) {
            //testina 4-13.0
            System.out.println("testina 4-13.0");
            prefsEditor.putInt("Testina", 5);
            prefsEditor.commit();
            individuaTestina();


        } else if (stringa_arrivata.contains("3E 01 FE 52 02 25 06 ")) {
            //testina ds 4-4.5
            System.out.println("testina ds 4-4.5");
            prefsEditor.putInt("Testina", 3);
            prefsEditor.commit();
            individuaTestina();
        } else if (stringa_arrivata.equals("3E 01 FE 52 02 25 00 ")) {
            //no testina
            System.out.println("no testina");
            prefsEditor.putInt("Testina", 0);
            prefsEditor.commit();
            individuaTestina();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });
        }
        /*else if (stringa_arrivata.contains("AA 71 04 03 1B 00 67 03 80 00 CD 03 1B 00 67 CC 33 C3 3C ")) {
            System.out.println("SONO QUAAAAAAAAAAA!!!!!!!!!!!!---------->");
            setStot();
        } else if (stringa_arrivata.contains("AA 71 05 03 60 02 9F 03 EF 02 D3 03 60 02 9F CC 33 C3 3C ") ||
                stringa_arrivata.contains("AA 71 09 00 C4 01 83 03 1E 02 E3 00 C4 01 83 CC 33 C3 3C") ||
                stringa_arrivata.contains("AA 71 0E 03 60 02 9F 03 EF 02 D3 03 60 02 9F CC 33 C3 3C ") ||
                stringa_arrivata.contains("AA 71 0C 00 C4 01 83 03 1E 02 E3 00 C4 01 83 CC 33 C3 3C ")) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });
        } else if (stringa_arrivata.contains("AA 71 06 03 60 02 9F 03 EF 02 D3 03 60 02 9F CC 33 C3 3C ") ||
                stringa_arrivata.contains("AA 71 07 00 C4 01 83 03 1E 02 E3 00 C4 01 83 CC 33 C3 3C") ||
                stringa_arrivata.contains("AA 71 0F 03 60 02 9F 03 EF 02 D3 03 60 02 9F CC 33 C3 3C ") ||
                stringa_arrivata.contains("AA 71 0A 00 C4 01 83 03 1E 02 E3 00 C4 01 83 CC 33 C3 3C ")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });
        }*/

    }


    private void stampa_a_video(int st) {
        step = st;
        if (step <= 9) {
            TVstep.setText("" + " " + step + " ");
        } else {
            TVstep.setText("" + step);
        }
    }


    public void reset(View view) {
        pacchetto_da_inviare("26", "00-00",0);
/*        conta_spari();
        //SendMsg(stop1);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, TestCartuccia.class);
        startActivityForResult(CauseNelleVic, 0);*/

    }

    public void reset1() {
        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("LENGTH", 10);
        prefsEditor.commit();
        prefsEditor.putInt("ENERGY", 1);
        prefsEditor.commit();
        prefsEditor.putInt("STEP", 5);
        prefsEditor.commit();
        lenght = 10;
        energy = 1;
        step = 5;
        //SendMsg(l0);
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        //SendMsg(e0);
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        //SendMsg(s0);

        TVlength.setText("1,0");
        min1.setEnabled(false);
        add1.setEnabled(true);
        TVenergy.setText("0,1");
        min2.setEnabled(false);
        add2.setEnabled(true);
        TVstep.setText(" 5 ");
        min3.setEnabled(false);
        add3.setEnabled(true);
    }

    public void stop(View view) {
        conta_spari();
        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("ShotTestina1", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina2", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina3", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina4", 0);
        prefsEditor.commit();
        prefsEditor.putInt("ShotTestina5", 0);
        prefsEditor.commit();
        //SendMsg(stop1);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";
        SendMsg(stop_mio);
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void impostazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void esci(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}