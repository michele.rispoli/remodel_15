package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

import android_serialport_api.SerialPort;

public class Imm_Diretta_ou extends Activity {

    private static final String TAG = "FRAZ";
    String msg = null;

    private SeekBar seekBar;
    private TextView textView;
    public int tempo = 0;
    SoundPool spool;
    int spoolId;
    Handler updateBarHandler;
    String hex;
    private SerialPort mSerialPort = null;

    String tipologia;
    Button v1;
    Button v2;
    Button v3;
    TextView freq1;
    Button manipolo;
    Bundle state;

    int frequenza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.immissione_diretta_ou);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0) {
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        } else if (reseller == 1) {
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        initializeVariables();
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("TIPOLOGIA", "x").commit();
        prefsEditor.putInt("ENERGIA", 60).commit();
        prefsEditor.putInt("FREQ1", 0).commit();
        prefsEditor.putInt("TEMPO", 0).commit();
        prefsEditor.putInt("Tempo", 0).commit();
        LinearLayout layout_vac = findViewById(R.id.layout_vac);
        int trattamento_1 = prefs.getInt("trattamento_1", 0);
        if (trattamento_1 == 1) {
            layout_vac.setVisibility(View.VISIBLE);
        }
        else if (trattamento_1 == 3) {
            layout_vac.setVisibility(View.GONE);
            prefsEditor.putString("TIPOLOGIA", "4").commit();
        }
        else if (trattamento_1 == 5) {
            layout_vac.setVisibility(View.GONE);
            prefsEditor.putString("TIPOLOGIA", "4").commit();
        }
        frequenza = prefs.getInt("FREQ1", 0);
        freq1 = (TextView) findViewById(R.id.textView2);

        prefsEditor.commit();
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);


        Button min1 = (Button) findViewById(R.id.min1);
        Button add1 = (Button) findViewById(R.id.add1);

        seekBar.setProgress(0);
        textView.setText("0:00");

        tipologia = prefs.getString("TIPOLOGIA", "x");
        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundResource(R.drawable.v1a);
                v2.setBackgroundResource(R.drawable.v2);
                v3.setBackgroundResource(R.drawable.v3);
                manipolo.setBackgroundResource(R.drawable.man_senvac);
                prefsEditor.putString("TIPOLOGIA", "4").commit();
            }
        });

        v2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //1:1-6 2:1-1 3:1-2 4:1-3
                v1.setBackgroundResource(R.drawable.v1);
                v2.setBackgroundResource(R.drawable.v2a);
                v3.setBackgroundResource(R.drawable.v3);
                manipolo.setBackgroundResource(R.drawable.man_vac);
                seleziona_intervallo();
            }
        });

        v3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundResource(R.drawable.v1);
                v2.setBackgroundResource(R.drawable.v2);
                v3.setBackgroundResource(R.drawable.v3a);
                manipolo.setBackgroundResource(R.drawable.man_vac);
                prefsEditor.putString("TIPOLOGIA", "5").commit();
            }
        });

        min1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frequenza == 0) {
                    frequenza = 0;
                    freq1.setText("0");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("FREQ1", frequenza).commit();
                } else {
                    frequenza = frequenza - 1;
                    freq1.setText(frequenza + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("FREQ1", frequenza).commit();
                }
            }
        });

        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frequenza >= 16) {
                    frequenza = 16;
                    freq1.setText(16 + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("FREQ1", frequenza).commit();
                } else {
                    frequenza = frequenza + 1;
                    freq1.setText(frequenza + "");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("FREQ1", frequenza).commit();
                }
            }
        });


        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                } else if (tempo >= 60) {
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo;
                    System.out.println(t);
                } else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;
                String t = "" + tempo + "minuti";
                textView.setText(tempo + ":00");
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                String t = "" + tempo + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo + "minuti";
                System.out.println(t);
            }
        });
    }

    public void seleziona_intervallo() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.intervallo);
        dialog.setCancelable(true);

        //Button a = findViewById(R.id.a);
        dialog.setCancelable(true);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        Button btn1 = (Button) dialog.findViewById(R.id.button1);
        Button btn2 = (Button) dialog.findViewById(R.id.button2);
        Button btn3 = (Button) dialog.findViewById(R.id.button3);
        Button btn4 = (Button) dialog.findViewById(R.id.button4);
        title.setText(getString(R.string.interval) + "");
        btn1.setText("1:1");
        btn2.setText("2:1");
        btn3.setText("3:1");
        btn4.setText("4:1");

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "500").commit();
                //a.setText("500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "6");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "500").commit();
                //a.setText("500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "1");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "1000").commit();
                //a.setText("1000 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "2");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "1500").commit();
                //a.setText("1500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "3");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
        TextView textView_titolo = (TextView) findViewById(R.id.textView4);
        v1 = (Button) findViewById(R.id.button10);
        v2 = (Button) findViewById(R.id.button11);
        v3 = (Button) findViewById(R.id.button13);
        manipolo = (Button) findViewById(R.id.select_manipolo);
        manipolo.setEnabled(false);
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        textView_titolo.setText(prefs.getString("Nome Trattamento", getString(R.string.imm_dir)));
        tempo = prefs.getInt("TEMPO", 0);
        prefsEditor.putInt("Tempo", tempo).commit();
        textView.setText(tempo + ":00");
        seekBar.setProgress(tempo);

    }


    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        tempo = prefs.getInt("TEMPO", 10);
        int freq1 = prefs.getInt("FREQ1", 0);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (freq1 != 0) {
            prefsEditor.putInt("FREQ2", freq1).commit();
            prefsEditor.putInt("FREQ3", freq1).commit();
        }
        tipologia = prefs.getString("TIPOLOGIA", "x");

        if (tempo_trattamento == 0 || freq1 == 0 || tipologia.equals("x")) {
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            final float f = actualVolume / maxVolume;
            System.out.println("OOOOKKKKK " + f);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {

            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta_ou.class);
            startActivityForResult(CauseNelleVic, 0);

        }
    }

    public int hex(int i) {
        int to_calc = i;
        String hex = Integer.toHexString(to_calc);
        System.out.println("hex" + hex);
        int foo = Integer.parseInt(hex);
        return foo;
    }


    /*funzioni fisse per tutti */
    public void back(View view) {

        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
        finish();
    }

    public void home(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
        finish();
    }

}