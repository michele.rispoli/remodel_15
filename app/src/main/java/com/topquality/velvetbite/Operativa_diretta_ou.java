package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android_serialport_api.SerialPort;

import static android.media.MediaExtractor.MetricsConstants.FORMAT;

public class Operativa_diretta_ou extends Activity {

    private static final String TAG = "OU_DB";
    String msg = null;
    LogFile log = new LogFile();
    String tolog;
    String nome_trattamento_db;
    int tempo_sel;

    Button v1;
    Button v2;
    Button v3;
    Button manipolo;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Operativa_diretta_ou.ReadThread mReadThread;
    private Operativa_diretta_ou.SendThread mSendThread;
    private Operativa_diretta_ou.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();

    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private static final String Sserialport = "COM0"; //nome porta  - non uso più il nome
    private static final int m_iSerialPort = 0;  //id_seriale
    private static final int baudrate = 57600;  //baud rate
    private static final int databits = 8;  //  dati
    private static final int stopbits = 1;  // stop
    private static final int parity = 'n';  // parità NESSUNA (esiste anche pari o dispari)

    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";
    String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";
    String pausa  = "3E-FE-01-58-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-F4-CC-3C";
    String riprendi = "3E-FE-01-59-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-C5-3C-3C";

    long startTime;
    long endTime;
    long tempoTrattamento;
    long tempocontatori;
    long update_contatori;

    int volume = 0;
    int polarita = 0;
    int tempo_cut;
    int tempo_count1;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;
    String hex;
    Operativa_diretta_ou.MyCount counter;
    Operativa_diretta_ou.MyCount1 counter1;
    Long s1;
    Long s2;
    int status = 0;
    TextView text1;

    /*                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("FREQ1", c.getInt(freq1));
                prefsEditor.putInt("FREQ2", c.getInt(freq2));
                prefsEditor.putInt("FREQ3", c.getInt(freq3));
                prefsEditor.putString("TIPOLOGIA", c.getString(tipologia));*/
    String trattamento;
    public int tempo = 0;
    int energy = 60;
    TextView enerhy_tv;
    int freq1 = 0;
    int freq2 = 0;
    int freq3 = 0;
    String tipologia;

    TextView titolo;


    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }
            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);
        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Operativa_diretta_ou.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operativa_diretta_ou);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }

        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            Log.d("VELVETERROR", e.getMessage());
        }

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        nome_trattamento_db = prefs.getString("Nome Trattamento", null);
        text1 = (TextView)findViewById(R.id.count_down);

        /*contatori utilizzo macchina*/
        final SharedPreferences res = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor resEditor = res.edit();
        startTime = System.currentTimeMillis();
        resEditor.putLong("Start time", startTime);
        /*contatori utilizzo macchina*/

        initializeVariables();
        LinearLayout layout_vac = findViewById(R.id.layout_vac);
        int trattamento_1 = prefs.getInt("trattamento_1", 0);
        if (trattamento_1 == 1) {
            layout_vac.setVisibility(View.VISIBLE);
        }
        else if (trattamento_1 == 3) {
            layout_vac.setVisibility(View.GONE);
        }
        else if (trattamento_1 == 5) {
            layout_vac.setVisibility(View.GONE);
        }
        SharedPreferences.Editor prefsEditor = prefs.edit();

        prefsEditor.commit();
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min1);

        Button add = (Button) findViewById(R.id.add1);
        energy = prefs.getInt("ENERGY", 60);
        //textView.setText("0:00");

        /*BOTTONE STOP */
        Button stop = (Button) findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
            }
        });

        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                energy = prefs.getInt("ENERGY", 60);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                if (energy == 60) {
                    energy = 60;
                    prefsEditor.putInt("ENERGY", 60).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "01", 0);
                } else if (energy == 90) {
                    energy = 60;
                    prefsEditor.putInt("ENERGY", 60).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "01", 0);
                }else if (energy == 120) {
                    energy = 90;
                    prefsEditor.putInt("ENERGY", 90).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "02", 0);
                }else if (energy == 185) {
                    energy = 120;
                    prefsEditor.putInt("ENERGY", 120).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "03", 0);
                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                energy = prefs.getInt("ENERGY", 60);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                if (energy == 60) {
                    energy = 90;
                    prefsEditor.putInt("ENERGY", 90).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "02", 0);
                } else if (energy == 90) {
                    energy = 120;
                    prefsEditor.putInt("ENERGY", 120).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "03", 0);
                }else if (energy == 120) {
                    energy = 185;
                    energy = prefs.getInt("ENERGY", 60);
                    prefsEditor.putInt("ENERGY", 185).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "04", 0);
                }else if (energy == 185) {
                    energy = 185;
                    energy = prefs.getInt("ENERGY", 60);
                    prefsEditor.putInt("ENERGY", 185).commit();
                    enerhy_tv.setText(""+ energy);
                    pacchetto_da_inviare("15", "04", 0);
            }
            }
        });
    }

    /* cio che avviene se clicco stop */
    private void stop() {
        SendMsg(pausa);
        counter.cancel();
        counter1.cancel();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.pausa);
        dialog.setCancelable(false);
        TextView txt2 = (TextView) dialog.findViewById(R.id.title);
        txt2.setText(R.string.pausa);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.pausa_tratt1);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter= new Operativa_diretta_ou.MyCount(s1,1000);
                counter1= new Operativa_diretta_ou.MyCount1(s2,1000);
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.commit();
                SendMsg(riprendi);
                counter.start();
                counter1.start();
                dialog.dismiss();
            }
        });
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.commit();
                try {
                    Thread.sleep(200);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                SendMsg(stop_mio);

                try {
                    Thread.sleep(100);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                endTime = System.currentTimeMillis();
                final SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
                final SharedPreferences.Editor prefsEditor2 = prefs1.edit();
                tempoTrattamento = (endTime - startTime)/1000;
                tempocontatori = prefs.getLong("CONTATORI EL", 0);
                update_contatori = tempocontatori + tempoTrattamento;
                prefsEditor2.putLong("CONTATORI EL", update_contatori);
                prefsEditor2.commit();
                contatori();
                closeSerialPort();
                closeCounter();
                counter.cancel();
                counter1.cancel();
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        });
        dialog.show();
    }
    public void closeCounter () {
        counter.cancel();
        counter1.cancel();

    }
    /* cio che avviene allo scadere del tempo */
    public void stop1(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.commit();
        SendMsg(stop_mio);
        System.out.println("Pacchetto inviato di stop:" + stop_mio);
        endTime = System.currentTimeMillis();
        final SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor2 = prefs1.edit();
        tempoTrattamento = (endTime - startTime)/1000;
        tempocontatori = prefs.getLong("CONTATORI RF", 0);
        update_contatori = tempocontatori + tempoTrattamento;
        prefsEditor2.putLong("CONTATORI RF", update_contatori);
        prefsEditor2.commit();
        contatori();
        bip();
        try {
            Thread.sleep(100);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.stop);
        dialog.setTitle(R.string.fine_tratt);
        dialog.setCancelable(false);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.fine_tratt1);
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setText(R.string.fine_tratt3);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSerialPort();
                closeCounter();
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);

            }
        });
        dialog.show();
    }

    public void bip(){

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(this, R.raw.fine_trattamento, 0);
        spoolId = spool.load(this, R.raw.fine_trattamento, 0);
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float f = actualVolume / maxVolume;
        System.out.println("OOOOKKKKK " + f);
        spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                spool.play(spoolId, f, f, 1, 0, 1);
            }
        });
        spool.play(spoolId, f, f, 1, 0, 1);
    }

    public void contatori(){
        endTime = System.currentTimeMillis();
        long millisDiff = endTime - startTime;
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        prefsEditor.putLong("Tempo parziale ou", millisDiff);
        prefsEditor.commit();
        int seconds = (int) (millisDiff / 1000 % 60);
        int minutes = (int) (millisDiff / 60000 % 60);
        int hours = (int) (millisDiff / 3600000 % 24);
        int days = (int) (millisDiff / 86400000);

        Long tempoOLD = reserved.getLong("TEMPO UTILIZZO ONDEDURTO", 0);
        Long tempoTOT = tempoOLD + millisDiff;
        prefsEditor.putLong("TEMPO UTILIZZO ONDEDURTO", tempoTOT);
        prefsEditor.commit();
        int seconds1 = (int) (tempoTrattamento % 60);
        int minutes1 = (int) (tempoTrattamento / 60);
        String tolog1 = minutes1 + " M " + seconds1 + "S";
        tolog = nome_trattamento_db + "Tempo= " + tolog1;
        log.Log(tolog, Operativa_diretta_ou.this);

        System.out.println(days + " days, ");
        System.out.println(hours + " hours, ");
        System.out.println(minutes + " minutes, ");
        System.out.println(seconds + " seconds");
    }

    public class MyCount extends CountDownTimer
    {
        public MyCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);

           //pacchetto_da_inviare("36", "00-00", 0);
        }
        @Override     public void onFinish()
        {
            text1.setText("00:00");
            stop1();
        }
        @Override     public void onTick(long millisUntilFinished)
        {
            s1=millisUntilFinished;
            int seconds = (int) (s1 / 1000 % 60);
            int minutes = (int) (s1 / 60000 % 60);

            text1.setText(""+ minutes + ":"+seconds);

            String sec = seconds +"";

            if (sec.length() == 1){
                sec = "0"+seconds;
            }else{
                sec = seconds +"";
            }
            text1.setText(""+ minutes + ":"+sec);
        }
    }

    public class MyCount1 extends CountDownTimer
    {
        public MyCount1(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
        }
        @Override     public void onFinish()
        {

            final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
            if (status == 0){
                //invia freq2
                status = 1;
                System.out.println("invio freq2");
                freq2 = prefs.getInt("FREQ2", 2);
                String freq_tosend = calcola_frequenza(freq2);
                pacchetto_da_inviare("16", freq_tosend, 0); // frequenza (1-16)
                counter1.cancel();
                counter1= new Operativa_diretta_ou.MyCount1(tempo_cut, 1000);
                counter1.start();
            }else if (status == 1){
                //invia freq3
                System.out.println("invio freq3");
                status = 2;
                freq3 = prefs.getInt("FREQ3", 1);
                String freq_tosend = calcola_frequenza(freq3);
                pacchetto_da_inviare("16", freq_tosend, 0); // frequenza (1-16)
                counter1.cancel();
                counter1= new Operativa_diretta_ou.MyCount1(tempo_cut, 1000);
                counter1.start();
            }
            else if (status == 2){
                //stop
                System.out.println("stop");
                counter1.cancel();
            }
        }
        @Override     public void onTick(long millisUntilFinished)
        {
            s2=millisUntilFinished;

        }
    }

    /*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
   /* public String pacchetto_da_inviare( String reg, String mess, int comando){
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        int byte_val = 0;
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot = null;
        *//*calcola i byte validi *//*
        int lung_mess = mess.length();
        System.out.println("CHE LUNGHEZZA HAI?????????????????????????1 " + lung_mess);

        if (lung_mess == 2) {
            byte_val = 2;
            int lung_mess1 = messaggio.length();
            if (lung_mess1%2 == 0) {
                msg_tot = messaggio +"-00";
                System.out.println("CHE HAI?????????????????????????1 " + msg_tot);
            }else if (lung_mess1%2 == 1){
                msg_tot = "0"+messaggio+"-00";
                System.out.println("CHE HAI??????????????????????????2 " + msg_tot);
            }
        }else if (lung_mess == 3){
            byte_val = 3;
            String mess1 = "00"+messaggio + "0";
            System.out.println("IL MESSAGGIO con lo 0 è: " + mess1);
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 4){
            byte_val = 3;
            String mess1 = "0"+messaggio + "0";
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 1){
            byte_val = 2;
            String mess1 = "0"+messaggio+"-00";
            msg_tot = mess1;
        }

        if (reg.equals("30")){
            byte_val = 3;
        }if (reg.equals("19")){
            byte_val = 3;
        }if (reg.equals("17")){
            byte_val = 3;
        }
        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"
        System.out.println("UTILIZZABILI "+ byteUtilizzabili);
        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }

        //numero di byte 00 per completare il pacchetto
        int bitZero = 14;
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;

        System.out.println("!!!!!!!!!!!!!  - " +data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }*/


    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot = null;

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 3;
        /*if (lung_mess/2 == 4) {
            byte_val = 2;
        }else{
            byte_val = 2;
        }*/

        if (lung_mess == 2) {
            byte_val = 2;
            int lung_mess1 = messaggio.length();
            if (lung_mess1%2 == 0) {
                msg_tot = messaggio +"-00";
                System.out.println("CHE HAI?????????????????????????1 " + msg_tot);
            }else if (lung_mess1%2 == 1){
                msg_tot = "0"+messaggio+"-00";
                System.out.println("CHE HAI??????????????????????????2 " + msg_tot);
            }
        }else if (lung_mess == 3){
            byte_val = 3;
            String mess1 = "00"+messaggio + "0";
            System.out.println("IL MESSAGGIO con lo 0 è: " + mess1);
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 4){
            byte_val = 3;
            String mess1 = "0"+messaggio + "0";
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 1){
            byte_val = 2;
            String mess1 = "0"+messaggio+"-00";
            msg_tot = mess1;
        }

        if (reg.equals("30")){
            byte_val = 3;
        }if (reg.equals("19")){
            byte_val = 3;
        }if (reg.equals("17")){
            byte_val = 3;
        }

        int lung_mess1 = messaggio.length();
        if (lung_mess1 == 5) {
            msg_tot = messaggio;
            System.out.println("CHE HAI?????????????????????????1 " + msg_tot);
        }else if (lung_mess1 == 4){
            msg_tot = "0"+messaggio;
            System.out.println("CHE HAI??????????????????????????2 " + msg_tot);
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }

        //numero di byte 00 per completare il pacchetto
        int bitZero = 14;
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;

        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String dividi_msg(String mess){
        String msg_div = null;

        String val1 = mess.substring(1,3);
        System.out.println("LA SOTTOSTRINGA1 è: " + val1);

        String val2 = mess.substring(3,5);
        System.out.println("LA SOTTOSTRINGA2 è: " + val2);

        msg_div = val2 + "-" + val1;

        return msg_div;

    }

    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }


    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }


    /******************************************SERIALE *******************************************/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onPause in");
        super.onPause();
        Log.i(TAG, "==>onPause out");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onStop in");
        super.onStop();
        Log.i(TAG, "==>onStop out");
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onDestroy in");
        closeSerialPort();
       closeCounter();
        super.onDestroy();

        Log.i(TAG, "==>onDestroy out");
    }

    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG+" LEGGO da thread: " + to_check);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {


        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }
    /***********************SERIALE ********************************/


    private void initializeVariables() {
        titolo = (TextView) findViewById(R.id.textView4);
        titolo.setText(nome_trattamento_db);
        text1 = (TextView)findViewById(R.id.count_down);
        v1 = (Button) findViewById(R.id.button10);
        v2 = (Button) findViewById(R.id.button11);
        v3 = (Button) findViewById(R.id.button13);
        manipolo = (Button) findViewById(R.id.select_manipolo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);

        tempo_sel = prefs.getInt("Tempo", 0);
        final int tempo_count = tempo_sel * 60000;

        tipologia = prefs.getString("TIPOLOGIA", "x");
        if (tipologia.equals("1") || tipologia.equals("2") || tipologia.equals("3") || tipologia.equals("6") ) {
            //1:1-6 2:1-1 3:1-2 4:1-3
            v1.setBackgroundResource(R.drawable.v1);
            v2.setBackgroundResource(R.drawable.v2a);
            v3.setBackgroundResource(R.drawable.v3);
            manipolo.setBackgroundResource(R.drawable.man_vac);
        } else if (tipologia.equals("4")) {
            v1.setBackgroundResource(R.drawable.v1a);
            v2.setBackgroundResource(R.drawable.v2);
            v3.setBackgroundResource(R.drawable.v3);
            manipolo.setBackgroundResource(R.drawable.man_senvac);
        }else if (tipologia.equals("5")) {
            v1.setBackgroundResource(R.drawable.v1);
            v2.setBackgroundResource(R.drawable.v2);
            v3.setBackgroundResource(R.drawable.v3a);
            manipolo.setBackgroundResource(R.drawable.man_vac);
        }
        energy = 60;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("ENERGY", 60).commit();
        enerhy_tv = (TextView) findViewById(R.id.textView2);
        enerhy_tv.setText(""+ energy);
        //textView = (TextView) findViewById(R.id.textView1);
        tempo_cut = dividi_tempo(tempo_count);
        status = 0;
        tempo_sel = prefs.getInt("Tempo", 0);

        System.out.println("TEMPO SEL = " + tempo_sel);
        updateBarHandler = new Handler();
        tempo_count1 = tempo_sel * 60000;
        counter1= new Operativa_diretta_ou.MyCount1(tempo_cut, 1000);
        counter1.start();
        counter= new Operativa_diretta_ou.MyCount(tempo_count, 1000);
        counter.start();

/*        new CountDownTimer(tempo_count, 1000) {

            public void onTick(long millisUntilFinished) {
                SimpleDateFormat df = new SimpleDateFormat("mm:ss", Locale.ITALIAN);
                String time = df.format(new Date());
                text1.setText(time);
            }

            public void onFinish() {
                text1.setText("done!");
            }

        }.start();*/
        updateBarHandler = new Handler();

        text1=(TextView)findViewById(R.id.count_down);
        //final int tempo_count = tempo_sel * 60000;
//        counter= new Operativa_diretta_ou.MyCount(tempo_count, 1000);
//        counter.start();
        freq1 = prefs.getInt("FREQ1", 1);
        String freq_tosend = calcola_frequenza(freq1);
        pacchetto_da_inviare("16", freq_tosend, 0); // frequenza (1-16)
        tipologia = prefs.getString("TIPOLOGIA", "0");

        pacchetto_da_inviare("14", "01", 0); // mod onde d'urto
        pacchetto_da_inviare("15", "01", 0); // potenza 1-4: 60, 90, 120, 185
        int pompa = calcola_pompa(tipologia);
        if (pompa == 2){
            pacchetto_da_inviare("1B", "02", 0); // pompa: 0 spenta, 1 sempre attiva, 2 ad intervalli
            if (tipologia.equals("1")){ //2:1
                pacchetto_da_inviare("17", "D0-07", 0); // pompa:attiva ad intervalli 1000s - impulso
                pacchetto_da_inviare("19", "E8-03", 0); // pompa:attiva ad intervalli 1000s - pausa
            }else if (tipologia.equals("2")){ //3:1
                pacchetto_da_inviare("17", "B8-0B", 0); // pompa:attiva ad intervalli 1000s - impulso
                pacchetto_da_inviare("19", "E8-03", 0); // pompa:attiva ad intervalli 1000s - pausa
            }else if (tipologia.equals("3")){ //4:1
                pacchetto_da_inviare("17", "A0-0F", 0); // pompa:attiva ad intervalli 1000s - impulso
                pacchetto_da_inviare("19", "E8-03", 0); // pompa:attiva ad intervalli 1000s - pausa
            }else if (tipologia.equals("6")){ //1:1
                pacchetto_da_inviare("17", "E8-03", 0); // pompa:attiva ad intervalli 1000s - impulso
                pacchetto_da_inviare("19", "E8-03", 0); // pompa:attiva ad intervalli 1000s - pausa
            }
        }else{
            pacchetto_da_inviare("1B", "0"+pompa, 0); // pompa: 0 spenta, 1 sempre attiva, 2 ad intervalli
        }
        SendMsg(start);
    }


    public int dividi_tempo(int tem){
        int tmp = tem /3;
        System.out.println("tmp= " + tmp);
        return tmp;
    }

    public String calcola_frequenza(int freq){
        String freq_tosend = null;
        freq_tosend = Integer.toHexString(freq);
        if (freq_tosend.length() == 1){
            freq_tosend = "0" + freq_tosend.toUpperCase();
        }else if(freq_tosend.length() == 2){
            freq_tosend = freq_tosend.toUpperCase();
        }

        System.out.println("freq_tosend= " + freq_tosend);
        return freq_tosend;
    }

    public int calcola_pompa(String tipologia){
        int mod_pompa = 0;
        if (tipologia.equals("4")){
            mod_pompa = 0;
        }else if (tipologia.equals("5")){
            mod_pompa = 1;
        }else {
            mod_pompa = 2;
        }
        return mod_pompa;
    }


    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        int impulso_scelto = prefs.getInt("Impulso", 10);
        polarita = prefs.getInt("Polarita", 0);
        hex = Integer.toHexString(impulso_scelto).toUpperCase();


        if (tempo_trattamento == 0) {
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            final float f = actualVolume / maxVolume;
            System.out.println("OOOOKKKKK " + f);
            spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                    spool.play(spoolId, f, f, 1, 0, 1);
                }
            });
            spool.play(spoolId, f, f, 1, 0, 1);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {

            Context context = getBaseContext();
            closeSerialPort();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta_ou.class);
            finish();
            startActivityForResult(CauseNelleVic, 0);
        }
    }

    public int hex (int i) {
        int to_calc = i;
        String hex = Integer.toHexString(to_calc);
        System.out.println("hex" + hex);
        int foo = Integer.parseInt(hex);
        return foo;
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}