package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by Oana S. Lele on 15/06/2016.
 * Tel: +39 334 54 97 054
 * Email: leleoanasvetlana@gmail.com
 **/
public class Disclaimer extends Activity {

    private static final int DIALOG_ALERT_ID = 1;
    static final int CONFIRM_DIALOG = 0;
    int vol_audio = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disclaimer);
        //volume();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        vol_audio = prefs.getInt("Volume", 1);
    }

    /* GESTIONE VOLUME */
    public void volume(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
        vol_audio = prefs.getInt("Volume", 1);
        System.out.println("Volume =" + vol_audio);
        final Button volume= (Button) findViewById(R.id.volume);
        if (vol_audio == 0) {
            volume.setBackgroundResource(R.drawable.vol_off);
            System.out.println("volume");
        } else {
            volume.setBackgroundResource(R.drawable.vol_on);
            System.out.println("muto");
        }
        //volume.setBackgroundResource(R.drawable.vol_on);
        volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vol_audio == 0) {
                    vol_audio = 1;
                    volume.setBackgroundResource(R.drawable.vol_off);
                    System.out.println("muto");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Volume", vol_audio);
                    prefsEditor.commit();
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                } else{
                    vol_audio = 0;
                    volume.setBackgroundResource(R.drawable.vol_on);
                    System.out.println("volume");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Volume", vol_audio);
                    prefsEditor.commit();
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                }
            }
        });
    }
    /* MENU MEDICINA ESTETICA*/
    public void continua(View view) {
        showDialog(CONFIRM_DIALOG);
    }

    /* CONTROLLO MESSAGGIO AUDIO*/
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
            case CONFIRM_DIALOG:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.sicuro);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.si1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Disclaimer.this, MenuPrinc.class);
                        startActivity(intent);
                        finish();
                    }
                });
                builder.setNegativeButton(R.string.no1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Disclaimer.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog = builder.create();
                break;
            default:
                dialog = null;
        }
        return dialog;
    }
    /* CONTATTA */
    public void microchirurgia(View view) {

        String t = getString(R.string.cont);
        System.out.println(t);
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Assistenza_tecnica.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    /* CONTINUA CMQ*/
    public void fisioterapia(View view) {
        String t = getString(R.string.continua);
        System.out.println(t);
        //t1(t, .QUEUE_FLUSH, null);
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Disclaimer.class);
        startActivityForResult(CauseNelleVic, 0);
    }

//    public void impostazioni(View view) {
//        String t = getString(R.string.sett);
//        System.out.println(t);
//        Context context = getBaseContext();
//        Intent CauseNelleVic = new Intent(context, Impostazioni.class);
//        startActivityForResult(CauseNelleVic, 0);
//    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    /* FUNZIONI FISSE */
//    public void settings(View view) {
//        Context context = getBaseContext();
//        Intent CauseNelleVic = new Intent(context, Impostazioni.class);
//        startActivityForResult(CauseNelleVic, 0);
//    }

    public void esci(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MainActivity.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}