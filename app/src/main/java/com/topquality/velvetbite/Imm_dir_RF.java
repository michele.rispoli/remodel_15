package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Imm_dir_RF  extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.immissione_diretta1);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        Button immissione_diretta = findViewById(R.id.immissione_diretta);
        Button seleziona_trattamento = findViewById(R.id.seleziona_trattamento);
        //seleziona_trattamento.setEnabled(false);
        Button assistenza = findViewById(R.id.assistenza);
        int trattamento = prefs.getInt("trattamento", 0);

        immissione_diretta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento == 1){
                    Intent i = new Intent(getApplicationContext(), Imm_dir_RF.class);
                    startActivity(i);
                    finish();
                }else if (trattamento == 2){
                    Intent i = new Intent(getApplicationContext(), Imm_dir_EL.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        seleziona_trattamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento == 1){
                    Intent i = new Intent(getApplicationContext(), Trattamenti_rf.class);
                    startActivity(i);
                    finish();
                }else if (trattamento == 2){
                    Intent i = new Intent(getApplicationContext(), Trattamenti_el.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        assistenza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Assistenza_tecnica.class);
                startActivity(i);
                finish();
            }
        });

    }

    public void back(View view) {
        finish();
//        Intent intent = new Intent(this, MenuPrinc.class);
//        startActivity(intent);
    }
    public void informazioni (View view) {
        finish();
        startActivity(new Intent(getApplicationContext(),Informazioni.class));
    }
}