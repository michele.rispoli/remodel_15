package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

import android_serialport_api.SerialPort;

public class TestCartuccia extends Activity {
    private static final String TAG = "TEST HIFU";

    int k= 0;


    /*Hifu rev2*/
    String start_h2 = "AA-73-03-89-00-8C-CC-33-C3-3C";
    String start_h2a = "AA-72-03-89-00-8C-CC-33-C3-3C";
    String stop1 = "AA-73-03-C7-00-30-CC-33-C3-3C";
    String stop2 = "AA-72-03-C7-00-30-CC-33-C3-3C";


    public int lenght = 10;
    LogFile log = new LogFile();
    int counter = 0;
    public int step = 25;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;
    int code;

    boolean testina_inserita = false;
    boolean chiudi_tutorial = false;

    /***********************SERIALE*************************/


    private TestCartuccia.SendThread mSendThread;
    private TestCartuccia.TimerSendThread mTimerSendThread;


    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";

    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    String msg = null;
    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();

    private SerialPort mSerialPort = null;




    Boolean test_res = false;
    public Integer[] tmp = {-1, -1, -1, -1};
    int pass;



    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600; //Integer.decode(sp.getString("BAUDRATE", "-1"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new TestCartuccia.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    /**************************SERIALE*********************************/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testcartuccia);


        /**************************SERIALE*********************************/
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SendMsg("3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C");
        /**************************SERIALE*********************************/
        pacchetto_da_inviare("14", "01", 0);
        //SendMsg(start);
        ImageView imageView5 = findViewById(R.id.imageView5);
        imageView5.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (counter == 5){
                    //Toast toast = Toast.makeText(getApplicationContext(), "CIAOOOOOOONE", Toast.LENGTH_SHORT);
                    //toast.show();
                    log.Log("Accesso Operativa Hifu SENZA CARTUCCIA!", TestCartuccia.this);
                    Context context = getBaseContext();
                    Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
                    startActivityForResult(CauseNelleVic, 0);
                    counter = 0;
                } else {
                    counter++;
                    System.out.println("counter = " + counter);
                }
                return true;
            }
        });

        //Shot = (TextView) findViewById(R.id.shot);
        //ShotRimasti = (TextView) findViewById(R.id.shot_rimasti);
        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(TestCartuccia.this, R.raw.doorbellup, 0);
        spoolId = spool.load(TestCartuccia.this, R.raw.doorbellup, 0);
        final SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        code = prefs.getInt("CODE_HIFU", 0);


        updateBarHandler = new Handler();
    }


    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String messaggio = traduci_messaggio(mess);// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        if (reg.equals("14")){
            byte_val=2;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String pacchetto_da_inviare1( String reg, String mess, int comando){
        String messaggio = mess;// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }


    public String traduci_messaggio(String string){
        String messaggio = null;
        int foo = Integer.parseInt(string);

        byte[] byteStr = new byte[8];
        System.out.println(Integer.toHexString(foo));
        byteStr[0] = (byte) ((foo & 0x00ff));
        byteStr[1] = (byte) ((foo & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else if (bLow.length()==0){
            bLow = "00";
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        messaggio = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(messaggio.toUpperCase() + " CALCOLO FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return messaggio.toUpperCase();
    }
    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive
        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        //System.out.println(bLow + " è giusto anch questo ? \n");
        //System.out.println(bHigh + " è giusto anch questo 4444? \n");
        //System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }

    private void analizzaPacchetto(String stringa, int dimensione){
        String stringa_arrivata = stringa;
        String stringa_pacchetto = null;
        System.out.println("LA STRINGA CHE ANALIZZO è -->" + stringa_arrivata);
        SharedPreferences prefs = getSharedPreferences("Hifu_val", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (stringa_arrivata.contains("3E 01 FE 52 02 25 03 ")){
            //testina ds 7-3.0
            System.out.println("testina ds 7-3.0");
            prefsEditor.putInt("Testina", 2);
            prefsEditor.commit();
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
            startActivityForResult(CauseNelleVic, 0);

        }else if (stringa_arrivata.contains("3E 01 FE 52 02 25 02 ")){
            //testina ds 10-1.5
            System.out.println("testina ds 10-1.5");
            prefsEditor.putInt("Testina", 1);
            prefsEditor.commit();
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
            startActivityForResult(CauseNelleVic, 0);

        }else if(stringa_arrivata.contains("3E 01 FE 52 02 25 07 ")){
            //testina ds 4-8.0
            System.out.println("testina ds 4-8.0");
            prefsEditor.putInt("Testina", 4);
            prefsEditor.commit();
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
            startActivityForResult(CauseNelleVic, 0);

        }else if(stringa_arrivata.contains("3E 01 FE 52 02 25 08 ")){
            //testina 4-13.0
            System.out.println("testina 4-13.0");
            prefsEditor.putInt("Testina", 5);
            prefsEditor.commit();
            //closeSerialPort();
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
            startActivityForResult(CauseNelleVic, 0);


        }else if(stringa_arrivata.contains("3E 01 FE 52 02 25 06 ")){
            //testina ds 4-4.5
            System.out.println("testina ds 4-4.5");
            prefsEditor.putInt("Testina", 3);
            prefsEditor.commit();
            //closeSerialPort();
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, OperativaHifu.class);
            startActivityForResult(CauseNelleVic, 0);
        }
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " +mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size>0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println("LEGGO da thread - test: " + to_check + " size" + size);
                        //hard_ck(to_check);
                        analizzaPacchetto(to_check, size);
                    } catch (IOException e) {

                    }
                }
            }

        }

/*        public void hard_ck(String stringa) {
            if (stringa.length() == 0) {
                test_res = true;
            }else{
                test_res = false;
            }
        }*/


    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {
        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            try {
                Thread.sleep(30);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }
    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /***********************
     * SERIALE
     **********************/
    /**************************SERIALE*********************************/



    /*funzioni fisse per tutti */
    public void back (View view) {
        finish();
    }
    public void esci(View view) {
        //closeSerialPort();
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void informazioni(View view) {
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}
