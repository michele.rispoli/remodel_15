package com.topquality.velvetbite;

import android_serialport_api.SerialPort;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

public class Immissione_remoto2ME_db   extends Activity {
    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    private SeekBar seekBar;
    private TextView textView;
    public int tempo = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;
    TextView atxt;
    TextView btxt;
    TextView c1txt;
    TextView c2txt;
    TextView dtxt;



    private static final String TAG = "ME-EL-DB";
    String msg = null;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Immissione_remoto2ME_db.ReadThread mReadThread;
    private Immissione_remoto2ME_db.SendThread mSendThread;
    private Immissione_remoto2ME_db.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();


    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private static final String Sserialport = "COM0"; //nome porta  - non uso più il nome
    private static final int m_iSerialPort = 0;  //id_seriale
    private static final int baudrate = 57600;  //baud rate
    private static final int databits = 8;  // bit??? byte??? di dati
    private static final int stopbits = 1;  // bit??? byte??? di stop
    private static final int parity = 'n';  // parità NESSUNA (esiste anche pari o dispari)

    //elettroporazione
    String elettroporazione = "3E-FE-01-51-02-4D-02-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0E-04-3C";
    //String forma_onda_quadr = "3E-FE-01-51-02-2F-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-7E-6D-3C";
    //String config_elettropora = "3E-FE-01-51-02-2D-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-EB-83-3C";
    //start
    String  start_elettropora = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";
    //potenza 0
    String pot0_el = "3E-FE-01-51-02-2A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-74-EB-3C";


    //String impulso1 = "3E-FE-01-51-03-14-E8-03-00-00-00-00-00-00-00-00-00-00-00-00-00-5F-06-3C";
    //String periodo10 = "3E-FE-01-51-03-16-64-00-00-00-00-00-00-00-00-00-00-00-00-00-00-70-23-3C";

    /*********************************************SERIALE ****************************************/

    int a;
    int b;
    int c;
    int c1;
    int d;




    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Immissione_remoto2ME_db.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.immissione_remoto_2db);
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.setAlpha(0);
        videoView.postDelayed(new Runnable() {
            @Override
            public void run() {
                videoView.setAlpha(1f);
            }
        }, 300);
        videoView.setVisibility(View.VISIBLE);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            Log.d("VELVETERROR", e.getMessage());
        }


        SendMsg(elettroporazione);
        inviaPacchetti();
        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Immissione_remoto2ME_db.this, R.raw.errore, 0);
        spoolId = spool.load(Immissione_remoto2ME_db.this, R.raw.errore, 0);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        initializeVariables();

        TextView textView4 = (TextView) findViewById(R.id.textView4);
        String nome_trattamento_db = prefs.getString("Nome Trattamento", null);
        textView4.setText(nome_trattamento_db);
        volume();
        manipolo = 0;
        int leng = prefs.getInt("LINGUA", 1);

        /* inizializzazione dei paramentri */
        SharedPreferences.Editor prefsEditor = prefs.edit();
        /*                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("A", c.getInt(a));
                prefsEditor.putInt("B", c.getInt(b));
                prefsEditor.putInt("C1", c.getInt(c1));
                prefsEditor.putString("C2", c.getString(c2));
                prefsEditor.putString("D", c.getString(d));
                prefsEditor.putString("FORMA ONDA", c.getString(forma_onda));
                prefsEditor.putString("MANIPOLO", c.getString(manipolo));
                prefsEditor.putString("PIASTRA", c.getString(piastra));
                prefsEditor.putString("PEDALIERA",c.getString(pedaliera));*/
        tempo = prefs.getInt("TEMPO", 0);
        prefsEditor.putInt("Tempo", tempo);
        prefsEditor.commit();
        a = prefs.getInt("A", -1);
        b = prefs.getInt("B", -1);
        c = prefs.getInt("C1", -1);
        c1 = prefs.getInt("C2", -1);
        d = prefs.getInt("D", -1);



        atxt = findViewById(R.id.atxt);
        btxt = findViewById(R.id.btxt);
        c1txt = findViewById(R.id.c1txt);
        c2txt = findViewById(R.id.c2txt);
        dtxt = findViewById(R.id.dtxt);

        SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor sharEdit = shared.edit();


        atxt.setText(String.valueOf(shared.getInt("A",0)) + " " + "(us)");
        btxt.setText(String.valueOf(shared.getInt("B",0)) + " " + "(ms)");
        c1txt.setText(String.valueOf(shared.getInt("C1",0))+ " " + "(Hz)");
        if (shared.getInt("C1", 0) == 100) {
            c2txt.setText("110"+ " " + "(ms)");
        } else if (shared.getInt("C1", 0) == 150) {
            c2txt.setText("77"+ " " + "(ms)");
        } else if (shared.getInt("C1", 0) == 300) {
            c2txt.setText("38"+ " " + "(ms)");
        }
        dtxt.setText(String.valueOf(shared.getInt("D",0))+ " " + "(Hz)");

        if (d == 1) {
            sharEdit.putInt("D",1000).commit();
        }
        else if (d == 2) {
            sharEdit.putInt("D", 500).commit();
        }
        else if (d == 3) {
            sharEdit.putInt("D",333).commit();
        }
        else if (d == 4) {
            sharEdit.putInt("D",250).commit();
        }
        else if (d == 5) {
            sharEdit.putInt("D",200).commit();
        }

        /*var ausiliari */
        /* gestione del volume */
        vol_audio = prefs.getInt("Volume", 1);
        //prefsEditor.commit();
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);
        seekBar.setProgress(tempo);
        textView.setText(""+tempo+":00");
//        min.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (tempo == 0) {
//                    tempo = 0;
//                    textView.setText("0:00");
//                    seekBar.setProgress(tempo);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", tempo);
//                    prefsEditor.commit();
//
//                    String t = "" + tempo   + "minuti";
//                    System.out.println(t);
//                } else if (1 < tempo && tempo < 10) {
//                    System.out.println(tempo);
//                    tempo = tempo - 5;
//                    textView.setText(tempo + ":00");
//                    seekBar.setProgress(tempo);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", tempo);
//                    prefsEditor.commit();
//
//
//                    String t = "" + tempo   + "minuti";
//                    System.out.println(t);
//                } else {
//                    tempo = tempo - 5;
//                    textView.setText(tempo + ":00");
//                    seekBar.setProgress(tempo);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", tempo);
//                    prefsEditor.commit();
//
//
//                    String t = "" + tempo   + "minuti";
//                    System.out.println(t);
//                }
//                return true;
//            }
//        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });
//        add.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                if (1 < tempo && tempo < 10) {
//                    tempo = tempo + 5;
//                    textView.setText(tempo + ":00");
//                    textView.setText(tempo + ":00");
//                    seekBar.setProgress(tempo);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", tempo);
//                    prefsEditor.commit();
//
//                    String t = "" + tempo + "minuti";
//                    System.out.println(t);
//                }else if (tempo >= 60){
//                    tempo = 60;
//                    textView.setText(60 + ":00");
//                    seekBar.setProgress(60);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", 60);
//                    prefsEditor.commit();
//
//                    String t = "" + tempo + "minuti";
//                    System.out.println(t);
//                } else {
//                    tempo = tempo + 5;
//                    textView.setText(tempo + ":00");
//                    seekBar.setProgress(tempo);
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Tempo", tempo);
//                    prefsEditor.commit();
//
//                    String t = "" + tempo + "minuti";
//                    System.out.println(t);
//                }
//                return true;
//            }
//        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo;
                    System.out.println(t);
                }else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });

        /* GESTIONE SEEKBAR */
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }
        });
    }

    private void inviaPacchetti() {
        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }

        //SendMsg(forma_onda_quadr);
        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        //SendMsg(config_elettropora);
        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        //SendMsg(pot0_el);
    }


    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String messaggio = traduci_messaggio(mess);// messaggio che voglio andare a scrivere
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere

        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 4;
        }else{
            byte_val = lung_mess/2+1;
        }


        if (reg.equals("2F")){
            byte_val=2;
        }

        if (reg.equals("14")||reg.equals("35")||reg.equals("36")){
            byte_val=2;
        }else if (reg.equals("2D")||reg.equals("2F")||reg.equals("31")||reg.equals("33")){
            byte_val=3;
        }


        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }

        //byte_val = 2;


        //numero di byte 00 per completare il pacchetto
        int bitZero = 16 - (lung_mess/2);
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + messaggio  + stringa_zero;


        System.out.println("stringa = ********************************* " + data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);


        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        //System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }



    public String traduci_messaggio(String string){
        String messaggio = null;
        int foo = Integer.parseInt(string);

        byte[] byteStr = new byte[8];
        System.out.println(Integer.toHexString(foo));
        byteStr[0] = (byte) ((foo & 0x00ff));
        byteStr[1] = (byte) ((foo & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else if (bLow.length()==0){
            bLow = "00";
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        messaggio = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(messaggio.toUpperCase() + " CALCOLO FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return messaggio.toUpperCase();
    }





    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive
        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        //System.out.println(bLow + " è giusto anch questo ? \n");
        //System.out.println(bHigh + " è giusto anch questo 4444? \n");
        //System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }



    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }





    /******************************************SERIALE *******************************************/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onPause in");
        super.onPause();
        Log.i(TAG, "==>onPause out");
    }

    /*@Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onStop in");
        super.onStop();
        Log.i(TAG, "==>onStop out");
    }*/

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onDestroy in");
        closeSerialPort();
        super.onDestroy();
        Log.i(TAG, "==>onDestroy out");
    }

    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG+" LEGGO da thread: " + to_check);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {
        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /************************************SERIALE*************************************************/
    /* GESTIONE VOLUME */
    public void volume() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

    }

    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
    }

    public void start1(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        int a = prefs.getInt("A", 0);
        int aval = 0;
        if (a == 500 || a == 1000){
            aval = 1;
        }else if (a == 1500 || a == 2000){
            aval = 2;
        }
        //double a1= Double.parseDouble(a);
        int b = prefs.getInt("B", -1);
        int c = prefs.getInt("C1", -1);
        int c1 = prefs.getInt("C2", -1);
        int d = prefs.getInt("D", -1);
        if ( c== 300){
            c1 = 38;
        }else if ( c== 150){
            c1 = 77;
        }else if ( c== 100){
            c1 = 110;
        }

        int dval = 0;
        if (d == 1) {
            dval = 1000;
        }else if (d == 2) {
            dval = 500;
        }else if (d == 3) {
            dval = 333;
        }else if (d == 4) {
            dval = 250;
        }else if (d == 5) {
            dval = 200;
        }

        int cval = 0;
        if (c == 300){
            cval = 1;
        }else if (c == 150){
            cval = 2;
        }    else if (c == 100){
            cval = 3;
        }

        int calc_d = 0;
        int tmp = aval+b+c1;
        calc_d = d - tmp;
        System.out.println(calc_d + " valore di D calcolato + tmp = " + tmp );
        System.out.println("T =" +tempo_trattamento + " a=" + a + " b=" + b + " c=" + cval + " c1=" + c1 + " d=" + d);
//        if (tempo_trattamento == 0 ||  a == -1 || b == -1 || c == -1 || c1 == -1 || d == -1) {
        if (tempo_trattamento == 0 ||  aval == -1 || b == -1 || c == -1 || c1 == -1 || calc_d == -1){
            System.out.println("T =" +tempo_trattamento + " a=" + a + " b=" + b + " c=" + c + " c1=" + c1 + " d=" + d);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {

            pacchetto_da_inviare("14", "02", 0);
            pacchetto_da_inviare("1E", "03", 0); //forma d'onda
            pacchetto_da_inviare("20", ""+a, 0 ); // A
            pacchetto_da_inviare("22", ""+b, 0 ); // B
            pacchetto_da_inviare("24", "12", 0 ); // numero di sinusoidi 12 fisso
            pacchetto_da_inviare("26", ""+calc_d, 0 ); // numero di sinusoidi 12 fisso
            pacchetto_da_inviare("28", ""+cval, 0); // C 1=300, 2=150, 3=100
            //pacchetto_da_inviare("2A", "00", 0); // potenza
            SendMsg(start_elettropora);

            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta2me_el_db.class);
            startActivityForResult(CauseNelleVic, 0);
        }
    }



    /*public void start1(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        System.out.println("T =" +tempo_trattamento + " a=" + a + " b=" + b + " c=" + c + " c1=" + c1 + " d=" + d);
        if (tempo_trattamento == 0 ||  a == -1 || b == -1 || c == -1 || c1 == -1 || d == -1) {
            System.out.println("T =" +tempo_trattamento + " a=" + a + " b=" + b + " c=" + c + " c1=" + c1 + " d=" + d);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {
            String t = getString(R.string.init_tratt);
            System.out.println(t);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            pacchetto_da_inviare("14", ""+a , 0);
            pacchetto_da_inviare("16", ""+b , 0);
            pacchetto_da_inviare("18", ""+c , 0);
            pacchetto_da_inviare("14", ""+a , 0);
            pacchetto_da_inviare("14", ""+a , 0);
            SendMsg(impulso1);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(periodo10);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(pot0_el);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(start_elettropora);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(pot0_el);
            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta2me_el_db.class);
            startActivityForResult(CauseNelleVic, 0);
        }
    }*/


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void esci(View view){
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Trattamenti_el.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}
