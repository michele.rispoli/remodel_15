package com.topquality.velvetbite;


import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FileContatori {
    private String TAG="LogContatori";
    private String file ="data/data/com.topquality.remodel/contatori.txt";
    public void Log(String text, Context cntx) {
        String data="00/00/0000 00:00";
        data = getFormattedDate();
        Log.d(TAG, "Log: "+data+" "+text);
        try {
            File file =new File(this.file);
            if(!file.exists())  file.createNewFile();
            FileWriter fileWritter = new FileWriter(this.file,true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data+" "+text+"\r\n");
            bufferWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("LogFile" , e.getMessage());
        }
    }
    private String getFormattedDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(); //called without pattern
        return df.format(c.getTime());
    }

}
