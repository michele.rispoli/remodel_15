package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MenuPrinc extends Activity {

    Boolean test_res = false;
    public int k = 0;
    public Integer[] tmp = {-1, -1, -1, -1};
    int pass;

    String nome_dispositivo;
    private static final String TAG = "MainActivity";
    String msg = null;
    String id;
    String plc;

    private boolean m_bShowDateType = false;

    TextView testo;
    FileContatori log_conta = new FileContatori();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuprinc);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        nome_dispositivo = reserved.getString("TIPOLOGIA_DISPOSITIVO", getString(R.string.tip_macchina1));   //nome del dispositivo acquistato. di base è VS
        SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        id = shared.getString("id", null);
        plc = shared.getString("PLC", null);
        int db = reserved.getInt("DATABASE", 0);
        if (db == 1 && id != null && plc!=null && reserved.getInt("INDUSTRIA4",0) == 1) {
            //sviluppo/
            postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/database.php", id, plc, "1");
        }
        if (id != null && plc!=null) {
            postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/putStato.php", id, plc, "1");
        }
/*
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

        /******File contatori******/
        Long tempo_ou = reserved.getLong("TEMPO UTIILIZZO ONDEDURTO",0);
        Long tempo_vac = reserved.getLong("TEMPO UTILIZZO VAC",0);
        Long tempo_el = reserved.getLong("TEMPO UTILIZZO ELETTROPORAZIONE", 0);
        log_conta.Log("" + getString(R.string.el) + "|" + tempo_el, this);
        log_conta.Log("" + getString(R.string.ou) + "| " + tempo_ou,this);
        log_conta.Log("" + getString(R.string.vacuum) + "| " + tempo_vac,this);
        //TODO: aggiungere contatori hifu

        /******File contatori******/
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        Button hifu = findViewById(R.id.rf);
        Button elettroporazione = findViewById(R.id.el);
        Button vacuum = findViewById(R.id.vac);
        Button scrub = findViewById(R.id.scrub);
        Button software = findViewById(R.id.sw);

        hifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("trattamento", 1).commit();
                //closeSerialPort();
                Intent i = new Intent(getApplicationContext(), Trattamenti_pre.class);
                startActivity(i);
//                finish();
            }
        });

        elettroporazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nome_dispositivo.equals(getString(R.string.tip_macchina1))) {
                    bloccoPacchetto();
                }
                else {
                    //pacchetto_da_inviare("14", "02", 0);
                    prefsEditor.putInt("trattamento", 2).commit();
                    //closeSerialPort();
                    Intent i = new Intent(getApplicationContext(), Trattamenti_pre.class);
                    startActivity(i);
                }
            }
        });

        vacuum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"WORK IN PROGRESS! - Disponibile appena arriva l'elettronica corretta",Toast.LENGTH_SHORT).show();
                //pacchetto_da_inviare("14", "02", 0);
                prefsEditor.putInt("trattamento", 3).commit();
                Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                startActivity(i);
            }
        });

        scrub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //closeSerialPort();
                    Intent i = new Intent(getApplicationContext(), Imm_Diretta_ou.class);
                    startActivity(i);
                    finish();
            }
        });

        software.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CDS.class);
                startActivity(i);
            }
        });

        //ck_com();
        //avvia_macchina();
    }
    //"data/data/com.topquality.tqgswcombi/logfile.txt"
    //"data/data/com.topquality.tqgswcombi/contatori.txt"

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onResume() {
        super.onResume();
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        Button icWifi = findViewById(R.id.buttonWifi);
        icWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),WifiLogin.class));
            }
        });
        if (reserved.getInt("INDUSTRIA4", 0) == 1) {
            icWifi.setVisibility(View.VISIBLE);
        }else{
            icWifi.setVisibility(View.GONE);
        }
        if (id != null && plc != null && reserved.getInt("INDUSTRIA4", 0) == 1) {
            final Handler handler = new Handler();
            final Runnable r = new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(this, 300000);
                    postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/putStato.php", id, plc, "1");
                }
            };
            handler.postDelayed(r, 300000);
        }

        if (isOnline() && id != null && plc != null && reserved.getInt("INDUSTRIA4", 0) == 1) {

            StringBuilder textLog = new StringBuilder();
            try {
                File file = new File("data/data/com.topquality.remodel/logfile.txt");
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    textLog.append(line);
                    textLog.append("\n");
                }
                br.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
//preparo il file contatori per mandarlo al server
            StringBuilder textContatori = new StringBuilder();
            try {
                File file = new File("data/data/com.topquality.remodel/contatori.txt");
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    textContatori.append(line);
                    textContatori.append("\n");
                }
                br.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/putLog.php", id, plc, textLog.toString());
            postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/contatori.php", id, plc, textContatori.toString());
            postFile(getApplicationContext(), 2, "http://flendcompanysoftware.it/admin/macchinari/putVersione.php", id, plc, pInfo.versionName);
        }
        if (isOnline()) {
            icWifi.setBackgroundResource(R.drawable.icwifi);
        } else {
            icWifi.setBackgroundResource(R.drawable.iconwifi);
        }
    }

    /* funzione dedicata all'alert che blocca le funzionalità non incluse nel pacchetto */
    public void bloccoPacchetto(){

        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.no_lic);
        dialog.setTitle(R.string.no_licenza);
        dialog.setCancelable(false);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.fine_tratt1);
        Button continueButton = (Button) dialog.findViewById(R.id.button1);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, Assistenza_tecnica.class);
                startActivityForResult(CauseNelleVic, 0);
                dialog.dismiss();
            }
        });
        Button continueButton2 = (Button) dialog.findViewById(R.id.button2);
        continueButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /***********************
     * SERIALE
     **********************/

    /***********************
     * INDUSTRIA 4.0
     **********************/

    private void postFile(Context context, int method, String url, final String id, final String plc, final String requestBody) {
        try {
            final SharedPreferences shared = getSharedPreferences("Shared",MODE_PRIVATE);
            final SharedPreferences reserved = getSharedPreferences("Reserved",MODE_PRIVATE);
            RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) {
                    HttpURLConnection connection = null;
                    try {
                        connection = super.createConnection(url);
                        connection.setInstanceFollowRedirects(true);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return connection;
                }
            });
//            requestQueue.getCache().clear();
            StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("VOLLEY", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<String, String>();
//                    String credentials = shared.getString("email",null)+":"+reserved.getString("password",null);
//                    String auth = "Basic" + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Id-Utente", id);
                    headers.put("Plc-Macchinario", plc);
//                    headers.put("Authorization", auth);
                    return headers;
                }

                @Override
                public byte[] getBody() {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = " ";
                    if (response != null) {
                        responseString = (response.statusCode) + " " + (response.allHeaders) + " " + (response.networkTimeMs);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            boolean exitValue = ipProcess.waitFor(3000, TimeUnit.MILLISECONDS);
            return (exitValue);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
    /***********************
     * INDUSTRIA 4.0
     **********************/

    public void esci (View view){
        SharedPreferences res =getSharedPreferences("Reserved",MODE_PRIVATE);
        if(res.getInt("SKIPPASS",0)==0){
            Intent i = new Intent(getApplicationContext(), Password.class);
            startActivity(i);
            finish();
        }else{
            Intent intent= new Intent();
            openApp(getApplicationContext(),"com.tqg.topquality.launchertqg");
           /* intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finishAffinity();*/
        }

    }
    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        //finish();
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
            //new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }

/*    public void avvia_macchina() {
        int pass = ck_com();


        if(pass == 1){

            this.runOnUiThread(new Runnable() {
                public void run() {
                    testo.setText("OK");
                }
            });

        }else {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    testo.setText("KO");
                }
            });
        }

    }*/


}