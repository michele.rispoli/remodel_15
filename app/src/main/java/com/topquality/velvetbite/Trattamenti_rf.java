package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class Trattamenti_rf  extends Activity {
    int db;
    int tratt = 0;
    VelvetSkinIntraPlusDB_rf1 mDb;
    HorizontalScrollView scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_rf);
        scroll = findViewById(R.id.scroll);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        //
        mDb = new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        String SharedPrefName = "Reserved";

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = reserved.getInt("DATABASE", 0);

        prefsEditor.putInt("tratt_rf1", 1).commit();
/*        if (db == 1){

        }else {
        }*/

        View a = findViewById(R.id.la);
        View b = findViewById(R.id.lb);
        Button trat1 = findViewById(R.id.trat1);      //viso
        Button trat2 = findViewById(R.id.trat2);      //corpo
        Button trat3 = findViewById(R.id.trat3);      //frazionata
        trat1.setBackgroundResource(R.drawable.btn_1p);
        trat1.setTextColor(Color.BLACK);
        Button trat_a1 = findViewById(R.id.trattrat_a1);
        Button trat_a2 = findViewById(R.id.trat_a2);
        Button trat_a3 = findViewById(R.id.trat_a3);
        Button trat_a4 = findViewById(R.id.trat_a4);

        trat_a1.setText(R.string.ipmu);
        trat_a2.setText(R.string.rilcut);
        trat_a3.setText(R.string.rug);
        trat_a4.setVisibility(View.GONE);

        Button trat_b1 = findViewById(R.id.trattrat_b1);
        Button trat_b2 = findViewById(R.id.trat_b2);
        Button trat_b3 = findViewById(R.id.trat_b3);
        Button trat_b4 = findViewById(R.id.trat_b4);
        b.setVisibility(View.GONE);


        trat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("tratt_rf1", 1).commit();
                trat1.setBackgroundResource(R.drawable.btn_1p);
                trat1.setTextColor(Color.BLACK);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);

                trat_a1.setText(R.string.ipmu);
                trat_a2.setText(R.string.rilcut);
                trat_a3.setText(R.string.rug);
                trat_a4.setVisibility(View.GONE);
                b.setVisibility(View.GONE);


            }
        });


        trat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("tratt_rf1", 2).commit();
                trat_a4.setVisibility(View.VISIBLE);
                trat_a1.setText(R.string.lipdis);
                trat_a2.setText(R.string.adloc);
                trat_a3.setText(R.string.ipmu);
                trat_a4.setText(R.string.rilcut);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1p);
                trat2.setTextColor(Color.BLACK);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
            }
        });

        trat3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Immissione_diretta_frazionata.class);
                startActivity(i);
                finish();
            }
        });

        trat_a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 1).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1) {
                    b.setVisibility(View.GONE);
                    fai_query("1");
                } else if (tratt == 2) {

                    trat_a1.setBackgroundResource(R.drawable.btn_1p);
                    trat_a1.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);

                    trat_b1.setText(R.string.edem);
                    trat_b2.setText(R.string.fibr);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    b.setVisibility(View.VISIBLE);
                }
                scroll.fullScroll(View.FOCUS_RIGHT);

            }
        });

        trat_a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 2).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1) {
                    b.setVisibility(View.GONE);
                    fai_query("2");
                } else if (tratt == 2) {
                    b.setVisibility(View.VISIBLE);

                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b1.setText(R.string.arsu);
                    trat_b2.setText(R.string.arin);
                    trat_b3.setText(R.string.addo);
                    trat_b4.setText(R.string.glut);
                    trat_a2.setBackgroundResource(R.drawable.btn_1p);
                    trat_a2.setTextColor(Color.BLACK);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                }

            }
        });

        trat_a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                scroll.fullScroll(View.FOCUS_RIGHT);
                prefsEditor.putInt("tratt_rf2", 3).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1) {
                    b.setVisibility(View.GONE);
                    fai_query("3");
                } else if (tratt == 2) {
                    scroll.fullScroll(View.FOCUS_RIGHT);
                    b.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b1.setText(R.string.arsu);
                    trat_b2.setText(R.string.arin);
                    trat_b3.setText(R.string.addo);
                    trat_b4.setText(R.string.seno);

                    trat_a3.setBackgroundResource(R.drawable.btn_1p);
                    trat_a3.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                }
                scroll.fullScroll(View.FOCUS_RIGHT);

            }
        });

        trat_a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                scroll.fullScroll(View.FOCUS_RIGHT);
                prefsEditor.putInt("tratt_rf2", 4).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1) {
                    b.setVisibility(View.GONE);
                } else if (tratt == 2) {
                    b.setVisibility(View.VISIBLE);
                }
                scroll.fullScroll(View.FOCUS_RIGHT);

                trat_b3.setVisibility(View.VISIBLE);
                trat_b4.setVisibility(View.VISIBLE);
                trat_b1.setText(R.string.arsu);
                trat_b2.setText(R.string.arin);
                trat_b3.setText(R.string.addo);
                trat_b4.setText(R.string.seno);

                trat_a4.setBackgroundResource(R.drawable.btn_1p);
                trat_a4.setTextColor(Color.BLACK);
                trat_a2.setBackgroundResource(R.drawable.btn_1);
                trat_a2.setTextColor(Color.WHITE);
                trat_a1.setBackgroundResource(R.drawable.btn_1);
                trat_a1.setTextColor(Color.WHITE);
                trat_a3.setBackgroundResource(R.drawable.btn_1);
                trat_a3.setTextColor(Color.WHITE);

            }
        });

        trat_b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tratt = prefs.getInt("tratt_rf2", 0);
                if (tratt == 1) {
                    fai_query("4");
                } else if (tratt == 2) {
                    fai_query("6");
                } else if (tratt == 3) {
                    fai_query("10");
                } else if (tratt == 4) {
                    fai_query("14");
                }

            }
        });
        trat_b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tratt = prefs.getInt("tratt_rf2", 0);
                if (tratt == 1) {
                    fai_query("5");
                } else if (tratt == 2) {
                    fai_query("7");
                } else if (tratt == 3) {
                    fai_query("11");
                } else if (tratt == 4) {
                    fai_query("15");
                }

            }
        });
        trat_b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tratt == 2) {
                    fai_query("8");
                } else if (tratt == 3) {
                    fai_query("12");
                } else if (tratt == 4) {
                    fai_query("16");
                }

            }
        });
        trat_b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tratt == 2) {
                    fai_query("9");
                } else if (tratt == 3) {
                    fai_query("13");
                } else if (tratt == 4) {
                    fai_query("17");
                }

            }
        });


    }

    public void scroll_rx() {


        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
}


    public void fai_query(String id){
        /*mDb.open();
        Cursor c=mDb.fetchTratRadio1_id(id);
        startManagingCursor(c);
        int nome_trattamento=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TRATTAMENTO);  //indici delle colonne
        int tempo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TEMPO_MIN);
        int preste_on=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.PRESET_ON);
        int preset_off=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.PRESET_OFF);
        int freq_mhz=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ_MHZ);
        int tipologia=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TIPOLOGIA);
        int polarita=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.POLARITA);
        int manipolo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.MANIPOLO);
        int piastra=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.PIASTRA);
        int pedaliera=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.PEDALIERA);
        int durata_feedback = c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.DURATA_FEEDBACK);
        int potenza_feed = c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.POTENZA_FEEDBACK);


        if(c.moveToFirst()){  //se va alla prima entry, il cursore non è vuoto
            do {

                System.out.println("Nome trattamento:"+c.getString(nome_trattamento)+"\n tempo:"+c.getInt(tempo)+"\n preset on:"+c.getInt(preste_on)
                        +"\n preset off:"+c.getInt(preset_off)+"\n freq:"+c.getInt(freq_mhz)+"\n tip"+c.getString(tipologia)
                        +"\n pola:"+c.getString(polarita)+"\n manip:"+c.getString(manipolo)+"\n piastra:"+c.getString(piastra)
                        +"\n pedaliera:"+c.getString(pedaliera)+"\n durata feed:"+c.getInt(durata_feedback)+"\n potenza feed:"+c.getInt(potenza_feed)); //estrazione dei dati dalla entry del cursor

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("PRESET_ON", c.getInt(preste_on));
                prefsEditor.putInt("PRESET_OFF", c.getInt(preset_off));
                prefsEditor.putInt("FREQUENZA", c.getInt(freq_mhz));
                prefsEditor.putString("TIPOLOGIA", c.getString(tipologia));
                prefsEditor.putString("POLARITA", c.getString(polarita));
                prefsEditor.putString("MANIPOLO", c.getString(manipolo));
                prefsEditor.putString("PIASTRA", c.getString(piastra));
                prefsEditor.putString("PEDALIERA",c.getString(pedaliera));
                prefsEditor.putInt("DURATA FEED",c.getInt(durata_feedback));
                prefsEditor.putInt("POTENZA FEED",c.getInt(potenza_feed));


                prefsEditor.commit();

            } while (c.moveToNext());//iteriamo al prossimo elemento
        }
        mDb.close();
        stopManagingCursor(c);
        c.close();

        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Immissione_diretta1ME_db.class);
        startActivityForResult(CauseNelleVic, 0);
*/
    }

public void back (View view) {
        finish();
}
    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

    public void esci (View view){
        finish();
    }
}