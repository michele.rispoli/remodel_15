package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;


public class Immissione_Remoto_Vacuum extends Activity {


    /*******************************************SERIALE **********************************/

    private static final String TAG = "MainActivity2";
    String msg = null;
    LogFile log = new LogFile();
    String tolog;
    String nome_trattamento_db;

    Button min1;
    Button add1;
    Button min_i;
    Button min_p;
    Button min_d;
    Button add_i;
    Button add_p;
    Button add_d;

    private SeekBar seekBar;
    private TextView textView;
    private TextView textView_i;
    private TextView textView_p;
    private TextView textView_d;
    public int tempo = 0;
    public int impulso = 1;
    public int pausa = 1;
    public int depressione = 0;
    String hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imm_remoto_vacuum);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        //inizializza();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        tempo = prefs.getInt("Tempo", 0);


        TextView titolo = findViewById(R.id.textView4);
        titolo.setText(prefs.getString("Nome Trattamento", getString(R.string.imm_dir)));
        textView = findViewById(R.id.textView1);
        textView_i = findViewById(R.id.textView_i);
        textView_p = findViewById(R.id.textView_p);
        textView_d = findViewById(R.id.textView_d);
        textView.setText(tempo + ":00");
        seekBar = findViewById(R.id.seekBar1);
        min1 = (Button) findViewById(R.id.min);
        add1 = (Button) findViewById(R.id.add);
        min_i = (Button) findViewById(R.id.min_i);
        min_p = (Button) findViewById(R.id.min_p);
        min_d = (Button) findViewById(R.id.min_d);
        add_i = (Button) findViewById(R.id.add_i);
        add_p = (Button) findViewById(R.id.add_p);
        add_d = (Button) findViewById(R.id.add_d);

        textView_i = findViewById(R.id.textView_i);
        textView_p = findViewById(R.id.textView_p);
        textView_d = findViewById(R.id.textView_d);
        impulso = prefs.getInt("Impulso", 1);
        pausa = prefs.getInt("Pausa", 1);
        depressione = prefs.getInt("Depressione", 5);
        textView_i.setText(impulso + "");
        textView_p.setText(pausa + "");
        textView_d.setText("-" + depressione + "0");
        calcola_depressione(depressione);
        seekBar.setProgress(tempo);

        min1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                }
            }
        });

        add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                } else if (tempo >= 60) {
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                } else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                }
            }
        });


//        min_i.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (impulso == 1) {
//                    impulso = 1;
//                    textView_i.setText("1");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Impulso", impulso);
//                    prefsEditor.commit();
//                } else if (2 <= impulso) {
//                    impulso = impulso - 1;
//                    textView_i.setText(impulso + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Impulso", impulso);
//                    prefsEditor.commit();
//                }
//            }
//        });
//
//        add_i.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (1 <= impulso && impulso < 10) {
//                    impulso = impulso + 1;
//                    textView_i.setText(impulso + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Impulso", impulso);
//                    prefsEditor.commit();
//                } else if (impulso >= 10){
//                    impulso = 10;
//                    textView_i.setText(10 + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Impulso", 10);
//                    prefsEditor.commit();
//
//                }
//            }
//        });
//
//        min_p.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (pausa == 1) {
//                    pausa = 1;
//                    textView_p.setText("1");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Pausa", pausa);
//                    prefsEditor.commit();
//                } else if (2 <= pausa ) {
//                    pausa = pausa - 1;
//                    textView_p.setText(pausa + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Pausa", pausa);
//                    prefsEditor.commit();
//                }
//            }
//        });
//
//        add_p.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (1 <= pausa && pausa < 10) {
//                    pausa = pausa + 1;
//                    textView_p.setText(pausa + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Pausa", pausa);
//                    prefsEditor.commit();
//                } else if (pausa >= 10){
//                    pausa = 10;
//                    textView_p.setText(10 + "");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Pausa", 10);
//                    prefsEditor.commit();
//
//                }
//            }
//        });
//
//        min_d.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (depressione == 5) {
//                    depressione = 5;
//                    textView_d.setText("-" + depressione + "0");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Depressione", depressione);
//                    prefsEditor.commit();
//                    calcola_depressione(depressione);
//                } else if (6 <= depressione ) {
//                    depressione = depressione - 1;
//                    textView_d.setText("-" + depressione + "0");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Depressione", depressione);
//                    prefsEditor.commit();
//                    calcola_depressione(depressione);
//
//
//                }
//            }
//        });
//
//        add_d.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (5 <= depressione && depressione < 20) {
//                    depressione = depressione + 1;
//                    textView_d.setText("-" + depressione + "0");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Depressione", depressione);
//                    prefsEditor.commit();
//                    calcola_depressione(depressione);
//                } else if (depressione >= 20){
//                    depressione = 20;
//                    textView_d.setText("-" + depressione + "0");
//                    SharedPreferences.Editor prefsEditor = prefs.edit();
//                    prefsEditor.putInt("Depressione", 10);
//                    prefsEditor.commit();
//                    calcola_depressione(depressione);
//                }
//            }
//        });

        /* GESTIONE SEEKBAR */
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = tempo;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progresValue = tempo;
                progress = progresValue;
                tempo = progress;
                String t = "" + tempo + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                progress = tempo;

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                progress = tempo;
                int minuti = tempo;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo + "minuti";
                System.out.println(t);
            }
        });


        Button start = findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, Operativa_Diretta_vacuum.class);
                startActivityForResult(CauseNelleVic, 0);

            }
        });
    }


    public void calcola_depressione(int val) {
        // calcolo del valore negativo in hex per la depressione //
        //val = 200;
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        int val_calc = val * 10;
        int calc_val = (65535 - val_calc) + 1;
        String val_to_send = (Integer.toHexString(calc_val)).toUpperCase();
        String send_val = val_to_send.substring(2, 4) + "-" + val_to_send.substring(0, 2);
        System.out.println("VALLL " + val_to_send + "->" + send_val);
        prefsEditor.putInt("Depressione", val).commit();

        // calcolo del valore negativo in hex per la depressione //

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);

    }

    public void home(View view) {
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}
