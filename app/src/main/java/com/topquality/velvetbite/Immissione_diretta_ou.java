package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Immissione_diretta_ou  extends Activity {

    private static final String TAG = "Imm_diretta_db_OU";
    String msg = null;

    private SeekBar seekBar;
    private TextView textView;
    public int tempo = 0;
    SoundPool spool;
    int spoolId;

    String nome_trattamento;
    int freq1;
    int freq2;
    int freq3;
    String tipologia;
    Button v1;
    Button v2;
    Button v3;
    Button manipolo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.immissione_diretta_ou_db);
        initializeVariables();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        SharedPreferences.Editor prefsEditor = prefs.edit();
        LinearLayout layout_vac = findViewById(R.id.layout_vac);
        int trattamento_1 = prefs.getInt("trattamento_1", 0);
        if (trattamento_1 == 1) {
            layout_vac.setVisibility(View.VISIBLE);
        }
        else if (trattamento_1 == 3) {
            layout_vac.setVisibility(View.GONE);
            prefsEditor.putString("TIPOLOGIA", "4").commit();
        }
        else if (trattamento_1 == 5) {
            layout_vac.setVisibility(View.GONE);
            prefsEditor.putString("TIPOLOGIA", "4").commit();
        }
        prefsEditor.putInt("ENERGY", 60).commit();
        v1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundResource(R.drawable.v1a);
                v2.setBackgroundResource(R.drawable.v2);
                v3.setBackgroundResource(R.drawable.v3);
                manipolo.setBackgroundResource(R.drawable.man_senvac);
                prefsEditor.putString("TIPOLOGIA", "4").commit();

            }
        });
        v2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundResource(R.drawable.v1);
                v2.setBackgroundResource(R.drawable.v2a);
                v3.setBackgroundResource(R.drawable.v3);
                manipolo.setBackgroundResource(R.drawable.man_vac);
                seleziona_intervallo();

            }
        });
        v3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundResource(R.drawable.v1);
                v2.setBackgroundResource(R.drawable.v2);
                v3.setBackgroundResource(R.drawable.v3a);
                manipolo.setBackgroundResource(R.drawable.man_vac);
                prefsEditor.putString("TIPOLOGIA", "5").commit();
            }
        });
/*
       pedale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("inviato");
                pacchetto_da_inviare("14", "01", 0); // mod onde d'urto
                pacchetto_da_inviare("15", "01", 0); // potenza 1-4: 60, 90, 120, 185
                pacchetto_da_inviare("16", "01", 0); // frequenza (1-16)
                pacchetto_da_inviare("1B", "02", 0); // pompa: 0 spenta, 1 sempre attiva, 2 ad intervalli
                pacchetto_da_inviare("17", "E8", 0); // pompa:attiva ad intervalli 1000s - impulso
                pacchetto_da_inviare("19", "E8", 0); // pompa:attiva ad intervalli 1000s - pausa
            }
        });*/


        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);


        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();
                    String t = "" + tempo;
                    System.out.println(t);
                }else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }
        });
    }

    public void seleziona_intervallo(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.intervallo);
        dialog.setCancelable(true);

        //Button a = findViewById(R.id.a);
        dialog.setCancelable(true);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        Button btn1 = (Button) dialog.findViewById(R.id.button1);
        Button btn2 = (Button) dialog.findViewById(R.id.button2);
        Button btn3 = (Button) dialog.findViewById(R.id.button3);
        Button btn4 = (Button) dialog.findViewById(R.id.button4);
        title.setText(getString(R.string.interval) + "");
        btn1.setText("1:1");
        btn2.setText("2:1");
        btn3.setText("3:1");
        btn4.setText("4:1");

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "500").commit();
                //a.setText("500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "6");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "500").commit();
                //a.setText("500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "1");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "1000").commit();
                //a.setText("1000 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "2");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences("Shared", MODE_PRIVATE).edit().putString("A", "1500").commit();
                //a.setText("1500 ms");
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putString("TIPOLOGIA", "3");
                prefsEditor.commit();
                dialog.dismiss();
            }
        });
        dialog.show();

    }



    public void SelezionaPedale(View view) {
        SelezionaPedale1();
    }

    public void SelezionaPedale1() {

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.pedale);
        dialog.setCancelable(true);
        dialog.show();

    }

    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
        TextView textView_titolo = (TextView) findViewById(R.id.textView4);
        v1 = (Button) findViewById(R.id.button10);
        v2 = (Button) findViewById(R.id.button11);
        v3 = (Button) findViewById(R.id.button13);
        manipolo = (Button) findViewById(R.id.select_manipolo);
        manipolo.setEnabled(false);
        v1.setEnabled(false);
        v2.setEnabled(false);
        v3.setEnabled(false);
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        nome_trattamento = prefs.getString("Nome Trattamento", "ONDE D'URTO");
        textView_titolo.setText(nome_trattamento);
        prefsEditor.putInt("TEMPO",0).commit();
        tempo = prefs.getInt("TEMPO", 0);
        prefsEditor.putInt("Tempo", 0).commit();
        textView.setText(tempo + ":00");
        seekBar.setProgress(tempo);
        freq1 = prefs.getInt("FREQ1", 0);
        freq2 = prefs.getInt("FREQ2", 0);
        freq3 = prefs.getInt("FREQ3", 0);
        tipologia = prefs.getString("TIPOLOGIA", "x");
        if (tipologia.equals("x")) {
            v1.setEnabled(true);
            v2.setEnabled(true);
            v3.setEnabled(true);
        } else if (tipologia.equals("1") || tipologia.equals("2") || tipologia.equals("3") || tipologia.equals("6") ) {
            //1:1-6 2:1-1 3:1-2 4:1-3
            v1.setBackgroundResource(R.drawable.v1);
            v2.setBackgroundResource(R.drawable.v2a);
            v3.setBackgroundResource(R.drawable.v3);
            manipolo.setBackgroundResource(R.drawable.man_vac);
        } else if (tipologia.equals("4")) {
            v1.setBackgroundResource(R.drawable.v1a);
            v2.setBackgroundResource(R.drawable.v2);
            v3.setBackgroundResource(R.drawable.v3);
            manipolo.setBackgroundResource(R.drawable.man_senvac);
        }else if (tipologia.equals("5")) {
            v1.setBackgroundResource(R.drawable.v1);
            v2.setBackgroundResource(R.drawable.v2);
            v1.setBackgroundResource(R.drawable.v3a);
            manipolo.setBackgroundResource(R.drawable.man_senvac);
        }
    }

    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        nome_trattamento = prefs.getString("Nome Trattamento", "ONDE D'URTO");
        tempo = prefs.getInt("TEMPO", 10);
        freq1 = prefs.getInt("FREQ1", 0);
        freq2 = prefs.getInt("FREQ2", 0);
        freq3 = prefs.getInt("FREQ3", 0);
        tipologia = prefs.getString("TIPOLOGIA", "x");

        if (tempo_trattamento == 0 || freq1 == 0 || freq2 == 0 || freq3 == 0 || tipologia.equals("x")) {
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            final float f = actualVolume / maxVolume;
            System.out.println("OOOOKKKKK " + f);


            System.out.println("tempo_trattamento == " + tempo + " freq1 == " + freq1+ " freq2 == " + freq2+" freq3 == " + freq3+" tip == " + tipologia);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {

            Context context = getBaseContext();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta_ou.class);
            startActivityForResult(CauseNelleVic, 0);
            finish();
        }
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}