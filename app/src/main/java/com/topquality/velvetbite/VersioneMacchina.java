package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class VersioneMacchina extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_macchina);

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        TextView sw = (TextView) findViewById(R.id.sw);
        TextView hw = (TextView) findViewById(R.id.hw);
        TextView rig = (TextView) findViewById(R.id.rig);
        TextView sw_ver = (TextView) findViewById(R.id.sw_ver);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("APP VER", e.getMessage());
        }
        sw_ver.setText("Vers."+ pInfo.versionName );
        TextView hw_ver = (TextView) findViewById(R.id.hw_ver);
        TextView copy = (TextView) findViewById(R.id.copy);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int leng = sp.getInt("LINGUA", 1);
    }

    /* INFORMAZIONI SULLA MACCHINA*/
    public void medicinaestetica(View view) {
        String t = getString(R.string.ver);
        System.out.println(t);
        Context context = getBaseContext();
        Intent MedEst = new Intent(context, VersioneMacchina.class);
        startActivityForResult(MedEst, 0);
    }

    /* NUMERI UTILI*/
    public void microchirurgia(View view) {
        String t = getString(R.string.cont);
        System.out.println(t);
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Assistenza_tecnica.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    /* FUNZIONI FISSE */
    public void settings(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home (View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}