package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class Trattamenti_el_andr  extends Activity {
    int db;
    int tratt = 1;
    VelvetSkinIntraPlusDB_rf1 mDb;
    HorizontalScrollView scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_el_andro);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        mDb = new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        String SharedPrefName = "Reserved";

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = reserved.getInt("DATABASE", 0);

        View a = findViewById(R.id.la);
        Button trat1 = findViewById(R.id.trat1);      //viso
        Button trat2 = findViewById(R.id.trat2);      //corpo
        Button trat3 = findViewById(R.id.trat3);      //
        trat1.setBackgroundResource(R.drawable.btn_1p);
        trat1.setTextColor(Color.BLACK);
        prefsEditor.putInt("tratt_rf1", 1).commit();
        Button trat_a1 = findViewById(R.id.trattrat_a1);
        Button trat_a2 = findViewById(R.id.trat_a2);
        Button trat_a3 = findViewById(R.id.trat_a3);

        scroll = findViewById(R.id.scroll);

        trat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //viso

                prefsEditor.putInt("tratt_rf1", 1).commit();
                trat1.setBackgroundResource(R.drawable.btn_1p);
                trat1.setTextColor(Color.BLACK);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
            }
        });

        trat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //collo

                prefsEditor.putInt("tratt_rf1", 2).commit();
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1p);
                trat2.setTextColor(Color.BLACK);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
            }
        });

        trat3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //collo

                prefsEditor.putInt("tratt_rf1", 3).commit();
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1p);
                trat3.setTextColor(Color.BLACK);
            }
        });

        trat_a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("tratt_rf2", 1).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    fai_query("112");
                }else if (tratt == 2){
                    fai_query("115");
                }else if (tratt == 3){
                    fai_query("118");
                }
            }
        });

        trat_a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("tratt_rf2", 2).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    fai_query("113");
                }else if (tratt == 2){
                    fai_query("116");
                }else if (tratt == 3){
                    fai_query("119");
                }
            }
        });

        trat_a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putInt("tratt_rf2", 3).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    fai_query("114");
                }else if (tratt == 2){
                    fai_query("117");
                }else if (tratt == 3){
                    fai_query("120");
                }
            }
        });
    }

    public void fai_query(String id){
        mDb.open();
        Cursor c=mDb.fetchTrateLETTROPORA_id(id);
        startManagingCursor(c);

        /*    private static final String CREA_TABELLA_ELETTROPORAZIONE = "CREATE TABLE "
            + MetaDatiTrattamentoElettropora.ELETTROPORAZIONE + " ("
            + MetaDatiTrattamentoElettropora.ID+ " integer primary key autoincrement, "
            + MetaDatiTrattamentoElettropora.TRATTAMENTO + " text not null, "
            + MetaDatiTrattamentoElettropora.TEMPO_MIN + " integer, "
            + MetaDatiTrattamentoElettropora.A + " integer, "
            + MetaDatiTrattamentoElettropora.B + " integer, "
            + MetaDatiTrattamentoElettropora.C + " integer, "
            + MetaDatiTrattamentoElettropora.C1 + " integer, "
            + MetaDatiTrattamentoElettropora.D + " integer, "
            + MetaDatiTrattamentoElettropora.FORMA_ONDA + " text, "
            + MetaDatiTrattamentoElettropora.MANIPOLO + " text, "
            + MetaDatiTrattamentoElettropora.PIASTRA + " text, "
            + MetaDatiTrattamentoElettropora.PEDALIERA + " text);";
            */
        int nome_trattamento=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.TRATTAMENTO);  //indici delle colonne
        int tempo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.TEMPO_MIN);
        int a=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.A);
        int b=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.B);
        int c1=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.C);
        int c2=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.C1);
        int d=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.D);
        int forma_onda =c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.FORMA_ONDA);
        int manipolo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.MANIPOLO);
        int piastra=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.PIASTRA);
        int pedaliera=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.PEDALIERA);

        if(c.moveToFirst()){  //se va alla prima entry, il cursore non è vuoto
            do {

                System.out.println("Nome trattamento:"+c.getString(nome_trattamento)+"\n tempo:"+c.getInt(tempo)+"\n a:"+c.getInt(a)
                        +"\n b:"+c.getInt(b)+"\n c1:"+c.getInt(c1)+"\n c2"+c.getString(c2)
                        +"\n d:"+c.getString(d)+"\n forma onda:"+c.getString(forma_onda)+"\n manip:"+c.getString(manipolo)+"\n piastra:"+c.getString(piastra)
                        +"\n pedaliera:"+c.getString(pedaliera)); //estrazione dei dati dalla entry del cursor

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("A", c.getInt(a));
                prefsEditor.putInt("B", c.getInt(b));
                prefsEditor.putInt("C1", c.getInt(c1));
                prefsEditor.putInt("C2", c.getInt(c2));
                prefsEditor.putInt("D", c.getInt(d));
                prefsEditor.putString("FORMA ONDA", c.getString(forma_onda));
                prefsEditor.putString("MANIPOLO", c.getString(manipolo));
                prefsEditor.putString("PIASTRA", c.getString(piastra));
                prefsEditor.putString("PEDALIERA",c.getString(pedaliera));
                prefsEditor.commit();

            } while (c.moveToNext());//iteriamo al prossimo elemento
        }
        mDb.close();
        stopManagingCursor(c);
        c.close();

        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Immissione_diretta2ME_db.class);
        startActivityForResult(CauseNelleVic, 0);

    }

    public void back (View view) {
        finish();
    }

    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

    public void esci (View view){
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
        finish();
    }
}