package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Onde_JSON extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags((WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON));
        setContentView(R.layout.onde_json);

        SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        RelativeLayout sfondo =  findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        String id = shared.getString("id", null);
        String plc = shared.getString("PLC", null);

        IVolleyListener listener = new IVolleyListener() {
            @Override
            public void onError(String response) {
                Log.i("VolleyError", response);
                TextView messageErr = findViewById(R.id.texterrEle);
                messageErr.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse(Context context, final JSONObject object) {
                if (object != null) {
                    LinearLayout layout = findViewById(R.id.lLayoutOnde);
                    try {
                        JSONArray arrNomi = object.names();
                        final ArrayList<String> listNomiArray = new ArrayList<>();
                        for (int y = arrNomi.length() - 1; y >= 0; y--) {
                            listNomiArray.add((String) arrNomi.get(y));
                        }
                        for (int i = 0; i < listNomiArray.size(); i++) {
                            if (!listNomiArray.get(i).equals("xx")) {
                                final Button b = new Button(getApplicationContext());
                                JSONArray arr;
                                arr = object.getJSONArray(listNomiArray.get(i));
                                JSONObject element;
                                element = arr.getJSONObject(0);
                                b.setText(element.getString("Nome"));
                                b.setId(i);
                                b.setBackgroundResource(R.drawable.btn29);
                                b.setTextColor(Color.WHITE);
                                b.setLayoutParams(new LinearLayout.LayoutParams(300, 80));
                                layout.addView(b);
                                b.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        JSONArray arr;
                                        ArrayList<String> jsonValues;
                                        try {
                                            arr = object.getJSONArray(listNomiArray.get(b.getId()));
                                            JSONObject element;
                                            element = arr.getJSONObject(0);
                                            jsonValues = new ArrayList<>();
                                            retrieveDataJSON(element, jsonValues);
                                            SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                                            final SharedPreferences.Editor editor = shared.edit();
                                            editor.putString("Nome Trattamento", element.getString("Nome")).commit();
                                            editor.putInt("Tempo", Integer.parseInt(element.getString("Tempo"))).commit();
                                            editor.putInt("FREQ1", Integer.parseInt(element.getString("Frequenza 1"))).commit();
                                            editor.putString("vacuum", element.getString("Vacuum")).commit();
                                            editor.putInt("trattamento_1",1).commit();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Context context = getBaseContext();
                                        Intent CauseNelleVic = new Intent(context, Immissione_remoto_ou.class);
                                        startActivityForResult(CauseNelleVic, 0);
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        chiamataJSON(getApplicationContext(), 0, "http://flendcompanysoftware.it/admin/macchinari/"+ id +"/"+ plc +"/ou_estetica.json", null, listener);
    }

    private void chiamataJSON(final Context context, int method, String url, final String requestBody, final IVolleyListener listener) {
        SharedPreferences shared = getSharedPreferences("Shared",MODE_PRIVATE);
        SharedPreferences reserved = getSharedPreferences("Reserved",MODE_PRIVATE);
        final String credentials = shared.getString("email",null)+reserved.getString("password",null);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.getCache().clear();
        JsonObjectRequest JSONRequest = new JsonObjectRequest(method, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(context, response);
                        Log.i("VOLLEY", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                listener.onError(error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                String auth = "Basic" + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                headers.put("Authorization", auth);
                headers.put("Keep-Alive", "Connection");
                return headers;
            }
        };
        JSONRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 3000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 3000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.i("VOLLEY", error.toString());
            }
        });
        //JSONRequest.setShouldCache(false);
        requestQueue.add(JSONRequest);
    }

    private void retrieveDataJSON(JSONObject element, ArrayList<String> jsonValues) throws JSONException {
        for (Iterator<String> it = element.keys(); it.hasNext(); ) {
            String keyString = it.next();
            jsonValues.add(keyString + " : " + element.get(keyString).toString());
        }
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
        startActivity(new Intent(getApplicationContext(),Trattamenti.class));
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}



