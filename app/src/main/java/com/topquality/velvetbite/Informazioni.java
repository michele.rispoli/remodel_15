package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Informazioni extends Activity {
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int checkIndustria;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informazioni);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);

        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        TextView assistenza = (TextView) findViewById(R.id.sett);

        Button uno = findViewById(R.id.uno);
        Button due = findViewById(R.id.due);
        Button tre = findViewById(R.id.tre);
        Button quattro = findViewById(R.id.sw);
        if (reserved.getInt("INDUSTRIA4", 0) == 1) {
            quattro.setVisibility(View.VISIBLE);
        }else{
            quattro.setVisibility(View.GONE);
        }

        assistenza.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (counter == 1) {
                    //Toast toast = Toast.makeText(getApplicationContext(), "CIAOOOOOOONE", Toast.LENGTH_SHORT);
                    //toast.show();
                    assistenza(v);
                    counter = 0;
                } else {
                    counter++;
                    System.out.println("counter = " + counter);
                }
                return true;
            }
        });

        uno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), VersioneMacchina.class);
                startActivity(i);
            }
        });

        due.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Assistenza_tecnica.class);
                startActivity(i);
            }
        });

        tre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),
                        Contatori.class);
                startActivity(i);
            }
        });

        quattro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginFlend.class));
            }
        });
    }

    public void assistenza(View view) {
        Intent intent = new Intent(this, Manutenzione.class);
        startActivity(intent);
    }

    public void back(View view) {
        finish();
//        Intent intent = new Intent(this, MenuPrinc.class);
//        startActivity(intent);
    }
    public void informazioni (View view) {
        startActivity(new Intent(getApplicationContext(),Informazioni.class));
        finish();
    }
}