package com.topquality.velvetbite;

import android.content.Context;

import org.json.JSONObject;

public interface IVolleyListener {
    void onError (String response);
    void onResponse (Context context, JSONObject object);
}

