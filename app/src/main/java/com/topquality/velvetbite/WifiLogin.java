    package com.topquality.velvetbite;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class WifiLogin extends Activity {
    private ListView wifiList;
    private WifiManager wifiManager;
    WifiReceiver wifiReceiver;
    int pos;
    WifiInfo info;
    String ssid;
    TextView connesso;
    private final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_login);
        connesso = findViewById(R.id.txtconnessione);
        wifiList = findViewById(R.id.lst2);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        info = wifiManager.getConnectionInfo();
        ssid  = info.getSSID();

        if (isOnline() && !ssid.contains("unknown ssid")) {
            connesso.setTextColor(Color.GREEN);
            connesso.setText(getString(R.string.cnok) + " " + ssid.replace("\"", ""));
            findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.icwifi);
        }
        else if (isOnline() && ssid.contains("unknown ssid")) {
            connesso.setTextColor(Color.GREEN);
            connesso.setText(getString(R.string.connesso));
            findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.icwifi);
        }
        else {
            connesso.setTextColor(Color.RED);
            connesso.setText(R.string.nc);
            findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.iconwifi);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isOnline()) {
            if (isOnline()) {
            }
            findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.icwifi);
        } else {
            findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.iconwifi);
        }

        wifiReceiver = new WifiReceiver(wifiManager, wifiList);
        wifiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String wifiSSID = wifiReceiver.deviceWifiList.getItemAtPosition(position).toString();
                String [] capabilities = wifiReceiver.sb.toString().split(" - ", 0);
                pos = position;
                AlertDialog.Builder builder = new AlertDialog.Builder(WifiLogin.this);
                final LayoutInflater inflater = getLayoutInflater();
                final View v = inflater.inflate(R.layout.wifi_dialog_layout, null);
                final EditText psw = v.findViewById(R.id.edtPSWIFI); //campo password
                builder.setView(v);
                builder.setTitle(wifiReceiver.deviceWifiList.getItemAtPosition(position).toString());
                builder.setPositiveButton(getString(R.string.connetti), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        connectWifi(wifiSSID, capabilities[position],psw.getText().toString());
                        final Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            //TODO miglioria: processo da togliere in quanto la textview viene settata nell'onResume con il processo ciclico di check, acquisire le info.ssid e basta quindi va tolto l'handler e il runnable da qua
                            public void run() {
                                if (isOnline()) {
                                    wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                                    info = wifiManager.getConnectionInfo();
                                    ssid  = info.getSSID();
                                    connesso.setTextColor(Color.GREEN);
                                    System.out.println("SSID ATTUALE CHE NON METTE A SCHERMO : " + ssid);
                                    connesso.setText(getString(R.string.cnok) + " " +  ssid.replace("\"", ""));
                                    findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.icwifi);

                                }
                                else {
                                    connesso.setTextColor(Color.RED);
                                    connesso.setText(R.string.nc);
                                    findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.iconwifi);
                                    //Toast.makeText(getApplicationContext(),getString(R.string.validazioneCon),Toast.LENGTH_LONG).show();
                                }
                            }
                        }, 5000);
                    }
                });
                builder.setNegativeButton(getString(R.string.annulla), null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(wifiReceiver, intentFilter);
        getWifi();
        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            @Override
            //TODO : da aggiungere agli altri software questo tipo di check e rimuovere la parte che si attiva dopo il dialog di connessione
            public void run() {
                if (isOnline()) {
                    connesso.setTextColor(Color.GREEN);
                    findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.icwifi);
                    connesso.setText(getString(R.string.cnok) + " " +  ssid.replace("\"", ""));

                } else {
                    findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.iconwifi);
                    connesso.setTextColor(Color.RED);
                    connesso.setText(R.string.nc);
                    findViewById(R.id.buttonWifi).setBackgroundResource(R.drawable.iconwifi);
                }
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(r, 5000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(wifiReceiver);
    }

    private void getWifi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(WifiLogin.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(WifiLogin.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_ACCESS_COARSE_LOCATION);
            } else {
                wifiManager.startScan();
            }
        } else {
            wifiManager.startScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    wifiManager.startScan();
                } else {
                    //Toast.makeText(WifiLogin.this, "permission not granted", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }

    private void connectWifi(String SSID, String Capabilities,String password) {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration conf = new WifiConfiguration();
        String psw = password;
        conf.status = WifiConfiguration.Status.ENABLED;
        conf.priority = 40;

        //configurazione WEP
        if (Capabilities.toUpperCase().contains("WEP")) {
            Toast.makeText(WifiLogin.this,getString(R.string.concorso),Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), "Connettendo ad una rete WEP", Toast.LENGTH_SHORT).show();
            conf.SSID = "\"" + SSID + "\"";
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            conf.wepKeys[0] = "\"" + psw + "\"";
            conf.wepTxKeyIndex = 0;
        }
        //configurazione WPA
        else if (Capabilities.toUpperCase().contains("WPA")) {
            Toast.makeText(WifiLogin.this,getString(R.string.concorso),Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), "Connettendo ad una rete WPA", Toast.LENGTH_SHORT).show();
            conf.SSID = "\"" + SSID + "\"";
            conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            conf.preSharedKey = "\"" + psw + "\"";
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        }
        //configurazione per network aperti
        else {
            Toast.makeText(WifiLogin.this,getString(R.string.concorso),Toast.LENGTH_LONG).show();
            //Toast.makeText(getApplicationContext(), "Connettendo ad una rete Aperta", Toast.LENGTH_SHORT).show();
            conf.SSID = "\"" + SSID + "\"";
            conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            conf.allowedAuthAlgorithms.clear();
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        }
        wifiManager.addNetwork(conf);
        try {
            List<WifiConfiguration> listConf = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration config : listConf) {
                if (config.SSID != null && config.SSID.equals("\"" + SSID + "\"")) {
                    this.wifiManager.disconnect();
                    this.wifiManager.enableNetwork(config.networkId, true);
                    this.wifiManager.reconnect();
                    break;

                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
    public void back(View view){
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
        finish();
    }

    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

//    public void home(View view) {
//        finish();
//        Context context = getBaseContext();
//        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
//        startActivityForResult(CauseNelleVic, 0);
//    }
}
