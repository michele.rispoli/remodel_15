package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginFlend extends Activity {
    EditText email;
    EditText psw;
    TextView conn;
    TextView cred;
    Button esci;
    Button entra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flend_login);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        email = findViewById(R.id.edtEmail);
        psw = findViewById(R.id.edtPsw);
        conn = findViewById(R.id.textConn);
        cred = findViewById(R.id.textView7);
        esci = findViewById(R.id.btnesciserver);
        entra = findViewById(R.id.btnloginServer);
        final SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences.Editor reservedEditor = reserved.edit();
        final SharedPreferences.Editor sharedEditor = shared.edit();
        if (shared.getString("connessioneServer", "non connesso").equals("connesso")) {
            email.setVisibility(View.GONE);
            psw.setVisibility(View.GONE);
            cred.setVisibility(View.GONE);
            entra.setVisibility(View.GONE);
            esci.setVisibility(View.VISIBLE);
            conn.setVisibility(View.VISIBLE);
            conn.setText(getString(R.string.alCon) + " \n " + shared.getString("email", null));
            esci.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sharedEditor.remove("connessioneServer").commit();
                    sharedEditor.remove("email").commit();
                    reservedEditor.remove("password").commit();
                    startActivity(new Intent(getApplicationContext(), MenuPrinc.class));
                }
            });
        }
        entra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final IVolleyListener listener = new IVolleyListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(Context context, final JSONObject result) {
                        if (isOnline() && !email.getText().toString().equals("") && !psw.getText().toString().equals("") && result != null) {
                            try {
                                JSONObject id = result.getJSONObject("0");
                                sharedEditor.putString("id", id.getString("id")).commit();
                                sharedEditor.putString("connessioneServer", "connesso").commit();
                                Toast.makeText(getApplicationContext(), getString(R.string.con), Toast.LENGTH_LONG).show();
                                if (result != null) {
                                    email.setVisibility(View.INVISIBLE);
                                    psw.setVisibility(View.INVISIBLE);
                                }
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getString(R.string.validazioneCon), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        } else {
                            sharedEditor.putString("connessioneServer", "non connesso").commit();
                            Toast.makeText(getApplicationContext(), getString(R.string.validazioneCon), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(String response) {
                        Log.i("VolleyError", response);
                        Toast.makeText(getApplicationContext(), getString(R.string.validazioneCon), Toast.LENGTH_LONG).show();
                    }
                };

                String chiaro = "CON_" + psw.getText().toString() + "_FRA";
                String scuro = null;
                try {
                    MessageDigest digest = MessageDigest.getInstance("sha-256");
                    byte[] hash = digest.digest(chiaro.getBytes(StandardCharsets.UTF_8));
                    scuro = byte2Hex(hash);
                    Log.i("codice", scuro);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                chiamataJSON(LoginFlend.this, 0, "http://flendcompanysoftware.it/admin/macchinari/dbQuery.php", email.getText().toString(), scuro, null, listener);

                sharedEditor.putString("email", email.getText().toString()).commit();
                reservedEditor.putString("password", psw.getText().toString()).commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(email.getWindowToken(), 0);
                }
            }
        });
    }


    private void chiamataJSON(final Context context, int method, String url, final String headerEmail, final String headerPassword, final String requestBody, final IVolleyListener listener) {
        final SharedPreferences shared = getSharedPreferences("Shared", MODE_PRIVATE);
        final SharedPreferences reserved = getSharedPreferences("Reserved", MODE_PRIVATE);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.getCache().clear();
        JsonObjectRequest JSONRequest = new JsonObjectRequest(method, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(context, response);
                        Log.i("VOLLEY", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                listener.onError(error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
//                String credentials = shared.getString("email", null) + ":" + reserved.getString("password", null);
//                String auth = "Basic" + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                headers.put("Authorization", auth);
                headers.put("Keep-Alive", "Connection");
                headers.put("id-email", headerEmail);
                headers.put("id-password", headerPassword);
                return headers;
            }

            @Override
            public byte[] getBody() {
                return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
            }
        };
        JSONRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 3000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 3000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.i("VOLLEY", error.toString());
            }
        });
        JSONRequest.setShouldCache(false);
        requestQueue.add(JSONRequest);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            boolean exitValue = ipProcess.waitFor(3000, TimeUnit.MILLISECONDS);
            return (exitValue);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
    private static String byte2Hex(byte[] bytes) {
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i = 0; i < bytes.length; i++) {
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length() == 1) {
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();

    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
//        startActivity(new Intent(getApplicationContext(),MenuPrinc.class));
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view) {
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}


