package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Trattamenti_pre extends Activity {
    int db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_pre);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        int trattamento = prefs.getInt("trattamento", 0);
        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        if(reserved.getInt("INDUSTRIA4",0)==0){
b2.setVisibility(View.GONE);
        }else{
b2.setVisibility(View.VISIBLE);
        }

        if (trattamento == 1){
            b1.setVisibility(View.VISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);

            b1.setText(R.string.me);
            b2.setText(R.string.fisio);
            b3.setText(R.string.andro);

        }else if (trattamento == 2){
            b1.setVisibility(View.VISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);

            b1.setText(R.string.me);
            b2.setText(R.string.fisio);
            b3.setText(R.string.andro);
        }


        Button seleziona_trattamento = findViewById(R.id.seleziona_trattamento);
        //

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento == 1){

                    prefsEditor.putInt("trattamento_1", 1).commit(); //me OU
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);


                }else if (trattamento == 2){
                    prefsEditor.putInt("trattamento_1", 2).commit(); //me EL
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);
                }
            }
        });
        final SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento == 1){
                    prefsEditor.putInt("trattamento_1", 3).commit(); //fisio OU
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);

                }else if (trattamento == 2){
                    prefsEditor.putInt("trattamento_1", 4).commit(); //fisio EL
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);
                }
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento == 1){

                    prefsEditor.putInt("trattamento_1", 5).commit(); //andro OU
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);

                }else if (trattamento == 2){

                    prefsEditor.putInt("trattamento_1", 6).commit(); //andro EL
                    Intent i = new Intent(getApplicationContext(), Trattamenti.class);
                    startActivity(i);
                }
            }
        });


    }
    public void back (View view) {
        finish();
    }
    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);

    }

    public void esci (View view){
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
    }
}