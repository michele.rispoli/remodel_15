package com.topquality.velvetbite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class CKaudio extends Activity {

    private static final int DIALOG_ALERT_ID = 1;
    static final int CONFIRM_DIALOG = 0;
    int vol_audio = 0;
    int volume = 0;

    SoundPool spool;
    int spoolres;
    int spoolId;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_audio);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(CKaudio.this, R.raw.doorbellup, 0);
        spoolId = spool.load(CKaudio.this, R.raw.doorbellup, 0);
        //volume();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        vol_audio = prefs.getInt("Volume", 1);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int leng = sp.getInt("LINGUA", 1);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Volume", 1);

        prefsEditor.commit();
        AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    /* MENU MEDICINA ESTETICA*/
    public void medicinaestetica(View view) {
        bip();
        ck_audio();
    }
    public void bip(){
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float f = actualVolume / maxVolume;
        System.out.println("OOOOKKKKK " + f);
        spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                spool.play(spoolId, f, f, 1, 0, 1);
            }
        });
        spool.play(spoolId, f, f, 1, 0, 1);
    }

    public void ck_audio(){
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.audio);
        dialog.setCancelable(true);
        Button no = (Button) dialog.findViewById(R.id.button1); //no
        Button si = (Button) dialog.findViewById(R.id.button2); //sì

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CKaudio.this, CKaudio.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        si.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
                startActivity(i);
                finish();
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    /* CONTATTA */
    public void microchirurgia(View view) {

        String toSpeak = getString(R.string.cont);
        System.out.println(toSpeak);
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Assistenza_tecnica.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    /* CONTINUA CMQ*/
    public void fisioterapia(View view) {
        String toSpeak = getString(R.string.continua);
        System.out.println(toSpeak);
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Disclaimer.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void informazioni(View view) {
/*        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);*/
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void impostazioni(View view) {
/*        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Impostazioni.class);
        startActivityForResult(CauseNelleVic, 0);*/
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}