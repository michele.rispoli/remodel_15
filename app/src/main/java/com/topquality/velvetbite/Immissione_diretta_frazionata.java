package com.topquality.velvetbite;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

import android_serialport_api.SerialPort;


public class Immissione_diretta_frazionata extends Activity {

    private static final String TAG = "FRAZ";
    String msg = null;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Immissione_diretta_frazionata.ReadThread mReadThread;
    private Immissione_diretta_frazionata.SendThread mSendThread;
    private Immissione_diretta_frazionata.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();


    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private static final String Sserialport = "COM0"; //nome porta  - non uso più il nome
    private static final int m_iSerialPort = 0;  //id_seriale
    private static final int baudrate = 57600;  //baud rate
    private static final int databits = 8;  //  dati
    private static final int stopbits = 1;  // stop
    private static final int parity = 'n';  // parità NESSUNA (esiste anche pari o dispari)


    String mono = "3E-FE-01-51-02-45-05-00-00-00-00-00-00-00-00-00-00-00-00-00-00-1C-6D-3C";
    String bipo = "3E-FE-01-51-02-45-0D-00-00-00-00-00-00-00-00-00-00-00-00-00-00-59-4A-3C";
    String pausa0 = "3E-FE-01-51-03-32-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-19-C9-3C";
    String mod3 = "3E-FE-01-51-02-4D-03-00-00-00-00-00-00-00-00-00-00-00-00-00-00-F2-AA-3C";

    String manipolo10 = "3E-FE-01-51-02-48-08-00-00-00-00-00-00-00-00-00-00-00-00-00-00-64-46-3C";
    String canale_pedale = "3E-FE-01-51-02-61-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-68-30-3C";


    /**non mi servono**/
    String freq_1000 = "3E-FE-01-51-04-52-00-00-01-00-00-00-00-00-00-00-00-00-00-00-00-E8-34-3C";
    String freq_500 = "3E-FE-01-51-04-52-00-80-00-00-00-00-00-00-00-00-00-00-00-00-00-F5-26-3C"; // occhio che è 1mhz
    String radiofrequenza = "3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C";
    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C";
    String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";



    //potenza 10
    String pot0 = "3E-FE-01-51-03-36-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-CB-49-3C";

    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    int polarita = 0;
    private SeekBar seekBar;
    private TextView textView;
    public int tempo = 0;
    int ck_polarita = 0;
    int ck_tipologia = 0;
    int ck_frequenza = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    int tempo_feed = 2;
    int tempo_tot_feed = 0;
    Handler updateBarHandler;
    Button min2;
    Button add2;
    TextView textView12;
    int impulso = 0;
    String hex;
    Button set_mono;
    Button set_bipo;

    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Immissione_diretta_frazionata.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.immissione_diretta_frazionata);

        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.setAlpha(0);
        videoView.postDelayed(new Runnable() {
            @Override
            public void run() {
                videoView.setAlpha(1f);
            }
        },300);
        videoView.setVisibility(View.VISIBLE);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });
        set_mono = (Button) findViewById(R.id.mono);
        set_bipo = (Button) findViewById(R.id.bipo);
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            Log.d("VELVETERROR", e.getMessage());
        }

        SendMsg(stop_mio);

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Immissione_diretta_frazionata.this, R.raw.errore, 0);
        spoolId = spool.load(Immissione_diretta_frazionata.this, R.raw.errore, 0);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        initializeVariables();
        set_mono.setBackgroundResource(R.drawable.btn_sx_ar);
        set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);

        SendMsg(radiofrequenza);
        manipolo = 0;
        int leng = prefs.getInt("LINGUA", 1);

        /* inizializzazione dei paramentri */
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Tempo", 0);
        prefsEditor.putInt("Manipolo", 1);
        prefsEditor.putInt("Elettrodo", 1);
        prefsEditor.putInt("Polarita", 0);
        prefsEditor.putInt("Pedaliera", 1);
        /*var ausiliari */
        prefsEditor.putInt("Impulso", 10);
        prefsEditor.putInt("CK_tipologia", 0);
        prefsEditor.putInt("CK_frequenza", 0);
        /* gestione del volume */
        vol_audio = prefs.getInt("Volume", 1);
        prefsEditor.commit();
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);
        seekBar.setProgress(0);
        textView.setText("0:00");
        min.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    //System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo - 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
                return true;
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });
        add.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (1 < tempo && tempo < 10) {
                    tempo = tempo + 5;
                    textView.setText(tempo + ":00");
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo + 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }
                return true;
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo;
                    System.out.println(t);
                }else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });

        /* GESTIONE SEEKBAR */
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }
        });

        min2 = (Button) findViewById(R.id.min2);
        add2 = (Button) findViewById(R.id.add2);
        textView12 = (TextView) findViewById(R.id.textView12);
        textView12.setText("1");

        min2.setEnabled(false);
        min2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (impulso == 0) {
                    impulso = 10;
                    textView12.setText("10");
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(false);
                    add2.setEnabled(true);
                } else if (50 >= impulso && impulso > 0) {
                    impulso = impulso - 50;
                    textView12.setText("10");
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(false);
                    add2.setEnabled(true);
                } else {
                    impulso = impulso - 50;
                    textView12.setText(""+impulso);
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(true);
                    add2.setEnabled(true);
                }
            }
        });

        add2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (0 < impulso && impulso < 10) {
                    impulso = 50;
                    textView12.setText("50");
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(true);
                    add2.setEnabled(true);
                }else if (impulso == 500) {
                    impulso = 500;
                    textView12.setText("500");
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(true);
                    add2.setEnabled(false);
                } else {
                    impulso = impulso + 50;
                    textView12.setText(""+impulso);
                    //hex = Integer.toHexString(impulso).toUpperCase();
                    //pacchetto_da_inviare("36", hex, 0);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Impulso", impulso);
                    prefsEditor.commit();
                    min2.setEnabled(true);
                    add2.setEnabled(true);
                } //aggiungi add max
            }
        });
    }

    public void monopolare(View view) {
        final Button elettrodo = (Button) findViewById(R.id.select_elettrodo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);
        ck_polarita = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_polarita == 0) {
            System.out.println("Sono monopolare - rimango tale");
            set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
            set_mono.setBackgroundResource(R.drawable.btn_sx_ar);

            prefsEditor.putInt("Polarita", 1);
            prefsEditor.putInt("CK_polarita", ck_polarita);
            prefsEditor.commit();
            String t = getString(R.string.mono);
            System.out.println(t);
        } else if (ck_polarita == 1) {
            System.out.println("sono bipolare - cambio");
            set_bipo.setBackgroundResource(R.drawable.btn_dx_blu);
            set_mono.setBackgroundResource(R.drawable.btn_sx_ar);

            prefsEditor.putInt("Polarita", 1);
            prefsEditor.putInt("CK_polarita", ck_polarita);
            prefsEditor.commit();
            String t = getString(R.string.bipo);
            System.out.println(t);
        }

    }

    public void bipolare(View view) {
        final Button elettrodo = (Button) findViewById(R.id.select_elettrodo);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        manipolo = prefs.getInt("Manipolo", 0);
        polarita = prefs.getInt("Polarita", 0);

        ck_polarita = 1;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_polarita == 1) {
            System.out.println("Sono bipolare - rimango tale");

            set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
            set_mono.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_polarita = 1;
            prefsEditor.putInt("Polarita", 2);
            prefsEditor.putInt("CK_polarita", ck_polarita);
            prefsEditor.commit();
            String t = getString(R.string.bipo);
            System.out.println(t);
        } else if (ck_polarita == 0) {
            System.out.println("sono monopolare - cambio");

            set_bipo.setBackgroundResource(R.drawable.btn_dx_ar);
            set_mono.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_polarita = 0;
            prefsEditor.putInt("Polarita", 2);
            prefsEditor.putInt("CK_polarita", ck_polarita);
            prefsEditor.commit();
            String t = getString(R.string.mono);
            System.out.println(t);
        }
    }


    /*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        int byte_val = 0;
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot = null;


        /*calcola i byte validi */
        int lung_mess = mess.length();
        System.out.println("CHE LUNGHEZZA HAI?????????????????????????1 " + lung_mess);

        if (lung_mess == 2) {
            byte_val = 2;
            int lung_mess1 = messaggio.length();
            if (lung_mess1%2 == 0) {
                msg_tot = messaggio +"-00";
                System.out.println("CHE HAI?????????????????????????1 " + msg_tot);
            }else if (lung_mess1%2 == 1){
                msg_tot = "0"+messaggio+"-00";
                System.out.println("CHE HAI??????????????????????????2 " + msg_tot);
            }
        }else if (lung_mess == 3){
            byte_val = 3;
            String mess1 = "00"+messaggio + "0";
            System.out.println("IL MESSAGGIO con lo 0 è: " + mess1);
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 4){
            byte_val = 3;
            String mess1 = "0"+messaggio + "0";
            msg_tot = dividi_msg(mess1);
        }else if (lung_mess == 1){
            byte_val = 2;
            String mess1 = "0"+messaggio+"-00";
            msg_tot = mess1;
        }




        if (reg.equals("30")){
            byte_val = 3;
        }
        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"
        System.out.println("UTILIZZABILI "+ byteUtilizzabili);
        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 14;
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;

        System.out.println("!!!!!!!!!!!!!  - " +data_to_send);
        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }

    public String dividi_msg(String mess){
        String msg_div = null;

        String val1 = mess.substring(1,3);
        System.out.println("LA SOTTOSTRINGA1 è: " + val1);

        String val2 = mess.substring(3,5);
        System.out.println("LA SOTTOSTRINGA2 è: " + val2);

        msg_div = val2 + "-" + val1;

        return msg_div;

    }

    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }


    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }


    /******************************************SERIALE *******************************************/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onPause in");
        super.onPause();
        Log.i(TAG, "==>onPause out");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onStop in");
        super.onStop();
        Log.i(TAG, "==>onStop out");
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onDestroy in");
        closeSerialPort();
        super.onDestroy();
        Log.i(TAG, "==>onDestroy out");
    }

    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG+" LEGGO da thread: " + to_check);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {


        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }

    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }
    /***********************SERIALE ********************************/


    /* GESTIONE VOLUME */
    public void volume() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

    }

    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
    }


    public void freq1(View view) {
        final Button set_freq1 = (Button) findViewById(R.id.freq1);
        final Button set_freq2 = (Button) findViewById(R.id.freq2);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        ck_frequenza = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_frequenza == 0) {
            System.out.println("Sono freq1 - rimango tale");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_blu);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_frequenza = 0;
            SendMsg(freq_500);
            prefsEditor.putInt("Frequenza", 1);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();
            String t = getString(R.string.freq1s);
            System.out.println(t);
        } else if (ck_frequenza == 1) {
            System.out.println("sono freq2 - cambio");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_blu);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_ar);
            ck_frequenza = 1;
            prefsEditor.putInt("Frequenza", 1);
            prefsEditor.putInt("CK_frequenza", ck_frequenza);
            prefsEditor.commit();
            SendMsg(freq_500);
            String t = getString(R.string.freq2s);
            System.out.println(t);
        }
    }

    public void freq2(View view) {
        final Button set_freq1 = (Button) findViewById(R.id.freq1);
        final Button set_freq2 = (Button) findViewById(R.id.freq2);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        ck_frequenza = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        if (ck_frequenza == 1) {
            System.out.println("Sono freq1 - rimango tale");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_ar);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_frequenza = 1;
            prefsEditor.putInt("Frequenza", 2);
            prefsEditor.putInt("CK_frequenza", ck_tipologia);
            prefsEditor.commit();
            SendMsg(freq_1000);
            String t = getString(R.string.freq1s);
            System.out.println(t);
        } else if (ck_frequenza == 0) {
            System.out.println("sono freq2 - cambio");
            set_freq2.setBackgroundResource(R.drawable.btn_dx_ar);
            set_freq1.setBackgroundResource(R.drawable.btn_sx_blu);
            ck_frequenza = 0;
            SendMsg(freq_1000);
            prefsEditor.putInt("Frequenza", 2);
            prefsEditor.putInt("CK_frequenza", ck_frequenza);
            prefsEditor.commit();

            String t = getString(R.string.freq2s);
            System.out.println(t);
        }
    }


    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int tempo_trattamento = prefs.getInt("Tempo", 0);
        int impulso_scelto = prefs.getInt("Impulso", 10);
        polarita = prefs.getInt("Polarita", 0);
        hex = Integer.toHexString(impulso_scelto).toUpperCase();


        if (tempo_trattamento == 0 || polarita == 0) {
            final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            final float f = actualVolume / maxVolume;
            System.out.println("OOOOKKKKK " + f);
            spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                    spool.play(spoolId, f, f, 1, 0, 1);
                }
            });
            spool.play(spoolId, f, f, 1, 0, 1);
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        } else {
            //ShowDialog();
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(pot0);
            try {
                Thread.sleep(200);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            if (polarita == 1){
                SendMsg(mono);
            }else if (polarita == 2){
                SendMsg(bipo);
            }
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(freq_1000);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            pacchetto_da_inviare("30", hex, 0);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(pausa0);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(mod3);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(manipolo10);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(canale_pedale);
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            SendMsg(start);
            String t = getString(R.string.init_tratt);
            System.out.println(t);

            Context context = getBaseContext();
            closeSerialPort();
            Intent CauseNelleVic = new Intent(context, Operativa_diretta_frazionata.class);
            finish();
            startActivityForResult(CauseNelleVic, 0);
        }
    }

    public int hex (int i) {
        int to_calc = i;
        String hex = Integer.toHexString(to_calc);
        System.out.println("hex" + hex);
        int foo = Integer.parseInt(hex);
        return foo;
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}