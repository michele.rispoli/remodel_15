package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class Trattamenti_el  extends Activity {
    int db;
    int tratt = 1;
    int tratt1 = 1;
    VelvetSkinIntraPlusDB_rf1 mDb;
    View b;
    View c;
    View d;
    View e;
    HorizontalScrollView scroll;
    int cursore = 0;

    Button trat_a1;
    Button trat_a2;
    Button trat_a3;
    Button trat_a4;
    Button trat_a5;
    Button trat_a6;

    Button trat_b1;
    Button trat_b2;
    Button trat_b3;
    Button trat_b4;
    Button trat_b5;
    Button trat_b6;

    Button trat_c1;
    Button trat_c2;
    Button trat_c3;
    Button trat_c4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_el);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        mDb=new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        String SharedPrefName = "Reserved";
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = reserved.getInt("DATABASE", 0);
        View a = findViewById(R.id.la);
        b = findViewById(R.id.lb);
        c = findViewById(R.id.lc);
        d = findViewById(R.id.ld);
        e = findViewById(R.id.le);
        Button trat1 = findViewById(R.id.trat1);      //viso
        Button trat2 = findViewById(R.id.trat2);      //corpo
        Button trat3 = findViewById(R.id.trat3);      //frazionata
        trat1.setBackgroundResource(R.drawable.btn_1p);
        trat1.setTextColor(Color.BLACK);
        prefsEditor.putInt("tratt_rf1", 1).commit();
        prefsEditor.putInt("tratt_rf2", 1).commit();
        trat_a1 = findViewById(R.id.trattrat_a1);
        trat_a2 = findViewById(R.id.trat_a2);
        trat_a3 = findViewById(R.id.trat_a3);
        trat_a4 = findViewById(R.id.trat_a4);
        trat_a5 = findViewById(R.id.trat_a5);
        trat_a6 = findViewById(R.id.trat_a6);


        trat_a1.setText(R.string.rug);
        trat_a3.setVisibility(View.GONE);
        trat_a2.setText(R.string.ipmu);
        trat_a4.setVisibility(View.GONE);
        trat_a5.setVisibility(View.GONE);
        trat_a6.setVisibility(View.GONE);

        trat_b1 = findViewById(R.id.trattrat_b1);
        trat_b2 = findViewById(R.id.trat_b2);
        trat_b3 = findViewById(R.id.trat_b3);
        trat_b4 = findViewById(R.id.trat_b4);
        trat_b5 = findViewById(R.id.trat_b5);
        trat_b6 = findViewById(R.id.trat_b6);


        trat_c1 = findViewById(R.id.trattrat_c1);
        trat_c2 = findViewById(R.id.trat_c2);
        trat_c3 = findViewById(R.id.trat_c3);
        trat_c4 = findViewById(R.id.trat_c4);
        b.setVisibility(View.GONE);
        c.setVisibility(View.GONE);
        d.setVisibility(View.GONE);
        e.setVisibility(View.GONE);

        scroll = findViewById(R.id.scroll);

        Button left = findViewById(R.id.left);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //viso
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_LEFT);
                            }
                        });
                    }
                });
            }
        });

        Button right = findViewById(R.id.right);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //viso
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
            }
        });



        trat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //viso
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_LEFT);
                            }
                        });
                    }
                });
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 1).commit();
                prefsEditor.putInt("Manipolo EL",1).commit();
                trat_a5.setVisibility(View.GONE);
                trat1.setBackgroundResource(R.drawable.btn_1p);
                trat1.setTextColor(Color.BLACK);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
                trat_a1.setText(R.string.rug);
                trat_a2.setText(R.string.ipmu);
                trat_a3.setVisibility(View.GONE);
                trat_a4.setVisibility(View.GONE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                b.setVisibility(View.GONE);
            }
        });


        trat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //collo
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_LEFT);
                            }
                        });
                    }
                });
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 2).commit();
                prefsEditor.putInt("Manipolo EL",1).commit();
                trat_a4.setVisibility(View.GONE);
                trat_a3.setVisibility(View.VISIBLE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                trat_a1.setText(R.string.rug);
                trat_a2.setText(R.string.azbi);
                trat_a3.setText(R.string.deca);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1p);
                trat2.setTextColor(Color.BLACK);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
            }
        });

        trat3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //corpo
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_LEFT);
                            }
                        });
                    }
                });
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 3).commit();
                prefsEditor.putInt("Manipolo EL",2).commit();
                trat_a3.setVisibility(View.VISIBLE);
                trat_a4.setVisibility(View.VISIBLE);
                trat_a5.setVisibility(View.VISIBLE);
                trat_a6.setVisibility(View.VISIBLE);
                trat_a1.setText(R.string.arsu);
                trat_a2.setText(R.string.seno);
                trat_a3.setText(R.string.addo);
                trat_a4.setText(R.string.dor);
                trat_a5.setText(R.string.glut);
                trat_a6.setText(R.string.arin);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1p);
                trat3.setTextColor(Color.BLACK);

            }
        });

        trat_a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 1).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){ //viso - rughe
                    prefsEditor.putInt("viso", 1).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1p);
                    trat_a1.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.fron);
                    trat_b2.setText(R.string.peri);
                    trat_b3.setText(R.string.nala);
                    trat_b4.setText(R.string.glab);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 2){ //collo - rughe
                    prefsEditor.putInt("collo", 1).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1p);
                    trat_a1.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.paa);
                    trat_b2.setText(R.string.paoo);
                    //trat_b3.setText(R.string.prp);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ //corpo - arti sup
                    prefsEditor.putInt("corpo", 1).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1p);
                    trat_a1.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1);
                    trat_a6.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.lipdis12);
                    trat_b2.setText(R.string.lipdis34);
                    trat_b3.setText(R.string.ipot);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b4.setText(R.string.stli);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b5.setText(R.string.stdi);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b5.setVisibility(View.VISIBLE);
                    trat_b6.setVisibility(View.GONE);
                }

            }
        });

        trat_a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 2).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){ //viso- ipotonia muscolare
                    prefsEditor.putInt("viso", 2).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1p);
                    trat_a2.setTextColor(Color.BLACK);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.paa);
                    trat_b2.setText(R.string.paoo);
                    trat_b3.setVisibility(View.GONE);
                    //trat_b3.setText(R.string.prp);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);

                }else if (tratt == 2){ //collo - az bios
                    prefsEditor.putInt("collo", 2).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1p);
                    trat_a2.setTextColor(Color.BLACK);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.paa);
                    trat_b2.setText(R.string.paoo);
                    trat_b3.setVisibility(View.GONE);
                    //trat_b3.setText(R.string.prp);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ // corpo - seno
                    prefsEditor.putInt("corpo", 2).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1p);
                    trat_a2.setTextColor(Color.BLACK);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.VISIBLE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1);
                    trat_a6.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.ipot);
                    trat_b2.setText(R.string.stdi);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }
            }
        });

        trat_a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 3).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1p);
                    trat_a3.setTextColor(Color.BLACK);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.paa);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_b2.setText(R.string.paoo);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 2){
                    prefsEditor.putInt("collo", 3).commit();
                    b.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b1.setText(R.string.paa);
                    trat_b2.setText(R.string.paoo);
                    trat_b3.setVisibility(View.GONE);
                    //trat_b3.setText(R.string.prp);
                    trat_b4.setVisibility(View.GONE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1p);
                    trat_a3.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ // corpo - addome
                    prefsEditor.putInt("corpo", 3).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1p);
                    trat_a3.setTextColor(Color.BLACK);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.VISIBLE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1);
                    trat_a6.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.azli);
                    trat_b2.setText(R.string.ipot);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b3.setText(R.string.stdi);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }


            }
        });

        trat_a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 4).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    b.setVisibility(View.GONE);
                }else if (tratt == 2){
                    b.setVisibility(View.GONE);
                }else if (tratt == 3){ //corpo - dorso
                    prefsEditor.putInt("corpo", 4).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.VISIBLE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_c1.setVisibility(View.GONE);
                    trat_c1.setText(R.string.paa);
                    trat_c2.setVisibility(View.GONE);
                    trat_c2.setText(R.string.paoo);
                    trat_c3.setVisibility(View.GONE);
                    trat_c3.setText(R.string.prp);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1p);
                    trat_a4.setTextColor(Color.BLACK);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.VISIBLE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1);
                    trat_a6.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.azli);
                    trat_b2.setVisibility(View.GONE);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }
            }
        });

        trat_a5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 5).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    b.setVisibility(View.GONE);
                }else if (tratt == 2){
                    b.setVisibility(View.VISIBLE);
                }else if ( tratt == 3){ // corpo glutei

                    prefsEditor.putInt("corpo", 5).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1p);
                    trat_a5.setTextColor(Color.BLACK);
                    trat_a6.setVisibility(View.VISIBLE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1);
                    trat_a6.setTextColor(Color.WHITE);
                    trat_b1.setText(R.string.lipdis12);
                    trat_b2.setText(R.string.lipdis34);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b3.setText(R.string.ipot);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setText(R.string.stli);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b5.setText(R.string.stve);
                    trat_b6.setText(R.string.stdi);
                    trat_b5.setVisibility(View.VISIBLE);
                    trat_b6.setVisibility(View.VISIBLE);
                }
            }
        });
        trat_a6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 6).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    b.setVisibility(View.GONE);
                }else if (tratt == 2){
                    b.setVisibility(View.VISIBLE);
                }else if ( tratt == 3){ //corpo - arti inferiori

                    prefsEditor.putInt("corpo", 6).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.VISIBLE);
                    trat_a6.setBackgroundResource(R.drawable.btn_1p);
                    trat_a6.setTextColor(Color.BLACK);
                    trat_b1.setText(R.string.lipdis12);
                    trat_b2.setText(R.string.lipdis34);
                    trat_b2.setVisibility(View.VISIBLE);
                    trat_b3.setText(R.string.ipot);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setText(R.string.stli);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b5.setText(R.string.stve);
                    trat_b6.setText(R.string.stdi);
                    trat_b5.setVisibility(View.VISIBLE);
                    trat_b6.setVisibility(View.VISIBLE);
                }
            }
        });

        trat_b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf3", 1).commit();

                int viso = prefs.getInt("viso", 0);
                int collo = prefs.getInt("collo", 0);
                int corpo = prefs.getInt("corpo", 0);
                tratt = prefs.getInt("tratt_rf1", 0);
                tratt1 = prefs.getInt("tratt_rf2", 0);
                System.out.println("trat = " + tratt + "tratt1 ^" + tratt1);
                if (tratt == 1){ //viso
                    if (viso ==1 ){
                        prefsEditor.putInt("cursore", 1).commit();
                        prefsEditor.putInt("viso1", 1).commit();
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("visro frontale");
                    }else if (viso == 2){
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        System.out.println("visro ipotonia paa");
                        fai_query("13");
                    }
                }else if (tratt == 2){ // rughe
                    if (collo == 1){
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        System.out.println("collo rughe p.a.allo");
                        fai_query("16");
                    }else if (collo ==2){
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
//                        c.setVisibility(View.GONE);
//                        trat_c3.setVisibility(View.GONE);
//                        trat_c4.setVisibility(View.GONE);
//                        trat_c1.setText(R.string.paa);
//                        trat_c2.setText(R.string.paoo);
//                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
//                        trat_b1.setTextColor(Color.BLACK);
//                        trat_b2.setBackgroundResource(R.drawable.btn_1);
//                        trat_b2.setTextColor(Color.WHITE);
//                        trat_b3.setBackgroundResource(R.drawable.btn_1);
//                        trat_b3.setTextColor(Color.WHITE);
//                        trat_b4.setBackgroundResource(R.drawable.btn_1);
//                        trat_b4.setTextColor(Color.WHITE);
//                        trat_b5.setBackgroundResource(R.drawable.btn_1);
//                        trat_b5.setTextColor(Color.WHITE);
                        System.out.println("collo az bio p.a.allo");
//                        trat_b6.setBackgroundResource(R.drawable.btn_1);
//                        trat_b6.setTextColor(Color.WHITE);
                        //TODO fai query
                        fai_query("19");

                    }else if (collo ==3){
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);


//                        c.setVisibility(View.GONE);
//                        trat_c3.setVisibility(View.GONE);
//                        trat_c4.setVisibility(View.GONE);
//                        trat_c1.setText(R.string.paa);
//                        trat_c2.setText(R.string.paoo);
//                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
//                        trat_b1.setTextColor(Color.BLACK);
//                        trat_b2.setBackgroundResource(R.drawable.btn_1);
//                        trat_b2.setTextColor(Color.WHITE);
//                        trat_b3.setBackgroundResource(R.drawable.btn_1);
//                        trat_b3.setTextColor(Color.WHITE);
//                        trat_b4.setBackgroundResource(R.drawable.btn_1);
//                        trat_b4.setTextColor(Color.WHITE);
//                        trat_b5.setBackgroundResource(R.drawable.btn_1);
//                        trat_b5.setTextColor(Color.WHITE);
//                        trat_b6.setBackgroundResource(R.drawable.btn_1);
//                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("collo dermo care p.a.allo");
                        //TODO fai query
                        fai_query("22");

                    }

                }else if (tratt == 3){
                    System.out.println("corpo =" + corpo);
                    if (corpo ==1){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo artsup lipodistrofia 1/2");
                        prefsEditor.putInt("cursore", 5).commit();
                    }else if (corpo == 2){
                        prefsEditor.putInt("cursore", 10).commit();
                        System.out.println("****seno ipotonia");
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 3){
                        prefsEditor.putInt("cursore", 12).commit();
                        System.out.println("corpo addome az lip");

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 4){
                        prefsEditor.putInt("cursore", 15).commit();
                        System.out.println("corpo dosro az lip");

                        c.setVisibility(View.VISIBLE);
                        trat_c1.setVisibility(View.VISIBLE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setVisibility(View.VISIBLE);
                        trat_c2.setText(R.string.paoo);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 5){
                        System.out.println("corpo glutei lipodistrofia 12");
                        prefsEditor.putInt("cursore", 16).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 6){
                        System.out.println("corpo art inf lipodistrofia 12");
                        prefsEditor.putInt("cursore", 22).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
                        trat_b1.setTextColor(Color.BLACK);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }
                }
            }
        });
        trat_b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf3", 2).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                int viso = prefs.getInt("viso", 0);
                int collo = prefs.getInt("collo", 0);
                int corpo = prefs.getInt("corpo", 0);
                if (tratt == 1){
                    if (viso ==1 ){
                        prefsEditor.putInt("viso1", 2).commit();
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("viso perioculare");
                        prefsEditor.putInt("cursore", 2).commit();
                    }else if (viso == 2){
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        System.out.println("visro ipotonia pao");
                        fai_query("14");
                    }

                }else if (tratt == 2){
                    if (collo == 1){
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
//                        c.setVisibility(View.VISIBLE);
//                        trat_c3.setVisibility(View.GONE);
//                        trat_c4.setVisibility(View.GONE);
//                        trat_c1.setText(R.string.paa);
//                        trat_c2.setText(R.string.paoo);
//                        trat_b1.setBackgroundResource(R.drawable.btn_1);
//                        trat_b1.setTextColor(Color.WHITE);
//                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
//                        trat_b2.setTextColor(Color.BLACK);
//                        trat_b3.setBackgroundResource(R.drawable.btn_1);
//                        trat_b3.setTextColor(Color.WHITE);
//                        trat_b4.setBackgroundResource(R.drawable.btn_1);
//                        trat_b4.setTextColor(Color.WHITE);
//                        trat_b5.setBackgroundResource(R.drawable.btn_1);
//                        trat_b5.setTextColor(Color.WHITE);
//                        trat_b6.setBackgroundResource(R.drawable.btn_1);
//                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("collo rughe p.a.omo");
                        fai_query("17");
                    }else if (collo ==2){
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
//                        c.setVisibility(View.VISIBLE);
//                        trat_c3.setVisibility(View.GONE);
//                        trat_c4.setVisibility(View.GONE);
//                        trat_c1.setText(R.string.paa);
//                        trat_c2.setText(R.string.paoo);
//                        trat_b1.setBackgroundResource(R.drawable.btn_1p);
//                        trat_b1.setTextColor(Color.BLACK);
//                        trat_b2.setBackgroundResource(R.drawable.btn_1);
//                        trat_b2.setTextColor(Color.WHITE);
//                        trat_b3.setBackgroundResource(R.drawable.btn_1);
//                        trat_b3.setTextColor(Color.WHITE);
//                        trat_b4.setBackgroundResource(R.drawable.btn_1);
//                        trat_b4.setTextColor(Color.WHITE);
//                        trat_b5.setBackgroundResource(R.drawable.btn_1);
//                        trat_b5.setTextColor(Color.WHITE);
//                        trat_b6.setBackgroundResource(R.drawable.btn_1);
//                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("collo az bio p.a.omo");
                        fai_query("20");
                    }else if (collo ==3){
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
//                        c.setVisibility(View.VISIBLE);
//                        trat_c3.setVisibility(View.GONE);
//                        trat_c4.setVisibility(View.GONE);
//                        trat_c1.setText(R.string.paa);
//                        trat_c2.setText(R.string.paoo);
//                        trat_b1.setBackgroundResource(R.drawable.btn_1);
//                        trat_b1.setTextColor(Color.WHITE);
//                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
//                        trat_b2.setTextColor(Color.BLACK);
//                        trat_b3.setBackgroundResource(R.drawable.btn_1);
//                        trat_b3.setTextColor(Color.WHITE);
//                        trat_b4.setBackgroundResource(R.drawable.btn_1);
//                        trat_b4.setTextColor(Color.WHITE);
//                        trat_b5.setBackgroundResource(R.drawable.btn_1);
//                        trat_b5.setTextColor(Color.WHITE);
//                        trat_b6.setBackgroundResource(R.drawable.btn_1);
//                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("collo dermo care p.a.omo");
                        fai_query("23");
                    }
                }else if (tratt == 3){
                    System.out.println("corpo =" + corpo);
                    if (corpo ==1){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo artsup lipodistrofia 34");
                        prefsEditor.putInt("cursore", 6).commit();
                    }else if (corpo == 2){
                        System.out.println("corpo seno strie sist");
                        prefsEditor.putInt("cursore", 11).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 3){
                        System.out.println("corpo addome ipotonia");
                        prefsEditor.putInt("cursore", 13).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 4){
                        //
                    }else if (corpo == 5){
                        System.out.println("corpo  glutei lipo 34");
                        prefsEditor.putInt("cursore", 17).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }else if (corpo == 6){
                        System.out.println("corpo arti inf lipo 34");
                        prefsEditor.putInt("cursore", 23).commit();

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1p);
                        trat_b2.setTextColor(Color.BLACK);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                    }
                }
            }
        });
        trat_b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf3", 3).commit();
                tratt = prefs.getInt("tratt_rf1", 0);

                int viso = prefs.getInt("viso", 0);
                int collo = prefs.getInt("collo", 0);
                int corpo = prefs.getInt("corpo", 0);
                if (tratt == 1){
                    if (viso == 1){
                        prefsEditor.putInt("viso1",3).commit();
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("viso naso-labbiale");
                        prefsEditor.putInt("cursore", 3).commit();
                    }
                    else if (viso == 2) { // ipotonia prp
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        fai_query("15");
                    }

                }else if (tratt == 2){
                    if (collo == 1) {
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        fai_query("18");
                    }
                    else if ( collo == 2 ) {
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        fai_query("21");

                    }
                    else if ( collo == 3 ) {
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        fai_query("24");

                    }
                }else if (tratt == 3){
                    System.out.println("corpo =" + corpo);
                    if (corpo ==1){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo artsup ipotonia");
                        prefsEditor.putInt("cursore", 7).commit();
                    }else if (corpo == 2){
                        //
                    }else if (corpo == 3){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);

                        System.out.println("corpo addome strie distense");
                        prefsEditor.putInt("cursore", 14).commit();
                    }else if (corpo == 4){
                        //
                    }else if (corpo == 5){

                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);

                        System.out.println("corpo glutei ipotonia");
                        prefsEditor.putInt("cursore", 18).commit();
                    }else if (corpo == 6){


                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1p);
                        trat_b3.setTextColor(Color.BLACK);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);

                        System.out.println("corpo arti inf ipotonia");
                        prefsEditor.putInt("cursore", 24).commit();
                    }

                }
            }
        });


        trat_b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf3", 4).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                int viso = prefs.getInt("viso", 0);
                int collo = prefs.getInt("collo", 0);
                int corpo = prefs.getInt("corpo", 0);
                if (tratt == 1){
                    if (viso == 1) {
                        prefsEditor.putInt("viso1", 4).commit();
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1p);
                        trat_b4.setTextColor(Color.BLACK);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("viso glabellare");

                        prefsEditor.putInt("cursore", 4).commit();
                    }
                }else if (tratt == 2){

                }else if (tratt == 3){
                    System.out.println("corpo =" + corpo);
                    if (corpo ==1){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1p);
                        trat_b4.setTextColor(Color.BLACK);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo artsup stasi linfatica");
                        prefsEditor.putInt("cursore",8).commit();
                    }else if (corpo == 2){

                        System.out.println("out11");
                        //
                    }else if (corpo == 3){

                        System.out.println("out12");
                        //
                    }else if (corpo == 4){

                        System.out.println("out13");
                        //
                    }else if (corpo == 5){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1p);
                        trat_b4.setTextColor(Color.BLACK);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);

                        System.out.println("corpo glutei stasi linfatica");
                        prefsEditor.putInt("cursore", 19).commit();
                    }else if (corpo == 6){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1p);
                        trat_b4.setTextColor(Color.BLACK);
                        trat_b5.setBackgroundResource(R.drawable.btn_1);
                        trat_b5.setTextColor(Color.WHITE);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);

                        System.out.println("corpo arti inf stasi linfatica");
                        prefsEditor.putInt("cursore", 25).commit();
                    }
                }
            }
        });

        trat_b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf3", 5).commit();
                int viso = prefs.getInt("viso", 0);
                int collo = prefs.getInt("collo", 0);
                int corpo = prefs.getInt("corpo", 0);
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                    if (viso == 1) {


                    }
                }else if (tratt == 2){
                }else if (tratt == 3){
                    System.out.println("corpo =" + corpo);
                    if (corpo ==1){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1p);
                        trat_b5.setTextColor(Color.BLACK);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo glutei strie distense");
                        prefsEditor.putInt("cursore", 9).commit();
                    }else if (corpo == 2){
                        System.out.println("out1");
                        //
                    }else if (corpo == 3){
                        System.out.println("coout2");
                        //
                    }else if (corpo == 4){
                        System.out.println("out3");
                        //
                    }else if (corpo == 5){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1p);
                        trat_b5.setTextColor(Color.BLACK);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo glutei strie venosa");

                        prefsEditor.putInt("cursore", 20).commit();
                    }else if (corpo == 6){
                        c.setVisibility(View.VISIBLE);
                        trat_c3.setVisibility(View.GONE);
                        //trat_c3.setText(R.string.prp);
                        trat_c4.setVisibility(View.GONE);
                        trat_c1.setText(R.string.paa);
                        trat_c2.setText(R.string.paoo);
                        trat_b1.setBackgroundResource(R.drawable.btn_1);
                        trat_b1.setTextColor(Color.WHITE);
                        trat_b2.setBackgroundResource(R.drawable.btn_1);
                        trat_b2.setTextColor(Color.WHITE);
                        trat_b3.setBackgroundResource(R.drawable.btn_1);
                        trat_b3.setTextColor(Color.WHITE);
                        trat_b4.setBackgroundResource(R.drawable.btn_1);
                        trat_b4.setTextColor(Color.WHITE);
                        trat_b5.setBackgroundResource(R.drawable.btn_1p);
                        trat_b5.setTextColor(Color.BLACK);
                        trat_b6.setBackgroundResource(R.drawable.btn_1);
                        trat_b6.setTextColor(Color.WHITE);
                        System.out.println("corpo arti inf stasi venosa");
                        prefsEditor.putInt("cursore", 26).commit();
                    }
                }

            }
        });

        trat_b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                int corpo = prefs.getInt("corpo", 0);
                if (corpo == 5) {

                    c.setVisibility(View.VISIBLE);
                    trat_c3.setVisibility(View.GONE);
                    //trat_c3.setText(R.string.prp);
                    trat_c4.setVisibility(View.GONE);
                    trat_c1.setText(R.string.paa);
                    trat_c2.setText(R.string.paoo);
                    trat_b1.setBackgroundResource(R.drawable.btn_1);
                    trat_b1.setTextColor(Color.WHITE);
                    trat_b2.setBackgroundResource(R.drawable.btn_1);
                    trat_b2.setTextColor(Color.WHITE);
                    trat_b3.setBackgroundResource(R.drawable.btn_1);
                    trat_b3.setTextColor(Color.WHITE);
                    trat_b4.setBackgroundResource(R.drawable.btn_1);
                    trat_b4.setTextColor(Color.WHITE);
                    trat_b5.setBackgroundResource(R.drawable.btn_1);
                    trat_b5.setTextColor(Color.WHITE);
                    trat_b6.setBackgroundResource(R.drawable.btn_1p);
                    trat_b6.setTextColor(Color.BLACK);
                    System.out.println("corpo glutei strie distense");

                    prefsEditor.putInt("cursore", 21).commit();
                } else if (corpo == 6) {
                    c.setVisibility(View.VISIBLE);
                    trat_c3.setVisibility(View.GONE);
                    //trat_c3.setText(R.string.prp);
                    trat_c4.setVisibility(View.GONE);
                    trat_c1.setText(R.string.paa);
                    trat_c2.setText(R.string.paoo);
                    trat_b1.setBackgroundResource(R.drawable.btn_1);
                    trat_b1.setTextColor(Color.WHITE);
                    trat_b2.setBackgroundResource(R.drawable.btn_1);
                    trat_b2.setTextColor(Color.WHITE);
                    trat_b3.setBackgroundResource(R.drawable.btn_1);
                    trat_b3.setTextColor(Color.WHITE);
                    trat_b4.setBackgroundResource(R.drawable.btn_1);
                    trat_b4.setTextColor(Color.WHITE);
                    trat_b5.setBackgroundResource(R.drawable.btn_1);
                    trat_b5.setTextColor(Color.WHITE);
                    trat_b6.setBackgroundResource(R.drawable.btn_1p);
                    trat_b6.setTextColor(Color.BLACK);
                    System.out.println("corpo arti inf stasi distense");
                    prefsEditor.putInt("cursore", 27).commit();
                }
            }
        });




        trat_c1.setOnClickListener(new View.OnClickListener() { //pa allo
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                switch (cursore) {
                    case 1:
                        fai_query("1");
                        break;
                    case 2:
                        fai_query("4");
                        break;
                    case 3:
                        fai_query("7");
                        break;
                    case 4:
                        fai_query("10");
                        break;
                    case 5:
                        fai_query("25");
                        break;
                    case 6:
                        fai_query("28");
                        break;
                    case 7:
                        fai_query("31");
                        break;
                    case 8:
                        fai_query("34");
                        break;
                    case 9:
                        fai_query("37");
                        break;
                    case 10:
                        fai_query("40");
                        break;
                    case 11:
                        fai_query("43");
                        break;
                    case 12:
                        fai_query("46");
                        break;
                    case 13:
                        fai_query("49");
                        break;
                    case 14:
                        fai_query("52");
                        break;
                    case 15:
                        fai_query("55");
                        break;
                    case 16:
                        fai_query("58");
                        break;
                    case 17:
                        fai_query("61");
                        break;
                    case 18:
                        fai_query("64");
                        break;
                    case 19:
                        fai_query("67");
                        break;
                    case 20:
                        fai_query("70");
                        break;
                    case 21:
                        fai_query("73");
                        break;
                    case 22:
                        fai_query("76");
                        break;
                    case 23:
                        fai_query("79");
                        break;
                    case 24:
                        fai_query("82");
                        break;
                    case 25:
                        fai_query("85");
                        break;
                    case 26:
                        fai_query("88");
                        break;
                    case 27:
                        fai_query("91");
                        break;
                }
            }
        });

        trat_c2.setOnClickListener(new View.OnClickListener() { //pa omo
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                switch (cursore) {
                    case 1:
                        fai_query("2");
                        break;
                    case 2:
                        fai_query("5");
                        break;
                    case 3:
                        fai_query("8");
                        break;
                    case 4:
                        fai_query("11");
                        break;
                    case 5:
                        fai_query("26");
                        break;
                    case 6:
                        fai_query("29");
                        break;
                    case 7:
                        fai_query("32");
                        break;
                    case 8:
                        fai_query("35");
                        break;
                    case 9:
                        fai_query("38");
                        break;
                    case 10:
                        fai_query("41");
                        break;
                    case 11:
                        fai_query("44");
                        break;
                    case 12:
                        fai_query("47");
                        break;
                    case 13:
                        fai_query("50");
                        break;
                    case 14:
                        fai_query("53");
                        break;
                    case 15:
                        fai_query("56");
                        break;
                    case 16:
                        fai_query("59");
                        break;
                    case 17:
                        fai_query("62");
                        break;
                    case 18:
                        fai_query("65");
                        break;
                    case 19:
                        fai_query("68");
                        break;
                    case 20:
                        fai_query("71");
                        break;
                    case 21:
                        fai_query("74");
                        break;
                    case 22:
                        fai_query("77");
                        break;
                    case 23:
                        fai_query("80");
                        break;
                    case 24:
                        fai_query("83");
                        break;
                    case 25:
                        fai_query("86");
                        break;
                    case 26:
                        fai_query("89");
                        break;
                    case 27:
                        fai_query("92");
                        break;
                }

            }
        });

        trat_c3.setOnClickListener(new View.OnClickListener() {  //prp velvet
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                switch (cursore) {
                    case 1:
                        fai_query("3");
                        break;
                    case 2:
                        fai_query("6");
                        break;
                    case 3:
                        fai_query("9");
                        break;
                    case 4:
                        fai_query("12");
                        break;
                    case 5:
                        fai_query("27");
                        break;
                    case 6:
                        fai_query("30");
                        break;
                    case 7:
                        fai_query("33");
                        break;
                    case 8:
                        fai_query("36");
                        break;
                    case 9:
                        fai_query("39");
                        break;
                    case 10:
                        fai_query("42");
                        break;
                    case 11:
                        fai_query("45");
                        break;
                    case 12:
                        fai_query("48");
                        break;
                    case 13:
                        fai_query("51");
                        break;
                    case 14:
                        fai_query("54");
                        break;
                    case 15:
                        fai_query("57");
                        break;
                    case 16:
                        fai_query("60");
                        break;
                    case 17:
                        fai_query("63");
                        break;
                    case 18:
                        fai_query("66");
                        break;
                    case 19:
                        fai_query("69");
                        break;
                    case 20:
                        fai_query("72");
                        break;
                    case 21:
                        fai_query("75");
                        break;
                    case 22:
                        fai_query("78");
                        break;
                    case 23:
                        fai_query("81");
                        break;
                    case 24:
                        fai_query("84");
                        break;
                    case 25:
                        fai_query("87");
                        break;
                    case 26:
                        fai_query("90");
                        break;
                    case 27:
                        fai_query("93");
                        break;
                }

            }
        });
    }






    public void fai_query(String id){
        mDb.open();
        Cursor c=mDb.fetchTrateLETTROPORA_id(id);
        startManagingCursor(c);

        /*    private static final String CREA_TABELLA_ELETTROPORAZIONE = "CREATE TABLE "
            + MetaDatiTrattamentoElettropora.ELETTROPORAZIONE + " ("
            + MetaDatiTrattamentoElettropora.ID+ " integer primary key autoincrement, "
            + MetaDatiTrattamentoElettropora.TRATTAMENTO + " text not null, "
            + MetaDatiTrattamentoElettropora.TEMPO_MIN + " integer, "
            + MetaDatiTrattamentoElettropora.A + " integer, "
            + MetaDatiTrattamentoElettropora.B + " integer, "
            + MetaDatiTrattamentoElettropora.C + " integer, "
            + MetaDatiTrattamentoElettropora.C1 + " integer, "
            + MetaDatiTrattamentoElettropora.D + " integer, "
            + MetaDatiTrattamentoElettropora.FORMA_ONDA + " text, "
            + MetaDatiTrattamentoElettropora.MANIPOLO + " text, "
            + MetaDatiTrattamentoElettropora.PIASTRA + " text, "
            + MetaDatiTrattamentoElettropora.PEDALIERA + " text);";
            */
        int nome_trattamento=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.TRATTAMENTO);  //indici delle colonne
        int tempo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.TEMPO_MIN);
        int a=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.A);
        int b=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.B);
        int c1=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.C);
        int c2=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.C1);
        int d=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.D);
        int forma_onda =c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.FORMA_ONDA);
        int manipolo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.MANIPOLO);
        int piastra=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.PIASTRA);
        int pedaliera=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.PEDALIERA);


        if(c.moveToFirst()){  //se va alla prima entry, il cursore non è vuoto
            do {

                System.out.println("Nome trattamento:"+c.getString(nome_trattamento)+"\n tempo:"+c.getInt(tempo)+"\n a:"+c.getInt(a)
                        +"\n b:"+c.getInt(b)+"\n c1:"+c.getInt(c1)+"\n c2"+c.getString(c2)
                        +"\n d:"+c.getString(d)+"\n forma onda:"+c.getString(forma_onda)+"\n manip:"+c.getString(manipolo)+"\n piastra:"+c.getString(piastra)
                        +"\n pedaliera:"+c.getString(pedaliera)); //estrazione dei dati dalla entry del cursor

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("A", c.getInt(a));
                prefsEditor.putInt("B", c.getInt(b));
                prefsEditor.putInt("C1", c.getInt(c1));
                prefsEditor.putInt("C2", c.getInt(c2));
                prefsEditor.putInt("D", c.getInt(d));
                prefsEditor.putString("FORMA ONDA", c.getString(forma_onda));
                prefsEditor.putString("MANIPOLO", c.getString(manipolo));
                prefsEditor.putString("PIASTRA", c.getString(piastra));
                prefsEditor.putString("PEDALIERA",c.getString(pedaliera));
                prefsEditor.commit();

            } while (c.moveToNext());//iteriamo al prossimo elemento
        }
        mDb.close();
        stopManagingCursor(c);
        c.close();

        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Immissione_diretta2ME_db.class);
        startActivityForResult(CauseNelleVic, 0);
        finish();

    }



    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

    public void back (View view){
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
        finish();
    }
}