package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Trattamenti extends Activity {
    int db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        Button seleziona_trattamento = findViewById(R.id.seleziona_trattamento);
        //
        String SharedPrefName = "Reserved";

        db = reserved.getInt("DATABASE", 0);
        if (db == 1) {
            seleziona_trattamento.setEnabled(true);
            seleziona_trattamento.setBackgroundResource(R.drawable.btn_menu_princ);
        } else {
            seleziona_trattamento.setEnabled(false);
            seleziona_trattamento.setBackgroundResource(R.drawable.dbt1dis);
        }

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();


        int trattamento_1 = prefs.getInt("trattamento", 0);


        Button immissione_diretta = findViewById(R.id.immissione_diretta);
        Button immissione_remoto = findViewById(R.id.immissione_remoto);
        Button assistenza = findViewById(R.id.assistenza);

        if (trattamento_1 == 3) { //vacuum
            immissione_diretta.setVisibility(View.VISIBLE);
            immissione_remoto.setVisibility(View.VISIBLE);
            seleziona_trattamento.setVisibility(View.GONE);
        }
        if (reserved.getInt("INDUSTRIA4",0)==0){
            immissione_remoto.setVisibility(View.GONE);
        }else{
            immissione_remoto.setVisibility(View.VISIBLE);
        }

        int trattamento = prefs.getInt("trattamento_1", 0);

        immissione_diretta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trattamento_1 == 3) {
                    prefsEditor.putString("Nome Trattamento", getString(R.string.vacuum)).commit();
                    Intent i = new Intent(getApplicationContext(), Imm_Diretta_vacuum.class);
                    startActivity(i);
                } else if (trattamento == 1) {
                    Intent i = new Intent(getApplicationContext(), Imm_Diretta_ou.class);
                    startActivity(i);
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir)).commit();
                } else if (trattamento == 3) {
                    Intent i = new Intent(getApplicationContext(), Imm_Diretta_ou.class);
                    startActivity(i);
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir_ou_fis)).commit();
                } else if (trattamento == 5) {
                    Intent i = new Intent(getApplicationContext(), Imm_Diretta_ou.class);
                    startActivity(i);
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir_ou_andr)).commit();

                } else if (trattamento == 2) {
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir2)).commit();
                    Intent i = new Intent(getApplicationContext(), Imm_dir_EL.class);
                    startActivity(i);
                } else if (trattamento == 4) {
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir3)).commit();
                    Intent i = new Intent(getApplicationContext(), Imm_dir_EL.class);
                    startActivity(i);

                } else if (trattamento == 6) {
                    prefsEditor.putString("Nome Trattamento", getString(R.string.imm_dir4)).commit();
                    Intent i = new Intent(getApplicationContext(), Imm_dir_EL.class);
                    startActivity(i);
                }
            }
        });
        final SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        immissione_remoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shared.getString("connessioneServer", null) != null && shared.getString("connessioneServer", null).equals("connesso")) {
                    if (trattamento_1 == 3) {
                         Intent i = new Intent(getApplicationContext(), JSON_Vacuum.class);
                        startActivity(i);
                    } else if (trattamento == 1) { //ou - me
                        startActivity(new Intent(getApplicationContext(), Onde_JSON.class));
                    } else if (trattamento == 2) { // el - me
                        startActivity(new Intent(getApplicationContext(), Visualizza_JSON_Remoto_Elettroporazione_MedEst.class));

                    } else if (trattamento == 3) { // ou - fisio
                        startActivity(new Intent(getApplicationContext(), Onde_JSON_fis.class));
                    } else if (trattamento == 4) { //el -fisio
                        startActivity(new Intent(getApplicationContext(), Visualizza_JSON_Remoto_ElFis.class));

                    } else if (trattamento == 5) { // ou - andr
                        startActivity(new Intent(getApplicationContext(), Onde_JSON_andr.class));
                    } else if (trattamento == 6) { // el - andr
                        startActivity(new Intent(getApplicationContext(), Visualizza_JSON_Remoto_Elettroporazione_andr.class));
                    }
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginFlend.class));
                    Toast.makeText(getApplicationContext(), getString(R.string.immrem), Toast.LENGTH_LONG).show();
                }
            }
        });

        seleziona_trattamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (trattamento == 1) { //ou - me
                    Intent i = new Intent(getApplicationContext(), Trattamenti_ou.class);
                    startActivity(i);
                } else if (trattamento == 2) { // el - me
                    Intent i = new Intent(getApplicationContext(), Trattamenti_el.class);
                    startActivity(i);
                } else if (trattamento == 3) { // ou - fisio
                    Intent i = new Intent(getApplicationContext(), Trattamenti_ou_fis.class);
                    startActivity(i);
                } else if (trattamento == 4) { //el -fisio
                    Intent i = new Intent(getApplicationContext(), Trattamenti_el_fis.class);
                    startActivity(i);
                } else if (trattamento == 5) { // ou - andr
                    Intent i = new Intent(getApplicationContext(), Trattamenti_ou_andr.class);
                    startActivity(i);
                } else if (trattamento == 6) { // el - andr
                    Intent i = new Intent(getApplicationContext(), Trattamenti_el_andr.class);
                    startActivity(i);
                }

            }
        });

        assistenza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Assistenza_tecnica.class);
                startActivity(i);

            }
        });

    }

    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);

    }

    public void esci(View view) {
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
    }
}