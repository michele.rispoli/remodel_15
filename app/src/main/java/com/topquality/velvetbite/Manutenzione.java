package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Manutenzione extends Activity {

    public int k = 0;
    public Integer [] tmp = {-1,-1,-1,-1, -1};
    static final int CONFIRM_DIALOG = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manutenzione);
        parseResult();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int leng = sp.getInt("LINGUA", 1);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
//        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
//        if (reseller == 0){
//            sfondo.setBackgroundResource(R.drawable.sfondo2);
//        }else if (reseller == 1){
//            sfondo.setBackgroundResource(R.drawable.sfondotqg);
//        }
    }


    /* CREAZIONE TASTIERA E CHECK PASSWORD */
    //ATTUALMENTE IL PIN E' DA 4 CARATTERI

    public void parseResult(){

        //GESTIONE BOTTONE 1
        ImageButton one = (ImageButton) findViewById(R.id.loginButton1);
        one.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 1;
                                if(k == 0){
                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;

                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;

                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });

        // GESTIONE BOTTONE 2
        ImageButton two = (ImageButton) findViewById(R.id.loginButton2);
        two.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 2;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        //GESTIONE BOTTONE 3
        ImageButton three = (ImageButton) findViewById(R.id.loginButton3);
        three.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 3;
                                if(k == 0){
                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 4
        ImageButton four = (ImageButton) findViewById(R.id.loginButton4);
        four.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 4;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 5
        ImageButton five = (ImageButton) findViewById(R.id.loginButton5);
        five.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 5;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 6
        ImageButton six = (ImageButton) findViewById(R.id.loginButton6);
        six.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 6;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 7
        ImageButton seven = (ImageButton) findViewById(R.id.loginButton7);
        seven.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 7;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 8
        ImageButton eight = (ImageButton) findViewById(R.id.loginButton8);
        eight.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 8;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 8
        ImageButton nine = (ImageButton) findViewById(R.id.loginButton9);
        nine.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 9;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }
                        }
                    }
                });
        // GESTIONE BOTTONE 9
        ImageButton zero = (ImageButton) findViewById(R.id.loginButton0);
        zero.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k >= 5){
                            System.out.println("Out of Bounds");
                            k = 5;
                        }else{
                            if(tmp[k] == -1){
                                tmp[k] = 0;
                                if(k == 0){

                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle_full);
                                    k++;
                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 5;
                                }
                            }

                        }
                    }
                });
        // GESTIONE BOTTONE CANC
        ImageButton cancel = (ImageButton) findViewById(R.id.loginButtonC);
        cancel.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(k <= 0){
                            k = 0;
                        }else{
                            k--;
                            if(tmp[k] != -1){
                                tmp[k] = -1;
                                if(k == 0){
                                    ImageView circle = (ImageView) findViewById(R.id.firstEntryLogin);
                                    circle.setImageResource(R.drawable.circle);
                                }else if(k == 1){
                                    ImageView circle = (ImageView) findViewById(R.id.secondEntryLogin);
                                    circle.setImageResource(R.drawable.circle);
                                }else if(k == 2){
                                    ImageView circle = (ImageView) findViewById(R.id.thirdEntryLogin);
                                    circle.setImageResource(R.drawable.circle);
                                }else if(k == 3){
                                    ImageView circle = (ImageView) findViewById(R.id.fourthEntryLogin);
                                    circle.setImageResource(R.drawable.circle);
                                }else if(k == 4){
                                    ImageView circle = (ImageView) findViewById(R.id.fifthEntryLogin);
                                    circle.setImageResource(R.drawable.circle);

                                }else{
                                    System.out.println("Out of Bounds");
                                    k = 0;
                                }
                            }
                        }

                    }
                });
        // GESTIONE BOTTONE OK
        ImageButton check = (ImageButton) findViewById(R.id.loginButtonOK);
        check.setOnClickListener(
                new ImageButton.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        toNextState(v);
                    }
                });

    }

    //CHECK PASSWORD
    public void toNextState(View v){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int pass = sp.getInt("pass", 89632);
        boolean res = checkPassword(pass);
        if(res){
            Intent i = new Intent(getApplicationContext(), PannelloAssistenza.class);
            startActivity(i);
            //toHome(v);
            //String t = getString(R.string.ben2);
            //System.out.println(t);
            //t1(t, .QUEUE_FLUSH, null);
            //showDialog(CONFIRM_DIALOG);//TEST AUDIO
        }else{
            resetPass();
            //String t = getString(R.string.check_pass_err);
            //System.out.println(t);
            //t2(t, .QUEUE_FLUSH, null);

            /* MESSAGGIO DI PASSWORD ERRATA */
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout,(ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.check_pass_err);

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
            //setContentView(R.layout.activity_main);
            /*
            Toast toast = Toast.makeText(this, getString(R.string.check_pass_err), Toast.LENGTH_LONG);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
            */
        }
    }
    public boolean checkPassword(int pass){
        int x = 0;
        int y = 4;
        for(int i = 0; i < 5; i++){

            x += (int) (tmp[i] * Math.pow(10, y));
            y--;
        }
        if(x == pass){
            return true;
        }else {
            return false;
        }
    }
    public void toHome(View view){

        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int code = sp.getInt("code", 0);
        // CONTROLLO SE E' IL PRIMO ACCESSO - SET LINGUA MACCHINA
        if(code <= 1){
            code = 1;
            sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("code", code);
            editor.commit();
            String t = getString(R.string.ben2);
            System.out.println(t);
            showDialog(CONFIRM_DIALOG);//TEST AUDIO

            // NON E' IL PRIMO ACCESSO
        }else if (code == 2){
            final Handler handler = new Handler();
            final Runnable r = new Runnable() {
                public void run() {
                    String t = getString(R.string.ben2);
                    System.out.println(t);
                    showDialog(CONFIRM_DIALOG);//TEST AUDIO
                }
            };
            /*
            handler.postDelayed(r, 3000);
            LinearLayout b = (LinearLayout) findViewById(R.id.reminderid);
            b.setOnClickListener(new RelativeLayout.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    String t = getString(R.string.ben2);
                    System.out.println(t);
                    t1(t, .QUEUE_FLUSH, null);
                    Intent intent = new Intent(getApplicationContext(), MenuPrinc.class);
                    startActivity(intent);
                    finish();
                    handler.removeCallbacks(r);
                }
            });*/
        }
    }

    // RESET DEI CAMPI DELLA PASSWORD (SVUOTO I CERCHI)
    public void resetPass(){

        for(int i = 0; i < 5; i++){
            tmp[i] = -1;
        }
        k = 0;

        ImageView circle1 = (ImageView) findViewById(R.id.firstEntryLogin);
        ImageView circle2 = (ImageView) findViewById(R.id.secondEntryLogin);
        ImageView circle3 = (ImageView) findViewById(R.id.thirdEntryLogin);
        ImageView circle4 = (ImageView) findViewById(R.id.fourthEntryLogin);
        ImageView circle5 = (ImageView) findViewById(R.id.fifthEntryLogin);
        circle1.setImageResource(R.drawable.circle);
        circle2.setImageResource(R.drawable.circle);
        circle3.setImageResource(R.drawable.circle);
        circle4.setImageResource(R.drawable.circle);
        circle5.setImageResource(R.drawable.circle);
    }

    /* CONTROLLO MESSAGGIO AUDIO - TEST AUDIO - IMPLEMENTAZIONE*/
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
            case CONFIRM_DIALOG:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.sentito);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                        int code = sp.getInt("code", 0);
                        System.out.println(code + " STAMPO IL CODICE!!!!!!");
                        if(code <= 1){
                            code = 1;
                            sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putInt("code", code);
                            editor.commit();
                            Intent i = new Intent(getApplicationContext(), PannelloAssistenza.class);
                            startActivity(i);
                            finish();

                        }else if (code == 2){
                            final Handler handler = new Handler();
                            final Runnable r = new Runnable() {
                                public void run() {

                                    String t = getString(R.string.ben2);
                                    System.out.println(t);
                                    Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
                                    startActivity(i);
                                    finish();
                                }
                            };
                        }
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Manutenzione.this, CKaudio.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog = builder.create();
                break;
            default:
                dialog = null;
        }
        return dialog;
    }

    /* FUNZIONI FISSE */
    public void settings(View view) {
/*        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Impostazioni.class);
        startActivityForResult(CauseNelleVic, 0);*/
    }


    public void esci(View view){
/*        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MainActivity2.class);
        startActivityForResult(CauseNelleVic, 0);*/
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void impostazioni(View view) {
/*        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Impostazioni.class);
        startActivityForResult(CauseNelleVic, 0);*/
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}
