package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Scrub_pre  extends Activity {
    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    private SeekBar seekBar;
    private SeekBar seekBar2;
    private TextView textView;
    public int tempo = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;
    Button start1;

    /*******************************************SERIALE **********************************/

    private static final String TAG = "MainActivity";
    String msg = null;
    /*************************************************************SERIALE ****************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrub);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondo2);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        start1 = (Button) findViewById(R.id.start);
        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });
        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Scrub_pre.this, R.raw.errore, 0);
        spoolId = spool.load(Scrub_pre.this, R.raw.errore, 0);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        initializeVariables();

        manipolo = 0;
        int leng = prefs.getInt("LINGUA", 1);



        /* inizializzazione dei paramentri */
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Tempo", 0);
        prefsEditor.putInt("Manipolo", 1);
        prefsEditor.putInt("Elettrodo", 1);
        prefsEditor.putInt("Pedaliera", 1);
        prefsEditor.putString("A", "-1");
        prefsEditor.putInt("B", -1);
        prefsEditor.putInt("C1", -1);
        prefsEditor.putInt("C2", -1);
        prefsEditor.putInt("D", -1);
        /*var ausiliari */
        /* gestione del volume */
        vol_audio = prefs.getInt("Volume", 1);
        prefsEditor.commit();
        updateBarHandler = new Handler();
        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);
        seekBar.setProgress(0);
        textView.setText("0:00");
        //seekBar2.setEnabled(false);
        min.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo - 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();


                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
                return true;
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempo == 0) {
                    tempo = 0;
                    textView.setText("0:00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo - 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();
                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });
        add.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (1 < tempo && tempo < 10) {
                    tempo = tempo + 5;
                    textView.setText(tempo + ":00");
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                } else {
                    tempo = tempo + 5;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo + "minuti";
                    System.out.println(t);
                }
                return true;
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (1 < tempo && tempo < 10) {
                    System.out.println(tempo);
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                } else if (tempo >= 60){
                    tempo = 60;
                    textView.setText(60 + ":00");
                    seekBar.setProgress(60);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", 60);
                    prefsEditor.commit();

                    String t = "" + tempo;
                }else {
                    tempo = tempo + 1;
                    textView.setText(tempo + ":00");
                    seekBar.setProgress(tempo);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Tempo", tempo);
                    prefsEditor.commit();

                    String t = "" + tempo   + "minuti";
                    System.out.println(t);
                }
            }
        });

        start1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                int manipolo_sel = prefs.getInt("Manipolo", 1);

                if (tempo == 0 ) {
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
                    ImageView image = (ImageView) layout.findViewById(R.id.image);
                    image.setImageResource(R.drawable.warning);
                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText(R.string.campi_incompleti);
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                } else {
                    String t = getString(R.string.init_tratt);
                    System.out.println(t);
                    Context context = getBaseContext();
                    Intent MedEst1 = new Intent(context, Scrub_op.class);
                    startActivityForResult(MedEst1, 0);
                }
            }
        });


        /* GESTIONE SEEKBAR */
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                tempo = progress;


                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                tempo = progress;
                int minuti = progress;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Tempo", tempo);
                prefsEditor.commit();
                System.out.println("num salti " + minuti);
                textView.setText(minuti + ":00");
                String t = "" + tempo   + "minuti";
                System.out.println(t);
            }
        });
    }

    /************************************SERIALE*************************************************/


    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
        seekBar2 = (SeekBar) findViewById(R.id.seekBar11);
    }


    public void start(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int manipolo_sel = prefs.getInt("Manipolo", 1);

        if (tempo == 0 ) {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout2, (ViewGroup) findViewById(R.id.toast_layout_root2));
            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.campi_incompleti);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        } else {
            String t = getString(R.string.init_tratt);
            System.out.println(t);
            Context context = getBaseContext();
            Intent MedEst1 = new Intent(context, Scrub_op.class);
            startActivityForResult(MedEst1, 0);
        }
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void esci(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}