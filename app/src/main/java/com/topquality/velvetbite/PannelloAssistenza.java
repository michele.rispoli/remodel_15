package com.topquality.velvetbite;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class PannelloAssistenza extends Activity {
    LogFile log = new LogFile();
    int vol_audio = 0;
    private static final int DIALOG_ALERT_ID = 1;
    String nome_dispositivo;
    int db = 0;
    Button database;
    Button SkipPass;
    int skpass;
    TextView verhw;
    Button Tqg;
    int reseller;
    Button Tqa,industria;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pannello_assistenza);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0) {
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        } else if (reseller == 1) {
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        visualizza_log();
        industria=(Button)findViewById(R.id.industria);
        String SharedPrefName = "Reserved";
        SharedPreferences.Editor prefsEditor = reserved.edit();
        //prefsEditor.putInt("DATABASE", 0);
        SharedPreferences sp1 = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
        String path = sp1.getString("DEVICE", "/dev/ttymxc1");
        final SharedPreferences.Editor spEditor = sp1.edit();
        String ver_hw = sp1.getString("BAUDRATE", "");
        verhw = (TextView) findViewById(R.id.verhw);
        if (ver_hw.equals("57600")) {
            verhw.setText("OLD");
        } else if (ver_hw.equals("19200")) {
            verhw.setText("NEW");

        }

        industria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences res=getSharedPreferences("Reserved",MODE_PRIVATE);
                SharedPreferences.Editor resEditor= res.edit();
                int value,color;
                Button buttonToColor;
                buttonToColor=(Button)findViewById(view.getId());
                if (res.getInt("INDUSTRIA4",0)==0){
                    value=1;
                    color=R.drawable.btn_oro;
                }else{
                    value=0;
                    color=R.drawable.btn_blu;
                }
                resEditor.putInt("INDUSTRIA4",value).commit();
                buttonToColor.setBackgroundResource(color);

            }
        });

        //versione scheda
        Button verhw1 = (Button) findViewById(R.id.ver_hw1);

        verhw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spEditor.putString("BAUDRATE", "57600").commit();
                verhw.setText("OLD");

            }
        });
        Button verhw2 = (Button) findViewById(R.id.ver_hw2);
        verhw2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spEditor.putString("BAUDRATE", "19200").commit();
                verhw.setText("NEW");

            }
        });
        Button set_macchina = (Button) findViewById(R.id.tipologia_macchina);
        //TextView assistenza = (TextView) findViewById(R.id.assistenza);
        database = (Button) findViewById(R.id.database);
        db = reserved.getInt("DATABASE", 0);
        if (db == 1) {
            database.setText("DATABASE\nATTIVO");
            database.setBackgroundResource(R.drawable.btn_d);
        } else if (db == 0) {
            database.setText("DATABASE\nNON ATTIVO");
            database.setBackgroundResource(R.drawable.btn_c2);
        }
        database.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attiva_db();

            }
        });

        reseller = reserved.getInt("RESELLER", 0);
        Tqg = (Button) findViewById(R.id.tqg);
        Tqg.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prefsEditor.putInt("RESELLER", 1).commit();
                        Tqg.setBackgroundResource(R.drawable.btn_d);
                        Tqa.setBackgroundResource(R.drawable.btn29);
                        sfondo.setBackgroundResource(R.drawable.sfondotqg);
                    }
                });
        Tqa = (Button) findViewById(R.id.tqa);
        Tqa.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prefsEditor.putInt("RESELLER", 0).commit();
                        Tqa.setBackgroundResource(R.drawable.btn_d);
                        Tqg.setBackgroundResource(R.drawable.btn29);
                        sfondo.setBackgroundResource(R.drawable.sfondo2);
                    }
                });
        if (reseller == 0) {
            Tqg.setBackgroundResource(R.drawable.btn29);
            Tqa.setBackgroundResource(R.drawable.btn_d);
        } else if (reseller == 1) {
            Tqa.setBackgroundResource(R.drawable.btn29);
            Tqg.setBackgroundResource(R.drawable.btn_d);
        }


        SkipPass = (Button) findViewById(R.id.skippass);
        skpass = reserved.getInt("DATABASE", 0);
        if (skpass == 1) {
            SkipPass.setText("PASSWORD\nDISATTTIVA");
            SkipPass.setBackgroundResource(R.drawable.btn_c2);
        }
//        else if (skpass == 0) {
//            SkipPass.setText("PASSWORD\nATTIVA");
//            SkipPass.setBackgroundResource(R.drawable.btn_d);
//        }
        SkipPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salta_password();

            }
        });


        nome_dispositivo = reserved.getString("TIPOLOGIA_DISPOSITIVO", getString(R.string.tip_macchina1));
        set_macchina.setText(nome_dispositivo);

        //String data = ReadSettings(this);
        //assistenza.setText(data);
        if (nome_dispositivo.equals(null)) {
            set_macchina.setText(R.string.tip_macchina1);
        } else if (nome_dispositivo.equals(R.string.tip_macchina2)) {
            set_macchina.setText(R.string.tip_macchina2);
        } else if (nome_dispositivo.equals(R.string.tip_macchina3)) {
            set_macchina.setText(R.string.tip_macchina3);
        } else if (nome_dispositivo.equals(R.string.tip_macchina4)) {
            set_macchina.setText(R.string.tip_macchina4);
        } else if (nome_dispositivo.equals(R.string.tip_macchina5)) {
            set_macchina.setText(R.string.tip_macchina5);
        } else if (nome_dispositivo.equals(R.string.tip_macchina1)) {
            set_macchina.setText(R.string.tip_macchina1);
        }
        volume();
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        vol_audio = prefs.getInt("Volume", 1);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int leng = sp.getInt("LINGUA", 1);

        /************************************ PULSANTE IMMISSIONE PLC *********************************/
        Button btnPlc = findViewById(R.id.plc);
        btnPlc.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PannelloAssistenza.this);
                final LayoutInflater inflater = getLayoutInflater();
                final View v = inflater.inflate(R.layout.dialog_layout, null);
                builder.setView(v);
                builder.setTitle("Inserisci PLC");
                final EditText plc = v.findViewById(R.id.edtPLC);
                builder.setPositiveButton("Inserisci", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (plc.getText().length() < 6 && plc.getText().length() != 0) {
                            SharedPreferences shared = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                            SharedPreferences.Editor plcEditor = shared.edit();
                            plcEditor.putString("PLC", plc.getText().toString()).commit();
                            Toast.makeText(getApplicationContext(), "PLC inserito", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Inserire un numero valido compreso fra 1 e 6 cifre", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("chiudi", null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    public void exit(View view) {
        openApp(this, "com.android.settings");
        //moveTaskToBack(true);
        finish();
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();

        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
            //new PackageManager.NameNotFoundException();
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }


    public void azzera(View view) {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putLong("TEMPO UTILIZZO ELETTROPORAZIONE", 0);
        prefsEditor.putLong("TEMPO UTILIZZO RADIOFREQUENZA", 0);
        prefsEditor.putLong("TEMPO UTILIZZO ONDEDURTO", 0);
        prefsEditor.commit();
        log.Log("CONTATORI AZZERATI", this);
    }

    // Save settings
    public void WriteSettings(Context context, String data) {
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;

        try {
            fOut = openFileOutput("settings.dat", MODE_PRIVATE);
            long time = System.currentTimeMillis();
            Date date = new Date();
            java.text.DateFormat dateFormat =
                    android.text.format.DateFormat.getDateFormat(getApplicationContext());

            osw = new OutputStreamWriter(fOut);
            osw.write("Time: " + dateFormat.format(time) + " " + data + "\n\n");
            osw.flush();
            Toast.makeText(context, "Settings saved", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.d("VELVETERROR", e.getMessage());
            Toast.makeText(context, "Settings not saved", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                Log.d("VELVETERROR", e.getMessage());
            }
        }
    }

    public void visualizza_log() {
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();

//Get the text file
        File file = new File("data/data/com.topquality.hit3/logfile.txt");

//Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        TextView tv = (TextView) findViewById(R.id.errorlog);

//Set the text
        tv.setText(text.toString());
    }


    public void salta_password() {
        final SharedPreferences prefs = getSharedPreferences("Reserved", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();

        if (skpass == 0) {
            skpass = 1;
            System.out.println(skpass);
            prefsEditor.putInt("SKIPPASS", skpass);
            prefsEditor.commit();
            prefsEditor.putInt("TMPTEST", 0);
            prefsEditor.commit();
            SkipPass.setText("PASSWORD\nDISATTTIVA");
            SkipPass.setBackgroundResource(R.drawable.btn_c2);

        } else if (skpass == 1) {

            skpass = 0;
            System.out.println(skpass);
            prefsEditor.putInt("SKIPPASS", skpass);
            prefsEditor.commit();
            prefsEditor.putInt("TMPTEST", 1500);
            prefsEditor.commit();
            SkipPass.setText("PASSWORD\nATTTIVA");
            SkipPass.setBackgroundResource(R.drawable.btn_d);
        }

    }

    /*public String ReadSettings(Context context){
        FileInputStream fIn = null;
        InputStreamReader isr = null;

        char[] inputBuffer = new char[255];
        String data = null;

        try{
            fIn = openFileInput("settings.dat");
            isr = new InputStreamReader(fIn);
            isr.read(inputBuffer);
            data = new String(inputBuffer);
            Toast.makeText(context, "Settings read",Toast.LENGTH_SHORT).show();
            System.out.println(data + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        catch (Exception e) {

           Log.d("VELVETERROR", e.getMessage());
            Toast.makeText(context, "Settings not read",Toast.LENGTH_SHORT).show();
        }
        finally {
            try {
                isr.close();
                fIn.close();
            } catch (IOException e) {

               Log.d("VELVETERROR", e.getMessage());
            }
        }
        return data;
    }*/
    public void volume() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        vol_audio = prefs.getInt("Volume", 1);
        System.out.println("Volume =" + vol_audio);
        final Button volume = (Button) findViewById(R.id.volume);
        if (vol_audio == 0) {
            volume.setBackgroundResource(R.drawable.vol_off);
            System.out.println("volume");
        } else {
            volume.setBackgroundResource(R.drawable.vol_on);
            System.out.println("muto");
        }
        volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vol_audio == 0) {
                    vol_audio = 1;
                    volume.setBackgroundResource(R.drawable.vol_off);
                    System.out.println("muto");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Volume", vol_audio);
                    prefsEditor.commit();
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                } else {
                    vol_audio = 0;
                    volume.setBackgroundResource(R.drawable.vol_on);
                    System.out.println("volume");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Volume", vol_audio);
                    prefsEditor.commit();
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                }
            }
        });
    }


    public void attiva_db() {
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File("data/data/com.topquality.velvetskin/databases/velvet_skin_intra_plus_rf1");
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = prefs.getInt("DATABASE", 0);
        if (db == 0) {
            db = 1;
            System.out.println(db);
            prefsEditor.putInt("DATABASE", db);
            prefsEditor.commit();
            database.setText("DATABASE\nATTIVO");
            database.setBackgroundResource(R.drawable.btn_d);

            File fdelete = new File(file.getPath());
            System.out.println(file.getPath().toString());
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    System.out.println("file Deleted :" + file.getPath());
                } else {
                    System.out.println("file not Deleted :" + file.getPath());
                }
            }
            popola_db();
        } else if (db == 1) {

            File fdelete = new File(file.getPath());
            System.out.println(file.getPath().toString());
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    System.out.println("file Deleted :" + file.getPath());
                } else {
                    System.out.println("file not Deleted :" + file.getPath());
                }
            }
            db = 0;
            System.out.println(db);
            prefsEditor.putInt("DATABASE", db);
            prefsEditor.commit();
            database.setText("DATABASE\nNON ATTIVO");
            database.setBackgroundResource(R.drawable.btn_c2);
        }

    }


    public void popola_db() {
        VelvetSkinIntraPlusDB_rf1 db = new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        db.open();  //apriamo il db

        if (db.fetchTratRadio1().getCount() == 0) {//inserimento dati RADIOFREQUENZA 1

            db.insertTrattRadio1(getString(R.string.ou1), 10, 10, 6, 8, "2");
            db.insertTrattRadio1(getString(R.string.ou2), 10, 10, 7, 9, "2");
            db.insertTrattRadio1(getString(R.string.ou3), 10, 12, 9, 11, "2");
            db.insertTrattRadio1(getString(R.string.ou4), 10, 12, 4, 5, "3");
            db.insertTrattRadio1(getString(R.string.ou5), 10, 12, 5, 6, "3");
            db.insertTrattRadio1(getString(R.string.ou6), 10, 12, 6, 7, "3");
            db.insertTrattRadio1(getString(R.string.ou7), 10, 12, 5, 6, "3");
            db.insertTrattRadio1(getString(R.string.ou8), 10, 12, 6, 7, "3");
            db.insertTrattRadio1(getString(R.string.ou9), 10, 12, 7, 8, "3");
            db.insertTrattRadio1(getString(R.string.ou10), 10, 12, 6, 7, "3");
            db.insertTrattRadio1(getString(R.string.ou11), 10, 12, 7, 8, "3");
            db.insertTrattRadio1(getString(R.string.ou12), 10, 12, 8, 9, "3");
            db.insertTrattRadio1(getString(R.string.ou13), 10, 12, 6, 7, "3");
            db.insertTrattRadio1(getString(R.string.ou14), 10, 12, 7, 8, "3");
            db.insertTrattRadio1(getString(R.string.ou15), 10, 12, 9, 10, "3");
            db.insertTrattRadio1(getString(R.string.ou16), 10, 12, 7, 12, "2");
            db.insertTrattRadio1(getString(R.string.ou17), 10, 12, 8, 12, "2");
            db.insertTrattRadio1(getString(R.string.ou18), 10, 12, 10, 12, "2");
            db.insertTrattRadio1(getString(R.string.ou19), 10, 12, 3, 12, "3");
            db.insertTrattRadio1(getString(R.string.ou20), 10, 12, 4, 12, "3");
            db.insertTrattRadio1(getString(R.string.ou21), 10, 12, 5, 12, "3");
            db.insertTrattRadio1(getString(R.string.ou22), 10, 16, 4, 16, "x");
            db.insertTrattRadio1(getString(R.string.ou23), 10, 10, 3, 16, "x");
            db.insertTrattRadio1(getString(R.string.ou24), 10, 10, 7, 16, "x");
            db.insertTrattRadio1(getString(R.string.ou25), 10, 10, 1, 10, "x");
            db.insertTrattRadio1(getString(R.string.ou26), 10, 10, 5, 10, "x");
            db.insertTrattRadio1(getString(R.string.ou27), 10, 16, 14, 12, "4");
            db.insertTrattRadio1(getString(R.string.ou28), 10, 16, 14, 16, "4");
            db.insertTrattRadio1(getString(R.string.ou29), 10, 16, 8, 12, "4");

            /*
                        String trattamento,
                        int tempo, - solitamente impostato a 10
                        int preset_on, - durata impulso
                        int preset_off, - durata pausa
                        int freq_mhz, - se 0 allora è multiscelta, se 1 è 1MHz, se 2 0.5MHz
                        String tipologia, - res/cap Se si sa a priori -> "res"/"cap" altrimenti "x"
                        String polarita, - mono/bipo Se si sa a priori -> "mono"/"bipo" altrimenti "x"
                        String manipolo,- Se si sa a priori -> "<nome manipolo>" altrimenti "x"
                        String piastra, "na" se non c'è, "rfe" se c'è, altrimenti "x"
                        String pedaliera, "na" se non c'è, "p2" altrimenti
                        int durata_feedback,
                        int potenza_feedback
            */

            //med. estetica
/*
            db.insertTrattRadio1(getString(R.string.rf1), 10, 1000, 500, 1, "cap", "mono", "rfpm", "rfe", "p2", 5, 0);
            db.insertTrattRadio1(getString(R.string.rf2), 10, 1000, 1000, 1, "cap", "mono", "rfpm", "rfe", "p2", 5, 0);
            db.insertTrattRadio1(getString(R.string.rf3), 10, 1500, 500, 1, "res", "bipo", "rfpb", "na", "p2", 5, 0);
            db.insertTrattRadio1(getString(R.string.rf4), 10, 2000, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf5), 10, 3000, 250, 1, "res", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf6), 10, 2000, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf7), 10, 2000, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf8), 10, 1500, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf9), 10, 1000, 500, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf10), 10, 1500, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf11), 10, 1500, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf12), 10, 2000, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf13), 10, 3000, 250, 1, "cap", "mono", "rfg", "rfe", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf14), 10, 1000, 500, 1, "res", "bipo", "rfg", "na", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf15), 10, 1500, 500, 1, "res", "bipo", "rfg", "na", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf16), 10, 1000, 250, 1, "res", "bipo", "rfg", "na", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf17), 10, 1000, 500, 1, "res", "bipo", "rfg", "na", "p2", 10, 0);
            //fisioterapia
            db.insertTrattRadio1(getString(R.string.rf18), 10, 1500, 500, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf19), 10, 1500, 500, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf20), 10, 1500, 500, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf21), 10, 1500, 500, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf22), 10, 1500, 500, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf23), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf24), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf25), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf26), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf27), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf28), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf28), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf30), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf31), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf32), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf33), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf34), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf35), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf36), 10, 2000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf37), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf38), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf39), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
            db.insertTrattRadio1(getString(R.string.rf40), 10, 1000, 1000, 0, "x", "x", "x", "x", "p2", 10, 0);
*/

            /*
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout,(ViewGroup) findViewById(R.id.toast_layout_root));

            ImageView image = (ImageView) layout.findViewById(R.id.image);
            image.setImageResource(R.drawable.warning);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(R.string.db_emp);

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
            */
        }

/*
        if(db.fetchTratRadio2().getCount()==0){//inserimento dati RADIOFREQUENZA 2

            *//*
                                  String trattamento,
                                  int tempo,
                                  int freq_mhz,
                                  int continua, // 0/1 no/sì da intendere come spunta
                                  int n_impulsi,
                                  int d_impulsi,
                                  String tipologia,
                                  String polarita,
                                  String manipolo,
                                  String piastra,
                                  String pedaliera
            *//*

            db.insertTrattRadio2(getString(R.string.rf2_1), 10, 1, 0, 0, 0, "res", "x", "rff", "x", "p2");
            db.insertTrattRadio2(getString(R.string.rf2_2), 10, 1, 0, 0, 0, "res", "mono", "rff", "rfe", "p2");
        }


        if(db.fetchTratRadio3().getCount()==0){//inserimento dati RADIOFREQUENZA 3

            *//*
                                  String trattamento,
                                  int tempo,
                                  int freq_mhz,
                                  String tipologia,
                                  String polarita,
                                  String manipolo,
                                  String piastra,
                                  String pedaliera
            *//*

        }*/


        if (db.fetchTratElettro().getCount() == 0) {//inserimento dati ELETTROPORAZIONE !

            /*
                                          static final String ELETTROPORAZIONE = "trattamento_el4";
        static final String ID = "_id";
        static final String TRATTAMENTO = "nome";
        static final String TEMPO_MIN = "tempo";
        static final String A = "a";
        static final String B = "b";
        static final String C = "c";
        static final String C1 = "c1";
        static final String D = "d";
        static final String FORMA_ONDA = "forma_onda";   //1 se quadrata 2 se sinusoidale
        static final String MANIPOLO = "manipolo";
        static final String PIASTRA = "piastra";
        static final String PEDALIERA = "pedaliera";

            */
            db.insertTrattElettropora(getString(R.string.ele1), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele2), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele3), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele4), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele5), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele6), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele7), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele8), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele9), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele10), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele11), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele12), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele13), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele14), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele15), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele16), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele17), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele18), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele19), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele20), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele21), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele22), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele23), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele24), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele25), 10, 1500, 10, 150, 77, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele26), 10, 500, 10, 150, 77, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele27), 10, 2000, 10, 150, 77, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele28), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele29), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele30), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele31), 10, 1500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele32), 10, 500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele33), 10, 2000, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele34), 10, 1500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele35), 10, 500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele36), 10, 2000, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele37), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele38), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele39), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele40), 10, 1500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele41), 10, 500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele42), 10, 2000, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele43), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele44), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele45), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele46), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele47), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele48), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele49), 10, 1500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele50), 10, 500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele51), 10, 2000, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele52), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele53), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele54), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele55), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele56), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele57), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele58), 10, 1500, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele59), 10, 500, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele60), 10, 2000, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele61), 10, 1500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele62), 10, 500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele63), 10, 2000, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele64), 10, 1500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele65), 10, 500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele66), 10, 2000, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele67), 10, 1500, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele68), 10, 500, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele69), 10, 2000, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele70), 10, 1500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele71), 10, 500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele72), 10, 2000, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele73), 10, 1500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele74), 10, 500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele75), 10, 2000, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele76), 10, 1500, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele77), 10, 500, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele78), 10, 2000, 10, 100, 118, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele79), 10, 1500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele80), 10, 500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele81), 10, 2000, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele82), 10, 1500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele83), 10, 500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele84), 10, 2000, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele85), 10, 1500, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele86), 10, 500, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele87), 10, 2000, 10, 100, 118, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele88), 10, 1500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele89), 10, 500, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele90), 10, 2000, 10, 100, 118, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele91), 10, 1500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele92), 10, 500, 10, 100, 118, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele93), 10, 2000, 10, 100, 118, 333, 1, "epm", "epe", "p2");


            //fisioterapia
            db.insertTrattElettropora(getString(R.string.ele94), 10, 1500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele95), 10, 500, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele96), 10, 2000, 10, 150, 77, 1000, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele97), 10, 1500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele98), 10, 500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele99), 10, 2000, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele100), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele101), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele102), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele103), 10, 1500, 10, 150, 77, 250, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele104), 10, 500, 10, 150, 77, 250, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele105), 10, 2000, 10, 150, 77, 250, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele106), 10, 1500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele107), 10, 500, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele108), 10, 2000, 10, 150, 77, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele109), 10, 1500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele110), 10, 500, 10, 150, 77, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele111), 10, 2000, 10, 150, 77, 333, 1, "epm", "epe", "p2");


            //andrologia
            db.insertTrattElettropora(getString(R.string.ele112), 10, 1500, 10, 300, 38, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele113), 10, 500, 10, 300, 38, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele114), 10, 2000, 10, 300, 38, 200, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele115), 10, 1500, 10, 300, 38, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele116), 10, 500, 10, 300, 38, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele117), 10, 2000, 10, 300, 38, 333, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele118), 10, 1500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele119), 10, 500, 10, 300, 38, 500, 1, "epm", "epe", "p2");
            db.insertTrattElettropora(getString(R.string.ele120), 10, 2000, 10, 300, 38, 500, 1, "epm", "epe", "p2");

        }

        Cursor c1 = db.fetchTratRadio1(); // query
        startManagingCursor(c1);
/*        Cursor c2=db.fetchTratRadio2(); // query
        startManagingCursor(c2);
        Cursor c3=db.fetchTratRadio3(); // query
        startManagingCursor(c3);*/
        Cursor c4 = db.fetchTratElettro(); // query
        startManagingCursor(c4);

        db.close();

    }

    /****************************************************************************************************************************/
    /*  CREAZIONE DELLE SHARED RISERVATE    */

    /***************************************************************************************************************************/

    public void SelezionaMACCHINA(View view) {
        Selezionamacchina1();
    }

    public void Selezionamacchina1() {
        final Button set_macchina = (Button) findViewById(R.id.tipologia_macchina);
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        nome_dispositivo = reserved.getString("TIPOLOGIA_DISPOSITIVO", getString(R.string.tip_macchina1));

        final String tip_disp;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("SELEZIONA LA MACCHINA");
        Resources res = getResources();
        final String[] options = {res.getString(R.string.tip_macchina1),
                res.getString(R.string.tip_macchina2),
        };
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String option = options[which];
                SharedPreferences.Editor reservedEditor = reserved.edit();
                Resources res = getResources();
                switch (which) {
                    case 0:
                        log.Log("CAMBIO LICENZA - " + res.getString(R.string.tip_macchina1), PannelloAssistenza.this);
                        set_macchina.setText(R.string.tip_macchina1);
                        reservedEditor.putString("TIPOLOGIA_temp", getResources().getString(R.string.tip_macchina1));
                        reservedEditor.commit();
                        impostaMacchina();
                        break;
                    case 1:
                        log.Log("CAMBIO LICENZA - " + res.getString(R.string.tip_macchina2), PannelloAssistenza.this);
                        log.Log("EXIT - launcher", PannelloAssistenza.this);
                        set_macchina.setText(R.string.tip_macchina2);
                        reservedEditor.putString("TIPOLOGIA_temp", getResources().getString(R.string.tip_macchina2));
                        reservedEditor.commit();
                        impostaMacchina();
                        break;
                    case 2:
                        log.Log("CAMBIO LICENZA - " + res.getString(R.string.tip_macchina3), PannelloAssistenza.this);
                        set_macchina.setText(R.string.tip_macchina3);
                        reservedEditor.putString("TIPOLOGIA_temp", getResources().getString(R.string.tip_macchina3));
                        reservedEditor.commit();
                        impostaMacchina();
                        break;

                }
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.annulla,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void impostaMacchina() {
        /*allert in cui chiede conferma per il riavvio
        - alla conferma salva "TIPOLOGIA_DISPOSITIVO"
        - una volta salvato riavvia il tablet con System.exec reboot;
         */
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.config_macchina);
        dialog.setTitle("STAI PER CAMBIARE LA CONFIGURAZIONE DELLA MACCHINA. PROCEDERE?");
        dialog.setCancelable(false);
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button continueButton2 = (Button) dialog.findViewById(R.id.button1);
        continueButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences reserved = getSharedPreferences("Reserved", 0);
                SharedPreferences.Editor reservedEditor = reserved.edit();
                String tmp = reserved.getString("TIPOLOGIA_temp", null);
                reservedEditor.putString("TIPOLOGIA_DISPOSITIVO", tmp);
                reservedEditor.commit();
                WriteSettings(PannelloAssistenza.this, tmp);
                //rebootSU();
            }
        });
        dialog.show();

    }

    public static void rebootSU() {
        Runtime runtime = Runtime.getRuntime();
        Process proc = null;
        OutputStreamWriter osw = null;
        String command = "/system/xbin/reboot";
        try { // Run Script
            proc = runtime.exec("su");
            osw = new OutputStreamWriter(proc.getOutputStream());
            osw.write(command);
            osw.flush();
            osw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (osw != null) {
                try {
                    osw.close();
                } catch (IOException e) {
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }
        try {
            if (proc != null)
                proc.waitFor();
        } catch (InterruptedException e) {
            Log.d("VELVETERROR", e.getMessage());
        }
        if (proc.exitValue() != 0) {
        }
    }

    public void home(View view) {
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}



