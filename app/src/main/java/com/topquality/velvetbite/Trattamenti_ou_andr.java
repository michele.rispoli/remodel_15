package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Trattamenti_ou_andr  extends Activity {
    int db;
    VelvetSkinIntraPlusDB_rf1 mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_ou_fis);
        mDb = new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        String SharedPrefName = "Reserved";
//        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = reserved.getInt("DATABASE", 0);

        Button trat1 = findViewById(R.id.trat1);      //arti sup
        Button trat2 = findViewById(R.id.trat2);      //arti inf
        Button trat3 = findViewById(R.id.trat3);      //addome
        Button trat4 = findViewById(R.id.trat4);      //glutei
        trat4.setVisibility(View.GONE);
        Button trat5 = findViewById(R.id.trat5);      //glutei
        trat5.setVisibility(View.GONE);
        trat1.setText(R.string.andro1);
        trat2.setText(R.string.andro2);
        trat3.setText(R.string.andro3);


        trat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //arti sup
                fai_query("27");
            }
        });


        trat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //arti inf
                fai_query("28");
            }
        });

        trat3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //addome
                fai_query("29");

            }
        });
    }

    public void fai_query(String id){
        System.out.println(id);
        mDb.open();
        Cursor c=mDb.fetchTratRadio1_id(id);
        startManagingCursor(c);

/*        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TRATTAMENTO, trattamento_rf1);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TEMPO_MIN, tempo);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ1, freq1);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ2, freq2);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ3, freq3);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TIPOLOGIA, vacuum1);
        mDb.insert(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.ONDEDURTO, null, cv);*/

        int nome_trattamento=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TRATTAMENTO);  //indici delle colonne
        int tempo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TEMPO_MIN);
        int freq1=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ1);
        int freq2=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ2);
        int freq3=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ3);
        int tipologia=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TIPOLOGIA);


        if(c.moveToFirst()){  //se va alla prima entry, il cursore non è vuoto
            do {

                System.out.println("Nome trattamento:"+c.getString(nome_trattamento)+"\n tempo:"+c.getInt(tempo)+"\n a:"+c.getInt(freq1)
                        +"\n b:"+c.getInt(freq2)+"\n c1:"+c.getInt(freq3)+"\n c2"+c.getString(tipologia)); //estrazione dei dati dalla entry del cursor

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("FREQ1", c.getInt(freq1));
                prefsEditor.putInt("FREQ2", c.getInt(freq2));
                prefsEditor.putInt("FREQ3", c.getInt(freq3));
                prefsEditor.putString("TIPOLOGIA", c.getString(tipologia));
                prefsEditor.commit();

            } while (c.moveToNext());//iteriamo al prossimo elemento
        }
        mDb.close();
        stopManagingCursor(c);
        c.close();

        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Immissione_diretta_ou.class);
        startActivityForResult(CauseNelleVic, 0);


    }

    public void back (View view) {
        finish();
    }
    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

    public void esci (View view){
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
        finish();
    }
}

