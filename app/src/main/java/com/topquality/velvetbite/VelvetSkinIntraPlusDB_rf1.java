package com.topquality.velvetbite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VelvetSkinIntraPlusDB_rf1 {
    SQLiteDatabase mDb;
    DbHelper mDbHelper;
    Context mContext;
    private static final String DB_NAME="velvet_skin_intra_plus_rf1";//nome del db
    private static final int DB_VERSION=2; //numero di versione del db

    public VelvetSkinIntraPlusDB_rf1(Context ctx){
        mContext=ctx;
        mDbHelper=new DbHelper(ctx, DB_NAME, null, DB_VERSION);
    }

    public void open(){  //il database su cui agiamo è leggibile/scrivibile
        mDb=mDbHelper.getWritableDatabase();

    }

    public void close(){ //chiudiamo il database su cui agiamo
        mDb.close();
    }

    /*********************************************/
    /* tabella di riferimento: RADIOFREQUENZA 1 */
    /******************************************/
    public void insertTrattRadio1(String trattamento_rf1,
                                  int tempo,
                                  int freq1,
                                  int freq2,
                                  int freq3,
                                  String vacuum1){ //metodo per inserire i dati
        ContentValues cv=new ContentValues();
        cv.put(MetaDatiTrattamentoRadio1.TRATTAMENTO, trattamento_rf1);
        cv.put(MetaDatiTrattamentoRadio1.TEMPO_MIN, tempo);
        cv.put(MetaDatiTrattamentoRadio1.FREQ1, freq1);
        cv.put(MetaDatiTrattamentoRadio1.FREQ2, freq2);
        cv.put(MetaDatiTrattamentoRadio1.FREQ3, freq3);
        cv.put(MetaDatiTrattamentoRadio1.TIPOLOGIA, vacuum1);
        mDb.insert(MetaDatiTrattamentoRadio1.ONDEDURTO, null, cv);
    }

    public Cursor fetchTratRadio1(){ //metodo per fare la query di tutti i dati
        return mDb.query(MetaDatiTrattamentoRadio1.ONDEDURTO, null,null,null,null,null,null);
    }
    public Cursor fetchTratRadio1_id(String id){
        return mDb.rawQuery("select * from "+VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.ONDEDURTO+" where _id = ?", new String[]{id});
    }

    static class MetaDatiTrattamentoRadio1 {  // i metadati della tabella, accessibili ovunque
        static final String ONDEDURTO = "trattamenti_rf1";
        static final String ID = "_id";
        static final String TRATTAMENTO = "nome";
        static final String TEMPO_MIN = "tempo";
        static final String FREQ1 = "freq1";
        static final String FREQ2 = "freq2";
        static final String FREQ3 = "freq3";
        static final String TIPOLOGIA = "vacuum1";
    }

    private static final String CREA_TABELLA_ONDEDURTO = "CREATE TABLE IF NOT EXISTS "
            + MetaDatiTrattamentoRadio1.ONDEDURTO + " ("
            + MetaDatiTrattamentoRadio1.ID+ " integer primary key autoincrement, "
            + MetaDatiTrattamentoRadio1.TRATTAMENTO + " text not null, "
            + MetaDatiTrattamentoRadio1.TEMPO_MIN + " integer, "
            + MetaDatiTrattamentoRadio1.FREQ1 + " integer, "
            + MetaDatiTrattamentoRadio1.FREQ2 + " integer, "
            + MetaDatiTrattamentoRadio1.FREQ3 + " integer, "
            + MetaDatiTrattamentoRadio1.TIPOLOGIA + " text);";

    private static final String ELIMINA_TABELLA_RADIOFREQUENZA1 = "DELETE TABLE IF EXIST"
            + MetaDatiTrattamentoRadio1.ONDEDURTO + " ;";


    /*********************************************/
    /* tabella di riferimento: ELETTROPORAZIONE */
    /******************************************/
    public void insertTrattElettropora(String trattamento_el4,
                                       int tempo,
                                       int a,
                                       int b,
                                       int c,
                                       int c1,
                                       int d,
                                       int forma_onda,
                                       String manipolo,
                                       String piastra,
                                       String pedaliera){ //metodo per inserire i dati
        ContentValues cv=new ContentValues();
        cv.put(MetaDatiTrattamentoElettropora.TRATTAMENTO, trattamento_el4);
        cv.put(MetaDatiTrattamentoElettropora.TEMPO_MIN, tempo);
        cv.put(MetaDatiTrattamentoElettropora.A, a);
        cv.put(MetaDatiTrattamentoElettropora.B, b);
        cv.put(MetaDatiTrattamentoElettropora.C, c);
        cv.put(MetaDatiTrattamentoElettropora.C1, c1);
        cv.put(MetaDatiTrattamentoElettropora.D, d);
        cv.put(MetaDatiTrattamentoElettropora.FORMA_ONDA, forma_onda);
        cv.put(MetaDatiTrattamentoElettropora.MANIPOLO, manipolo);
        cv.put(MetaDatiTrattamentoElettropora.PIASTRA, piastra);
        cv.put(MetaDatiTrattamentoElettropora.PEDALIERA, pedaliera);
        mDb.insert(MetaDatiTrattamentoElettropora.ELETTROPORAZIONE, null, cv);
    }

    public Cursor fetchTratElettro(){ //metodo per fare la query di tutti i dati
        return mDb.query(MetaDatiTrattamentoElettropora.ELETTROPORAZIONE, null,null,null,null,null,null);
    }

    static class MetaDatiTrattamentoElettropora {  // i metadati della tabella, accessibili ovunque
        static final String ELETTROPORAZIONE = "trattamento_el4";
        static final String ID = "_id";
        static final String TRATTAMENTO = "nome";
        static final String TEMPO_MIN = "tempo";
        static final String A = "a";
        static final String B = "b";
        static final String C = "c";
        static final String C1 = "c1";
        static final String D = "d";
        static final String FORMA_ONDA = "forma_onda";
        static final String MANIPOLO = "manipolo";
        static final String PIASTRA = "piastra";
        static final String PEDALIERA = "pedaliera";
    }

    private static final String CREA_TABELLA_ELETTROPORAZIONE = "CREATE TABLE "
            + MetaDatiTrattamentoElettropora.ELETTROPORAZIONE + " ("
            + MetaDatiTrattamentoElettropora.ID+ " integer primary key autoincrement, "
            + MetaDatiTrattamentoElettropora.TRATTAMENTO + " text not null, "
            + MetaDatiTrattamentoElettropora.TEMPO_MIN + " integer, "
            + MetaDatiTrattamentoElettropora.A + " integer, "
            + MetaDatiTrattamentoElettropora.B + " integer, "
            + MetaDatiTrattamentoElettropora.C + " integer, "
            + MetaDatiTrattamentoElettropora.C1 + " integer, "
            + MetaDatiTrattamentoElettropora.D + " integer, "
            + MetaDatiTrattamentoElettropora.FORMA_ONDA + " text, "
            + MetaDatiTrattamentoElettropora.MANIPOLO + " text, "
            + MetaDatiTrattamentoElettropora.PIASTRA + " text, "
            + MetaDatiTrattamentoElettropora.PEDALIERA + " text);";

    public Cursor fetchTrateLETTROPORA_id(String id){
        return mDb.rawQuery("select * from "+VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoElettropora.ELETTROPORAZIONE+" where _id = ?", new String[]{id});
    }


    private class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase _db) {
            _db.execSQL(CREA_TABELLA_ONDEDURTO);
            _db.execSQL(CREA_TABELLA_ELETTROPORAZIONE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
            onCreate(_db);
        }

    }


}