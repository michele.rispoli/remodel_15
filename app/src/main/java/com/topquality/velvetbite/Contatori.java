package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

public class Contatori extends Activity {
    LogFile log = new LogFile();
    int vol_audio=0;
    static final int CONFIRM_DIALOG = 0;
    int counter = 0;
    AlertDialog.Builder builder;
    FileContatori log_conta = new FileContatori();

    Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contatori);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }

        calcola_ou();
        calcola_el();
        //calcola_vac();
        /******File contatori******/

        Long tempo_el = reserved.getLong("TEMPO UTILIZZO ELETTROPORAZIONE", 0);
        log_conta.Log("" + getString(R.string.el) + "|" + tempo_el, this);

        Long tempo_ou = reserved.getLong("TEMPO UTILIZZO ONDEDURTO", 0);
        log_conta.Log("" + getString(R.string.ou) + "|" + tempo_ou, this);

        /******File contatori******/
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        vol_audio = prefs.getInt("Volume", 1);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int leng = sp.getInt("LINGUA", 1);

    }

    public void calcola_ou(){
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        Long tempoTOT = reserved.getLong("TEMPO UTILIZZO ONDEDURTO", 0);
        int seconds_ou = (int) (tempoTOT / 1000 % 60);
        int minutes_ou = (int) (tempoTOT / 60000 % 60);
        int hours_ou = (int) (tempoTOT / 3600000 % 24);
        int days_ou = (int) (tempoTOT / 86400000);

        System.out.println(days_ou + " days, ");
        System.out.println(hours_ou + " hours, ");
        System.out.println(minutes_ou + " minutes, ");
        System.out.println(seconds_ou + " seconds");

        TextView giorni = (TextView) findViewById(R.id.giorni);
        giorni.setText(getString(R.string.giorni) + ": "+days_ou);
        TextView ore1 = (TextView)findViewById(R.id.ore);
        ore1.setText(getString(R.string.ora) +": "+hours_ou);
        TextView minuti1 = (TextView)findViewById(R.id.minuti);
        minuti1.setText(getString(R.string.minuti) +": "+minutes_ou);
        TextView secondi1 = (TextView)findViewById(R.id.secondi);
        secondi1.setText(getString(R.string.secondi) +": "+seconds_ou);
    }


    public void calcola_el(){
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        Long tempoTOT = reserved.getLong("TEMPO UTILIZZO ELETTROPORAZIONE", 0);
        int seconds_el = (int) (tempoTOT / 1000 % 60);
        int minutes_el = (int) (tempoTOT / 60000 % 60);
        int hours_el = (int) (tempoTOT / 3600000 % 24);
        int days_el = (int) (tempoTOT / 86400000);

        System.out.println(days_el + " days, ");
        System.out.println(hours_el + " hours, ");
        System.out.println(minutes_el + " minutes, ");
        System.out.println(seconds_el + " seconds");

        TextView giorni1 = (TextView)findViewById(R.id.giorni1);
        giorni1.setText(getString(R.string.giorni) +": "+ days_el);
        TextView ore1 = (TextView)findViewById(R.id.ore1);
        ore1.setText(getString(R.string.ora) +": "+hours_el);
        TextView minuti1 = (TextView)findViewById(R.id.minuti1);
        minuti1.setText(getString(R.string.minuti) +": "+minutes_el);
        TextView secondi1 = (TextView)findViewById(R.id.secondi1);
        secondi1.setText(getString(R.string.secondi) +": "+seconds_el);
    }


    public void calcola_vac(){
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        Long tempoTOT = reserved.getLong("TEMPO UTILIZZO VAC", 0);
        int seconds_v = (int) (tempoTOT / 1000 % 60);
        int minutes_v = (int) (tempoTOT / 60000 % 60);
        int hours_v = (int) (tempoTOT / 3600000 % 24);
        int days_v = (int) (tempoTOT / 86400000);

        TextView giorni = (TextView) findViewById(R.id.giorni2);
        giorni.setText(getString(R.string.giorni) + ": "+days_v);
        TextView ore1 = (TextView)findViewById(R.id.ore2);
        ore1.setText(getString(R.string.ora) +": "+hours_v);
        TextView minuti1 = (TextView)findViewById(R.id.minuti2);
        minuti1.setText(getString(R.string.minuti) +": "+minutes_v);
        TextView secondi1 = (TextView)findViewById(R.id.secondi2);
        secondi1.setText(getString(R.string.secondi) +": "+seconds_v);

        log.Log("CONTATORI VACUUM: - GIORNI" + days_v + " ORE: " + hours_v + " MINUTI: " + minutes_v + " SECONDI: " + seconds_v, Contatori.this);
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }
}
