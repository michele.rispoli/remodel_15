package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import android_serialport_api.SerialPort;

public class Scrub_op extends Activity {
    /*******************************************SERIALE **********************************/

    private static final String TAG = "MainActivity2";
    String msg = null;
    public int k = 0;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Scrub_op.ReadThread mReadThread;
    private Scrub_op.SendThread mSendThread;
    private Scrub_op.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();


    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private static final String Sserialport = "COM0"; //nome porta  - non uso più il nome
    private static final int m_iSerialPort = 0;  //id_seriale
    private static final int baudrate = 57600;  //baud rate
    private static final int databits = 8;  // bit??? byte??? di dati
    private static final int stopbits = 1;  // bit??? byte??? di stop
    private static final int parity = 'n';  // parità NESSUNA (esiste anche pari o dispari)

    String impulso = "3E-FE-01-51-03-30-10-27-00-00-00-00-00-00-00-00-00-00-00-00-00-33-2A-3C"; //impulso 10000
    String pausa = "3E-FE-01-51-03-32-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-19-C9-3C"; // pausa 0
    String frequenza = "3E-FE-01-51-04-52-00-00-01-00-00-00-00-00-00-00-00-00-00-00-00-E8-34-3C"; //1Mhz
    String canale = "3E-FE-01-51-02-48-04-00-00-00-00-00-00-00-00-00-00-00-00-00-00-93-FA-3C"; //canale 4
    String attivazione_led_rs = "3E-FE-01-51-02-5E-06-00-00-00-00-00-00-00-00-00-00-00-00-00-00-FB-54-3C"; //attivazione led + radioskin
    String attiva= "3E-FE-01-51-02-5E-08-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D5-A5-3C";
    String disattiva ="3E-FE-01-51-02-5E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-90-82-3C";
    String disattivazione_led_rs = "3E-FE-01-51-02-5E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-90-82-3C";
    String flash = "3E-FE-01-51-02-4D-03-00-00-00-00-00-00-00-00-00-00-00-00-00-00-F2-AA-3C"; // modo flash
    String conf1= "3E-FE-01-51-02-45-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-AE-F6-3C"; //configurazione 1
    String conf2="3E-FE-01-51-02-62-04-00-00-00-00-00-00-00-00-00-00-00-00-00-00-6B-ED-3C"; //configurazione 2
    String pulsanti = "3E-FE-01-51-02-61-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-68-30-3C"; //configurazione pulsanti
    String start = "3E-FE-01-53-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-0B-2D-3C"; //start
    String stop_mio = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";
    String pot10="3E-FE-01-51-02-68-0A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-A9-4D-3C"; //potenza 10%
    //3E-FE-01-51-02-68-0A-00-00-00-00-00-00-00-00-00-00-00-00-00-00-A9-4D-3C

    String hex;
    /*************************************************************SERIALE ****************************************/
    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    int polarita = 0;

    int tempo_sel = 0;

    private TextView textView;
    public int potenza = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;

    static final int CONFIRM_DIALOG = 0;

    TextView text1;


    int res;

    private static final String FORMAT = "%02d:%02d";


    Scrub_op.MyCount counter;
    Long s1;

    String stop_scrub = "3E-FE-01-51-02-5E-02-00-00-00-00-00-00-00-00-00-00-00-00-00-00-49-CF-3C";
    String start_scrub = "3E-FE-01-51-02-5E-FD-00-00-00-00-00-00-00-00-00-00-00-00-00-00-1E-3D-3C";

    long startTime;
    long endTime;
    long tempoTrattamento;
    long tempocontatori;
    long update_contatori;
    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Scrub_op.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrub_op);
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondo2);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.video_scrub);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });
        /*contatori utilizzo macchina*/
        final SharedPreferences res1 = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor resEditor = res1.edit();
        startTime = System.currentTimeMillis();
        resEditor.putLong("Start time", startTime);
        /*contatori utilizzo macchina*/
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SendMsg(start_scrub);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        String nome_trattamento_db = prefs.getString("Nome Trattamento", null);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Potenza", 0);
        prefsEditor.commit();

        int leng = prefs.getInt("LINGUA", 1);

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Scrub_op.this, R.raw.fine_trattamento, 0);
        spoolId = spool.load(Scrub_op.this, R.raw.fine_trattamento, 0);

        initializeVariables();
        volume();


        /* inizializzazione dei paramentri */
        tempo_sel = prefs.getInt("Tempo", 0);

        vol_audio = prefs.getInt("Volume", 1);

        prefsEditor.commit();
        updateBarHandler = new Handler();

        text1=(TextView)findViewById(R.id.count_down);
        final int tempo_count = tempo_sel * 60000;
        counter= new Scrub_op.MyCount(tempo_count, 1000);
        counter.start();


        /*BOTTONE STOP */
        Button stop = (Button) findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pacchetto_da_inviare("68", "64-00", 0);
                stop();
            }
        });

    }


    /*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot = null;

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 2;

        int lung_mess1 = messaggio.length();
        if (lung_mess1 == 5) {
            msg_tot = messaggio;
            System.out.println("CHE HAI?????????????????????????1 " + msg_tot);
        }else if (lung_mess1 == 4){
            msg_tot = "0"+messaggio;
            System.out.println("CHE HAI??????????????????????????2 " + msg_tot);
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 14;
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;

        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }

    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2A-1E-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " +mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size>0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println("LEGGO da thread - YEAH: " + to_check + " size" + size);
                        //hard_ck(to_check);
                    } catch (IOException e) {

                    }
                }
            }

        }




    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {
        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            try {
                Thread.sleep(30);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }
    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /***********************
     * SERIALE
     **********************/

    public class MyCount extends CountDownTimer
    {
        public MyCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
        }
        @Override     public void onFinish()
        {
            text1.setText("00:00");
            stop1();
        }
        @Override     public void onTick(long millisUntilFinished)
        {
            s1=millisUntilFinished;
            text1.setText(String.format(FORMAT,
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

        }
    }

    /* cio che avviene se clicco stop */
    private void stop() {
        counter.cancel();
        SendMsg(stop_scrub);
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.pausa);
        dialog.setCancelable(false);
        TextView txt2 = (TextView) dialog.findViewById(R.id.title);
        txt2.setText(R.string.pausa);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.pausa_tratt1);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter= new Scrub_op.MyCount(s1,1000);
                potenza = 0;
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", potenza);
                prefsEditor.commit();
                res = 100;
                hex = Integer.toHexString(res).toUpperCase();
                SendMsg(start_scrub);
                counter.start();
                dialog.dismiss();
            }
        });
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SendMsg(stop_scrub);
                contatori();
                counter.cancel();
                closeSerialPort();
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        });
        dialog.show();
    }
    /* cio che avviene allo scadere del tempo */
    public void stop1(){
        SendMsg(stop_scrub);
        contatori();
        bip();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.stop);
        dialog.setTitle(R.string.fine_tratt);
        dialog.setCancelable(false);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.fine_tratt1);
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setText(R.string.fine_tratt3);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        });
        dialog.show();
    }

    public void bip(){

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(this, R.raw.fine_trattamento, 0);
        spoolId = spool.load(this, R.raw.fine_trattamento, 0);
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float f = actualVolume / maxVolume;
        System.out.println("OOOOKKKKK " + f);
        spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                spool.play(spoolId, f, f, 1, 0, 1);
            }
        });
        spool.play(spoolId, f, f, 1, 0, 1);
    }

    public void contatori(){
        endTime = System.currentTimeMillis();
        long millisDiff = endTime - startTime;
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();

        prefsEditor.putLong("Tempo parziale scrub", millisDiff);
        prefsEditor.commit();
        int seconds = (int) (millisDiff / 1000 % 60);
        int minutes = (int) (millisDiff / 60000 % 60);
        int hours = (int) (millisDiff / 3600000 % 24);
        int days = (int) (millisDiff / 86400000);
        System.out.println("inizio " + startTime + " fine " + endTime + " ore " + hours + " minuti " + minutes + " seconds " + seconds);

        Long tempoOLD = reserved.getLong("TEMPO UTILIZZO SCRUB", 0);
        Long tempoTOT = tempoOLD + millisDiff;
        prefsEditor.putLong("TEMPO UTILIZZO SCRUB", tempoTOT);
        prefsEditor.commit();
        int seconds1 = (int) (tempoTrattamento % 60);
        int minutes1 = (int) (tempoTrattamento / 60);
        String tolog1 = minutes1 + " M " + seconds1 + "S";
        System.out.println(days + " days, ");
        System.out.println(hours + " hours, ");
        System.out.println(minutes + " minutes, ");
        System.out.println(seconds + " seconds");
    }



    /* GESTIONE VOLUME */
    public void volume() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        final AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

    }

    private void initializeVariables() {
        textView = (TextView) findViewById(R.id.textView1);
    }


    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
            case CONFIRM_DIALOG:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.pausa);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.continuare, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        counter= new Scrub_op.MyCount(s1,1000);
                        potenza = 0;
                        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        prefsEditor.putInt("Potenza", potenza);
                        prefsEditor.commit();
                        pacchetto_da_inviare("68", "00-00", 0);
                        textView.setText("0 %");
                        counter.start();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(R.string.esci, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SendMsg(stop_mio);
                        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
                        startActivity(i);
                        finish();
                    }
                });
                dialog = builder.create();
                break;
            default:
                dialog = null;
        }
        return dialog;
    }

    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }

    public void home(View view){
        finish();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
        startActivityForResult(CauseNelleVic, 0);
    }

}