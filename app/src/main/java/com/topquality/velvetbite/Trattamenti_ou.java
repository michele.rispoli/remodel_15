




package com.topquality.velvetbite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class Trattamenti_ou extends Activity {
    int db;
    int tratt = 1;
    int tratt1 = 1;
    VelvetSkinIntraPlusDB_rf1 mDb;
    View b;
    View c;
    View d;
    View e;
    HorizontalScrollView scroll;
    int cursore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trattamenti_ou);
        mDb=new VelvetSkinIntraPlusDB_rf1(getApplicationContext());
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }

        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        db = reserved.getInt("DATABASE", 0);

        View a = findViewById(R.id.la);
        b = findViewById(R.id.lb);
        c = findViewById(R.id.lc);
        d = findViewById(R.id.ld);
        e = findViewById(R.id.le);
        Button trat1 = findViewById(R.id.trat1);      //arti sup
        Button trat2 = findViewById(R.id.trat2);      //arti inf
        Button trat3 = findViewById(R.id.trat3);      //addome
        Button trat4 = findViewById(R.id.trat4);      //glutei
        trat1.setBackgroundResource(R.drawable.btn_1p);
        trat1.setTextColor(Color.BLACK);
        prefsEditor.putInt("tratt_rf1", 1).commit();
        Button trat_a1 = findViewById(R.id.trattrat_a1);
        Button trat_a2 = findViewById(R.id.trat_a2);
        Button trat_a3 = findViewById(R.id.trat_a3);
        Button trat_a4 = findViewById(R.id.trat_a4);
        Button trat_a5 = findViewById(R.id.trat_a5);
        Button trat_a6 = findViewById(R.id.trat_a6);

        trat_a1.setText(R.string.lipdis12);
        trat_a2.setText(R.string.lipdis34);
        trat_a3.setText(R.string.adip);
        trat_a4.setVisibility(View.GONE);
        trat_a5.setVisibility(View.GONE);
        trat_a6.setVisibility(View.GONE);

        Button trat_b1 = findViewById(R.id.trattrat_b1);
        Button trat_b2 = findViewById(R.id.trat_b2);
        Button trat_b3 = findViewById(R.id.trat_b3);
        Button trat_b4 = findViewById(R.id.trat_b4);
        Button trat_b5 = findViewById(R.id.trat_b5);
        Button trat_b6 = findViewById(R.id.trat_b6);


        Button trat_c1 = findViewById(R.id.trattrat_c1);
        Button trat_c2 = findViewById(R.id.trat_c2);
        Button trat_c3 = findViewById(R.id.trat_c3);
        Button trat_c4 = findViewById(R.id.trat_c4);
        b.setVisibility(View.GONE);
        c.setVisibility(View.GONE);
        d.setVisibility(View.GONE);
        e.setVisibility(View.GONE);

        scroll = findViewById(R.id.scroll);

        trat1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //arti sup
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 1).commit();
                trat_a5.setVisibility(View.GONE);
                trat1.setBackgroundResource(R.drawable.btn_1p);
                trat1.setTextColor(Color.BLACK);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
                trat4.setBackgroundResource(R.drawable.btn_1);
                trat4.setTextColor(Color.WHITE);
                trat_a1.setText(R.string.lipdis12);
                trat_a2.setText(R.string.lipdis34);
                trat_a3.setText(R.string.adip);
                trat_a4.setVisibility(View.GONE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                b.setVisibility(View.GONE);
            }
        });


        trat2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //arti inf
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 2).commit();
                trat_a3.setVisibility(View.VISIBLE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                trat_a1.setText(R.string.coscia);
                trat_a2.setText(R.string.gamba);
                trat_a3.setText(R.string.ginocc);
                trat_a4.setVisibility(View.GONE);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1p);
                trat2.setTextColor(Color.BLACK);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
                trat4.setBackgroundResource(R.drawable.btn_1);
                trat4.setTextColor(Color.WHITE);
            }
        });

        trat3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //addome
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 3).commit();
                trat_a3.setVisibility(View.VISIBLE);
                trat_a4.setVisibility(View.GONE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                trat_a1.setText(R.string.lipdis12);
                trat_a2.setText(R.string.lipdis34);
                trat_a3.setText(R.string.adip);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1p);
                trat3.setTextColor(Color.BLACK);
                trat4.setBackgroundResource(R.drawable.btn_1);
                trat4.setTextColor(Color.WHITE);

            }
        });

        trat4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //glutei
                b.setVisibility(View.GONE);
                c.setVisibility(View.GONE);
                prefsEditor.putInt("tratt_rf1", 4).commit();
                trat_a3.setVisibility(View.VISIBLE);
                trat_a4.setVisibility(View.GONE);
                trat_a5.setVisibility(View.GONE);
                trat_a6.setVisibility(View.GONE);
                trat_a1.setText(R.string.lipdis12);
                trat_a2.setText(R.string.lipdis34);
                trat_a3.setText(R.string.adip);
                trat1.setBackgroundResource(R.drawable.btn_1);
                trat1.setTextColor(Color.WHITE);
                trat2.setBackgroundResource(R.drawable.btn_1);
                trat2.setTextColor(Color.WHITE);
                trat3.setBackgroundResource(R.drawable.btn_1);
                trat3.setTextColor(Color.WHITE);
                trat4.setBackgroundResource(R.drawable.btn_1p);
                trat4.setTextColor(Color.BLACK);

            }
        });

        trat_a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 1).commit();
                tratt = prefs.getInt("tratt_rf1", 0);

                prefsEditor.putInt("curs", 1).commit();
                if (tratt == 1){ //viso - rughe
                    fai_query("1");
                }else if (tratt == 2){ //collo - rughe
                    prefsEditor.putInt("coscia", 1).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1p);
                    trat_a1.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.interno);
                    trat_b2.setText(R.string.esterno);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.GONE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ //corpo - arti sup
                    fai_query("16");
                }else if (tratt == 4){ //corpo - arti sup
                    fai_query("19");
                }

            }
        });

        trat_a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("tratt_rf2", 2).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                prefsEditor.putInt("curs", 2).commit();
                if (tratt == 1){ //viso- ipotonia muscolare
                    fai_query("2");
                }else if (tratt == 2){ //collo - az bios
                    prefsEditor.putInt("collo", 2).commit();
                    b.setVisibility(View.VISIBLE);
                    c.setVisibility(View.GONE);
                    d.setVisibility(View.GONE);
                    e.setVisibility(View.GONE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a2.setBackgroundResource(R.drawable.btn_1p);
                    trat_a2.setTextColor(Color.BLACK);
                    trat_a3.setBackgroundResource(R.drawable.btn_1);
                    trat_a3.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_a6.setVisibility(View.GONE);
                    trat_b1.setText(R.string.lipdis12);
                    trat_b2.setText(R.string.lipdis34);
                    trat_b3.setText(R.string.adip);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.GONE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ // corpo - seno
                    fai_query("17");
                }else if (tratt == 4){ // corpo - seno
                    fai_query("20");
                }
            }
        });

        trat_a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                prefsEditor.putInt("curs", 3).commit();
                prefsEditor.putInt("tratt_rf2", 3).commit();
                tratt = prefs.getInt("tratt_rf1", 0);
                if (tratt == 1){
                   fai_query("3");
                }else if (tratt == 2){
                    prefsEditor.putInt("collo", 3).commit();
                    b.setVisibility(View.VISIBLE);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b4.setVisibility(View.VISIBLE);
                    trat_b1.setText(R.string.lipdis12);
                    trat_b2.setText(R.string.lipdis34);
                    trat_b3.setVisibility(View.VISIBLE);
                    trat_b3.setText(R.string.adip);
                    trat_b4.setVisibility(View.GONE);
                    trat_a3.setBackgroundResource(R.drawable.btn_1p);
                    trat_a3.setTextColor(Color.BLACK);
                    trat_a2.setBackgroundResource(R.drawable.btn_1);
                    trat_a2.setTextColor(Color.WHITE);
                    trat_a1.setBackgroundResource(R.drawable.btn_1);
                    trat_a1.setTextColor(Color.WHITE);
                    trat_a4.setBackgroundResource(R.drawable.btn_1);
                    trat_a4.setTextColor(Color.WHITE);
                    trat_a5.setBackgroundResource(R.drawable.btn_1);
                    trat_a5.setTextColor(Color.WHITE);
                    trat_b5.setVisibility(View.GONE);
                    trat_b6.setVisibility(View.GONE);
                }else if (tratt == 3){ // corpo - addome
                    fai_query("18");
                }else if (tratt == 4){ // corpo - seno
                    fai_query("21");
                }


            }
        });


        trat_b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                }); //interno coscia
                int curs = prefs.getInt("curs", 1);
                if (curs == 1){
                    prefsEditor.putInt("coscia", 1).commit();
                    c.setVisibility(View.VISIBLE);
                    trat_c3.setVisibility(View.GONE);
                    trat_c4.setVisibility(View.GONE);
                    trat_c1.setText(R.string.lipdis12);
                    trat_c2.setText(R.string.lipdis34);
                    trat_c3.setVisibility(View.VISIBLE);
                    trat_c3.setText(R.string.adip);
                    trat_b1.setBackgroundResource(R.drawable.btn_1p);
                    trat_b1.setTextColor(Color.BLACK);
                    trat_b2.setBackgroundResource(R.drawable.btn_1);
                    trat_b2.setTextColor(Color.WHITE);
                    trat_b3.setBackgroundResource(R.drawable.btn_1);
                    trat_b3.setTextColor(Color.WHITE);
                    trat_b4.setBackgroundResource(R.drawable.btn_1);
                    trat_b4.setTextColor(Color.WHITE);
                    trat_b5.setBackgroundResource(R.drawable.btn_1);
                    trat_b5.setTextColor(Color.WHITE);
                    trat_b6.setBackgroundResource(R.drawable.btn_1);
                    trat_b6.setTextColor(Color.WHITE);
                }else if (curs == 2){
                    fai_query("10");
                }else if (curs == 3){
                    fai_query("13");
                }

       //}
            }
        });
        trat_b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                }); // esterno coscia

                int curs = prefs.getInt("curs", 1);
                if (curs == 1){
                    prefsEditor.putInt("cursore", 1).commit();
                    prefsEditor.putInt("coscia", 2).commit();
                    c.setVisibility(View.VISIBLE);
                    trat_c3.setVisibility(View.GONE);
                    trat_c4.setVisibility(View.GONE);
                    trat_c1.setText(R.string.lipdis12);
                    trat_c2.setText(R.string.lipdis34);
                    trat_c3.setVisibility(View.VISIBLE);
                    trat_c3.setText(R.string.adip);
                    trat_b1.setBackgroundResource(R.drawable.btn_1);
                    trat_b1.setTextColor(Color.WHITE);
                    trat_b2.setBackgroundResource(R.drawable.btn_1p);
                    trat_b2.setTextColor(Color.BLACK);
                    trat_b3.setBackgroundResource(R.drawable.btn_1);
                    trat_b3.setTextColor(Color.WHITE);
                    trat_b4.setBackgroundResource(R.drawable.btn_1);
                    trat_b4.setTextColor(Color.WHITE);
                    trat_b5.setBackgroundResource(R.drawable.btn_1);
                    trat_b5.setTextColor(Color.WHITE);
                    trat_b6.setBackgroundResource(R.drawable.btn_1);
                    trat_b6.setTextColor(Color.WHITE);
                }else if (curs == 2){
                    fai_query("11");
                }else if (curs == 3){
                    fai_query("14");
                }

            }
        });
        trat_b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                int curs = prefs.getInt("curs", 1);
                if (curs == 1){
                    prefsEditor.putInt("cursore", 1).commit();
                    prefsEditor.putInt("viso1", 1).commit();
                    c.setVisibility(View.VISIBLE);
                    trat_c3.setVisibility(View.GONE);
                    trat_c4.setVisibility(View.GONE);
                    trat_c1.setText(R.string.lipdis12);
                    trat_c2.setText(R.string.lipdis34);
                    trat_c3.setVisibility(View.VISIBLE);
                    trat_c3.setText(R.string.adip);
                    trat_b1.setBackgroundResource(R.drawable.btn_1p);
                    trat_b1.setTextColor(Color.BLACK);
                    trat_b2.setBackgroundResource(R.drawable.btn_1);
                    trat_b2.setTextColor(Color.WHITE);
                    trat_b3.setBackgroundResource(R.drawable.btn_1);
                    trat_b3.setTextColor(Color.WHITE);
                    trat_b4.setBackgroundResource(R.drawable.btn_1);
                    trat_b4.setTextColor(Color.WHITE);
                    trat_b5.setBackgroundResource(R.drawable.btn_1);
                    trat_b5.setTextColor(Color.WHITE);
                    trat_b6.setBackgroundResource(R.drawable.btn_1);
                    trat_b6.setTextColor(Color.WHITE);
                }else if (curs == 2){
                    fai_query("12");
                }else if (curs == 3){
                    fai_query("15");
                }

            }
        });




        trat_c1.setOnClickListener(new View.OnClickListener() { //pa allo
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                int coscia = prefs.getInt("coscia", 1);
                if (coscia == 1){
                    fai_query("4");
                }else if (coscia == 2){
                    fai_query("7");
                }

            }
        });

        trat_c2.setOnClickListener(new View.OnClickListener() { //pa omo
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                int coscia = prefs.getInt("coscia", 1);
                if (coscia == 1){
                    fai_query("5");
                }else if (coscia == 2){
                    fai_query("8");
                }
            }
        });

        trat_c3.setOnClickListener(new View.OnClickListener() {  //prp velvet
            @Override
            public void onClick(View v) {
                scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        scroll.post(new Runnable() {
                            public void run() {
                                scroll.fullScroll(View.FOCUS_RIGHT);
                            }
                        });
                    }
                });
                cursore = prefs.getInt("cursore", 0);
                int coscia = prefs.getInt("coscia", 1);
                if (coscia == 1){
                    fai_query("6");
                }else if (coscia == 2){
                    fai_query("9");
                }

            }
        });

    }

    public void chiudi_layer(){

    }


    public void fai_query(String id){
        System.out.println(id);
        mDb.open();
        Cursor c=mDb.fetchTratRadio1_id(id);
        startManagingCursor(c);

/*        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TRATTAMENTO, trattamento_rf1);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TEMPO_MIN, tempo);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ1, freq1);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ2, freq2);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ3, freq3);
        cv.put(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TIPOLOGIA, vacuum1);
        mDb.insert(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.ONDEDURTO, null, cv);*/

        int nome_trattamento=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TRATTAMENTO);  //indici delle colonne
        int tempo=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TEMPO_MIN);
        int freq1=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ1);
        int freq2=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ2);
        int freq3=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.FREQ3);
        int tipologia=c.getColumnIndex(VelvetSkinIntraPlusDB_rf1.MetaDatiTrattamentoRadio1.TIPOLOGIA);


        if(c.moveToFirst()){  //se va alla prima entry, il cursore non è vuoto
            do {

                System.out.println("Nome trattamento:"+c.getString(nome_trattamento)+"\n tempo:"+c.getInt(tempo)+"\n a:"+c.getInt(freq1)
                        +"\n b:"+c.getInt(freq2)+"\n c1:"+c.getInt(freq3)+"\n c2"+c.getString(tipologia)); //estrazione dei dati dalla entry del cursor

                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();

                prefsEditor.putString("Nome Trattamento", c.getString(nome_trattamento));
                prefsEditor.putInt("TEMPO", c.getInt(tempo));
                prefsEditor.putInt("FREQ1", c.getInt(freq1));
                prefsEditor.putInt("FREQ2", c.getInt(freq2));
                prefsEditor.putInt("FREQ3", c.getInt(freq3));
                prefsEditor.putString("TIPOLOGIA", c.getString(tipologia));
                prefsEditor.commit();

            } while (c.moveToNext());//iteriamo al prossimo elemento
        }
        mDb.close();
        stopManagingCursor(c);
        c.close();
        finish();

        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Immissione_diretta_ou.class);
        startActivityForResult(CauseNelleVic, 0);


    }

    public void back (View view) {
        finish();
    }
    public void informazioni (View view){
        Intent i = new Intent(getApplicationContext(), Informazioni.class);
        startActivity(i);
        finish();
    }

    public void esci (View view){
        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
        startActivity(i);
        finish();
    }
}
