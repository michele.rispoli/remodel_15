package com.topquality.velvetbite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import android_serialport_api.SerialPort;

public class Operativa_elettroporazione_db extends Activity {
    int volume = 0;
    int vol_audio = 0;
    int manipolo = 0;
    int polarita = 0;
    LogFile log = new LogFile();
    String tolog;
    String nome_trattamento_db;

    int tempo_sel = 0;
    int manipolo_sel = 0;
    int elettrodo_sel = 0;
    int pedaliera_sel = 0;

    int a = 0;
    int b = 0;
    int c1= 0;
    int c2=0;
    int d = 0;

    private TextView textView;
    public int potenza = 0;
    SoundPool spool;
    int spoolres;
    int spoolId;
    Handler updateBarHandler;

    static final int CONFIRM_DIALOG = 0;

    TextView text1;


    private static final String FORMAT = "%02d:%02d";

    Operativa_elettroporazione_db.MyCount counter;
    Long s1;

    private static final String TAG = "Operativa EL";
    String msg = null;

    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private Operativa_elettroporazione_db.ReadThread mReadThread;
    private Operativa_elettroporazione_db.SendThread mSendThread;
    private Operativa_elettroporazione_db.TimerSendThread mTimerSendThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();


    private boolean m_bShowDateType = false;
    private boolean m_bSendDateType = false;

    private static final String Sserialport = "COM0"; //nome porta  - non uso più il nome
    private static final int m_iSerialPort = 0;  //id_seriale
    private static final int baudrate = 57600;  //baud rate
    private static final int databits = 8;  // bit??? byte??? di dati
    private static final int stopbits = 1;  // bit??? byte??? di stop
    private static final int parity = 'n';  // parità NESSUNA (esiste anche pari o dispari)

    //stop
    String stop_elettropora = "3E-FE-01-54-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-DE-DD-3C";


    /*************************************************************SERIALE ****************************************/

    long startTime;
    long endTime;
    long tempoTrattamento;
    long tempocontatori;
    long update_contatori;


    Button venticinque;
    Button cinquanta;
    Button settantacinque;



    String hex;
    private SerialPort mSerialPort = null;

    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600;//Integer.decode(sp.getString("BAUDRATE", "19200"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Operativa_elettroporazione_db.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operativa_diretta2me_el);

        final SharedPreferences reserved = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        int reseller = reserved.getInt("RESELLER", 0);
        LinearLayout sfondo = (LinearLayout) findViewById(R.id.sfondoll);
        if (reseller == 0){
            sfondo.setBackgroundResource(R.drawable.sfondo2);
        }else if (reseller == 1){
            sfondo.setBackgroundResource(R.drawable.sfondotqg);
        }
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            Log.d("VELVETERROR", e.getMessage());
        }

        SendMsg("3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C");
        pacchetto_da_inviare("1F", "00", 0);

        /*contatori utilizzo macchina*/
        final SharedPreferences res = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor resEditor = res.edit();
        startTime = System.currentTimeMillis();
        resEditor.putLong("Start time", startTime);
        /*contatori utilizzo macchina*/


        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(Operativa_elettroporazione_db.this, R.raw.fine_trattamento, 0);
        spoolId = spool.load(Operativa_elettroporazione_db.this, R.raw.fine_trattamento, 0);

        initializeVariables();

        /* inizializzazione dei paramentri */
        SharedPreferences.Editor prefsEditor = prefs.edit();
        tempo_sel = prefs.getInt("Tempo", 0);
        manipolo_sel = 1; // manipolo, elettrodo e pedaliera sono note e priori quindi le forzo a 1 a priori
        elettrodo_sel = 1;
        pedaliera_sel = 1;
        //prendo i valori di a, b, c1, c2, d

        TextView textView4 = (TextView) findViewById(R.id.textView4);
        nome_trattamento_db = prefs.getString("Nome Trattamento", null);
        textView4.setText(nome_trattamento_db);

        a = prefs.getInt("A", 0);
        b = prefs.getInt("B", 0);
        c1 = prefs.getInt("C1", 0);
        c2 = prefs.getInt("C2",0);
        d = prefs.getInt("D", 0);


        venticinque = findViewById(R.id.venticinque);
        cinquanta  = findViewById(R.id.cinquanta);
        settantacinque = findViewById(R.id.settantacinque);


        int manipolo_el = prefs.getInt("Manipolo EL",  1);
        final Button set_manipolo = (Button) findViewById(R.id.select_manipolo);

        if (manipolo_el == 1)
        {
            set_manipolo.setBackgroundResource(R.drawable.epm);
        }else if(manipolo_el==2){
            set_manipolo.setBackgroundResource(R.drawable.epmg);
        }


        set_manipolo.setText(" ");
        potenza = 0;
        potenza = 0;
        prefsEditor.putInt("Potenza", 0);
        prefsEditor.commit();
        pacchetto_da_inviare("1F", "00", 0);

        vol_audio = prefs.getInt("Volume", 1);

        prefsEditor.commit();
        updateBarHandler = new Handler();


        text1=(TextView)findViewById(R.id.count_down);
        final int tempo_count = tempo_sel * 60000;
        counter= new Operativa_elettroporazione_db.MyCount(tempo_count, 1000);
        counter.start();

        Button min = (Button) findViewById(R.id.min);
        Button add = (Button) findViewById(R.id.add);
        textView.setText("0 %");

        /*BOTTONE STOP */
        Button stop = (Button) findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop();
            }
        });

        venticinque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                potenza = 25;
                textView.setText(25 + " %");
                hex = Integer.toHexString(potenza).toUpperCase();
                pacchetto_da_inviare("1F", hex+"", 0);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", 25);
                prefsEditor.commit();
            }
        });

        cinquanta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                potenza = 50;
                textView.setText(50 + " %");
                hex = Integer.toHexString(potenza).toUpperCase();
                pacchetto_da_inviare("1F", hex+"", 0);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", 50);
                prefsEditor.commit();
            }
        });

        settantacinque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                potenza = 75;
                textView.setText(75 + " %");
                hex = Integer.toHexString(potenza).toUpperCase();
                pacchetto_da_inviare("1F", hex+"", 0);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", 75);
                prefsEditor.commit();
            }
        });


        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (potenza <= 0) {
                    potenza = 0;
                    textView.setText("0 %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", potenza);
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    prefsEditor.commit();
                    pacchetto_da_inviare("1F", "00", 0);
                } else if (1 < potenza && potenza < 10) {
                    System.out.println(potenza);
                    potenza = potenza - 1;
                    textView.setText(potenza + " %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", potenza);
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    prefsEditor.commit();
                    hex = Integer.toHexString(potenza).toUpperCase();
                    pacchetto_da_inviare("1F", hex, 0);
                } else {
                    potenza = potenza - 1;
                    textView.setText(potenza + " %");
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", potenza);
                    prefsEditor.commit();
                    hex = Integer.toHexString(potenza).toUpperCase();
                    pacchetto_da_inviare("1F", hex, 0);
                }
            }
        });

        /* GESTIONE CLICK CORTO-LUNGO PULSANTE PIU' --- corto +1 --- lungo +5* */

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (1 < potenza && potenza < 10) {
                    System.out.println(potenza);
                    potenza = potenza + 1;
                    textView.setText(potenza + " %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", potenza);
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    prefsEditor.commit();
                    hex = Integer.toHexString(potenza).toUpperCase();
                    pacchetto_da_inviare("1F", hex, 0);
                }else if (potenza >= 100){
                    potenza = 100;
                    textView.setText(100 + " %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", 100);
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    prefsEditor.commit();
                    hex = Integer.toHexString(potenza).toUpperCase();
                    pacchetto_da_inviare("1F", hex, 0);
                }else {
                    potenza = potenza + 1;
                    textView.setText(potenza + " %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", potenza);
                    String toSpeak = getString(R.string.pottratt) + potenza + getString(R.string.percento);
                    System.out.println(toSpeak);
                    prefsEditor.commit();
                    hex = Integer.toHexString(potenza).toUpperCase();
                    pacchetto_da_inviare("1F", hex, 0);
                }
            }
        });


    }

    /******************************************SERIALE *******************************************/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onPause in");
        super.onPause();
        Log.i(TAG, "==>onPause out");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onStop in");
        super.onStop();
        Log.i(TAG, "==>onStop out");
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.i(TAG, "==>onDestroy in");
        closeSerialPort();
        super.onDestroy();
        Log.i(TAG, "==>onDestroy out");
    }

    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " + mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size > 0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (int k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println(TAG + " LEGGO da thread: " + to_check);
                        analizza_pacchetto(to_check, size);

                    } catch (IOException e) {

                    }
                }
            }

        }
    }

    public void analizza_pacchetto(String msg, int size){
        final String mes = msg;

        if(msg.contains("3E 01 FE 52 02 44 ")){

            this.runOnUiThread(new Runnable() {
                public void run() {
                    final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                    String c = mes.substring(18,20);
                    System.out.println("!!! POTENZA !!! = " + Integer.parseInt(c,16));
                    int num = Integer.parseInt(c,16);
                    textView.setText(num + " %");
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putInt("Potenza", num);
                    prefsEditor.commit();
                }
            });
        }
    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    Log.d("VELVETERROR", e.getMessage());
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {
        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            try {
                Thread.sleep(30);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }
    }

    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }


    /*****************************************************************/
    /************* CREA PACCHETTO DA INVIARE ************************/
    /***************************************************************/
    public String pacchetto_da_inviare( String reg, String mess, int comando){
        String data_to_send = null;  // pacchetto sul quale viene calcolato il crc16
        int to_do = comando; //scrivi o leggi registro
        String start = "3E";
        String stop = "3C";
        String mittente = "FE";
        String destinatario = "01";
        String azione = null; //viene calcolata in base al valore di to_do -> in base al comando e gli viene asseganto uno dei 4 valori sottostanti (scrivi, leggi, avvia o ferma)
        String scrivi ="51";
        String leggi = "52";
        String avvia = "53";
        String ferma = "54";
        String registro = reg; //registro sul quale vado a scrivere
        String messaggio = mess; // messaggio che voglio andare a scrivere
        String crc16 = null; // crc16 calcolato su data_to_send
        String stringa_zero = "-00"; // stringhe di 00 per completare i pacchetti/
        String pacchettocompleto = null; //pacchetto da dare in pasto a SendMsg()
        String msg_tot;

        /*calcola i byte validi */
        int lung_mess = messaggio.length();
        int byte_val = 0;
        if (lung_mess/2 == 4) {
            byte_val = 2;
        }else{
            byte_val = 2;
        }

        int lung_mess1 = messaggio.length();
        if (lung_mess1 == 2) {
            msg_tot = messaggio;
            System.out.println("CHE HAI????????????????????????????" + msg_tot);
        }else{
            msg_tot = "0"+messaggio;
            System.out.println("CHE HAI????????????????????????????" + msg_tot);
        }

        String byteUtilizzabili = "0"+ byte_val; // bite validi in versione "pacchetto"

        //calcola l'azione da scrivere nel pacchetto
        if (to_do == 0){
            azione = scrivi;
        }else if (to_do == 1){
            azione = leggi;
        }else if (to_do == 2){
            azione = avvia;
        }else if (to_do == 3){
            azione = ferma;
        }


        //numero di byte 00 per completare il pacchetto
        int bitZero = 15;
        //System.out.println("bit 0 ========== " + bitZero);

        for (int i = 1; i < (bitZero -1); i++ ){
            stringa_zero = stringa_zero + "-00";
        }

        //pacchetto sul quale calcolare il crc16
        data_to_send = start + "-" + mittente + "-" + destinatario + "-" + azione + "-" + byteUtilizzabili + "-" + registro + "-" + msg_tot + stringa_zero;

        //valore del crc16
        crc16 = preparaArray(data_to_send);

        System.out.println("stringa = ********************************* " + data_to_send);

        // pacchetto completo da dare in pasto a SendMsg()
        pacchettocompleto = data_to_send + "-" +crc16 + "-" +stop;
        System.out.println("!!!!!!!!!!!!!  - " +pacchettocompleto);

        try {
            Thread.sleep(100);
            System.out.print("Sto inviando: "+pacchettocompleto+ "\n");
            SendMsg(pacchettocompleto);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return pacchettocompleto;
    }

    //private static final int POLYNOMIAL   = 0x1021;
    //private static final int PRESET_VALUE = 0x0000;

    public String preparaArray(String pacchetto){
        //String start = "3E-FE-01-51-02-2C-2C-00-00-00-00-00-00-00-00-00-00-00-00-00-00-D0-54-3C";
        String n1 = pacchetto;
        byte [] array = new byte[21];

        //tolgo i trattini dal pacchetto
        int j=0;
        for(int i = 0; i<= n1.length(); i=i+3){
            array[j] = (byte)Integer.parseInt(n1.substring(i,i+2), 16);
            j++;
        }
        byte[] byteStr = new byte[8];
        int crcRes = calculate_crc(array);
        System.out.println(Integer.toHexString(crcRes));
        byteStr[0] = (byte) ((crcRes & 0x00ff));
        byteStr[1] = (byte) ((crcRes & 0xff00) >>> 8);
        System.out.println(Integer.toHexString(byteStr[0] & 0xFF) + " è giusto 0?");
        System.out.println(Integer.toHexString(byteStr[1] & 0xFF) + " è giusto 1?");

        // se il valore del byte è dispari aggiungo uno 0 davanti. Questo problema si presenta solo se il
        // primo numero è 0 in quanto non le lo scrive

        String bLow= Integer.toHexString(byteStr[0] & 0x00ff);
        if (bLow.length()==1){
            bLow = "0"+bLow;
        }else{
            bLow = bLow;
        }
        String bHigh= Integer.toHexString(byteStr[1]& 0xFF);
        if (bHigh.length()==1){
            bHigh = "0"+bHigh;
        }else{
            bHigh = bHigh;
        }
        System.out.printf("%02X\n%02X", byteStr[0],byteStr[1]);
        System.out.printf("\n%02X ", byteStr[1]);

        // crc16 completo in versione stringa
        String crc16 = bLow+"-"+bHigh;
        System.out.println(bLow + " è giusto anch questo ? \n");
        System.out.println(bHigh + " è giusto anch questo 4444? \n");
        System.out.println(crc16.toUpperCase() + " CRC 16 FINITO ho fatto bene");

        //di default i caratteri sono in munuscolo
        return crc16.toUpperCase();
    }

    int calculate_crc(byte[] bytes) {
        int i;
        int crc_value = 0;
        for (int len = 0; len < bytes.length; len++) {
            for (i = 0x80; i != 0; i >>= 1) {
                if ((crc_value & 0x8000) != 0) {
                    crc_value = (crc_value << 1) ^ 0x1021;  //polinomio 1021
                } else {
                    crc_value = crc_value << 1;
                }
                if ((bytes[len] & i) != 0) {
                    crc_value ^= 0x1021;
                }
            }
        }
        return crc_value;
    }


    /***********************SERIALE ********************************/


    public class MyCount extends CountDownTimer
    {
        public MyCount(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
            hex = Integer.toHexString(potenza).toUpperCase();
            pacchetto_da_inviare("1F", hex, 0);
        }
        @Override     public void onFinish()
        {
            text1.setText("00:00");
            stop1();
        }
        @Override     public void onTick(long millisUntilFinished)
        {
            s1=millisUntilFinished;
            text1.setText(String.format(FORMAT,
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
        }
    }


    /*private void calcola_pot_dainviare() {
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        int pot = prefs.getInt("Potenza", potenza);
        System.out.println(pot + "a");
        if ( 0 == pot){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 0");
            SendMsg(pot0_el);
        }else if ( pot >=1 && pot<=10 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 1");
            SendMsg(pot1_el);
        }else if ( pot >=  11 && pot <=20 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 2");
            SendMsg(pot2_el);
        }else if ( pot >=  21 && pot <=30 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 3");
            SendMsg(pot3_el);
        }else if ( pot >=  31 && pot <=40 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 4");
            SendMsg(pot4_el);
        }else if ( pot >=  41 && pot <=50){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 5");
            SendMsg(pot5_el);
        }else if ( pot >=  51 && pot <=60 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 6");
            SendMsg(pot6_el);
        }else if ( pot >= 61 && pot <=70 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 7");
            SendMsg(pot7_el);
        }else if ( pot >=  71 && pot <=80 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 8");
            SendMsg(pot8_el);
        }else if ( pot >=  81 && pot <= 90 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 9");
            SendMsg(pot9_el);
        }else if ( pot >=  91 && pot <=100 ){
            try {
                Thread.sleep(100);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            System.out.println(pot + " 10");
            SendMsg(pot10_el);
        }
    }*/


    /* cio che avviene se clicco stop */
    private void stop() {

        pacchetto_da_inviare("1F", "00", 0); // potenza
        counter.cancel();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.pausa);
        dialog.setCancelable(false);
        TextView txt2 = (TextView) dialog.findViewById(R.id.title);
        txt2.setText(R.string.pausa);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.pausa_tratt1);
        Button dismissButton = (Button) dialog.findViewById(R.id.button1);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter= new Operativa_elettroporazione_db.MyCount(s1,1000);
                potenza = 0;
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", 0);
                prefsEditor.commit();
                pacchetto_da_inviare("1F", "00", 0); // potenza
                textView.setText("0 %");
                counter.start();
                dialog.dismiss();
            }
        });
        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
                potenza = 0;
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("Potenza", 0);
                prefsEditor.commit();
                pacchetto_da_inviare("1F", "00", 0); // potenza
                try {
                    Thread.sleep(200);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                SendMsg(stop_elettropora);
                endTime = System.currentTimeMillis();
                final SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
                final SharedPreferences.Editor prefsEditor2 = prefs1.edit();
                tempoTrattamento = (endTime - startTime)/1000;
                tempocontatori = prefs.getLong("CONTATORI EL", 0);
                update_contatori = tempocontatori + tempoTrattamento;
                prefsEditor2.putLong("CONTATORI EL", update_contatori);
                prefsEditor2.commit();
                contatori();

                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
                counter.cancel();
            }
        });
        dialog.show();
    }


    /* cio che avviene allo scadere del tempo */
    public void stop1(){
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences("Shared", MODE_PRIVATE);
        potenza = 0;
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putInt("Potenza", 0);
        prefsEditor.commit();
        pacchetto_da_inviare("1F", "00", 0); // potenza
        try {
            Thread.sleep(200);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        SendMsg(stop_elettropora);
        endTime = System.currentTimeMillis();
        final SharedPreferences prefs1 = getApplicationContext().getSharedPreferences("Reserved", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor2 = prefs1.edit();
        tempoTrattamento = (endTime - startTime)/1000;
        tempocontatori = prefs1.getLong("CONTATORI EL", 0);
        update_contatori = tempocontatori + tempoTrattamento;
        prefsEditor2.putLong("CONTATORI EL", update_contatori);
        prefsEditor2.commit();
        contatori();
        bip();
        final Dialog dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.stop);
        dialog.setTitle(R.string.fine_tratt);
        dialog.setCancelable(false);
        TextView txt1 = (TextView) dialog.findViewById(R.id.text);
        txt1.setText(R.string.fine_tratt1);

        Button continueButton = (Button) dialog.findViewById(R.id.button2);
        continueButton.setText(R.string.fine_tratt3);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Context context = getBaseContext();
                Intent CauseNelleVic = new Intent(context, MenuPrinc.class);
                startActivityForResult(CauseNelleVic, 0);
            }
        });
        dialog.show();
    }

    public void contatori(){
        endTime = System.currentTimeMillis();
        long millisDiff = endTime - startTime;
        String SharedPrefName = "Reserved";
        final SharedPreferences reserved = getSharedPreferences(SharedPrefName, 0);
        SharedPreferences.Editor prefsEditor = reserved.edit();
        prefsEditor.putLong("Tempo parziale el", millisDiff);
        prefsEditor.commit();
        int seconds = (int) (millisDiff / 1000 % 60);
        int minutes = (int) (millisDiff / 60000 % 60);
        int hours = (int) (millisDiff / 3600000 % 24);
        int days = (int) (millisDiff / 86400000);

        Long tempoOLD = reserved.getLong("TEMPO UTILIZZO ELETTROPORAZIONE", 0);
        Long tempoTOT = tempoOLD + millisDiff;
        prefsEditor.putLong("TEMPO UTILIZZO ELETTROPORAZIONE", tempoTOT);
        prefsEditor.commit();

        System.out.println(days + " days, ");
        System.out.println(hours + " hours, ");
        System.out.println(minutes + " minutes, ");
        System.out.println(seconds + " seconds");
        int seconds1 = (int) (tempoTrattamento % 60);
        int minutes1 = (int) (tempoTrattamento / 60);
        String tolog1 = minutes1 + " M " + seconds1 + "S";
        tolog = nome_trattamento_db + "Tempo= " + tolog1;
        log.Log(tolog, this);
    }

    public void bip(){

        spool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        spoolres = spool.load(this, R.raw.fine_trattamento, 0);
        spoolId = spool.load(this, R.raw.fine_trattamento, 0);
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float f = actualVolume / maxVolume;
        System.out.println("OOOOKKKKK " + f);
        spool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool paramAnonymousSoundPool, int paramAnonymousInt1, int paramAnonymousInt2) {
                spool.play(spoolId, f, f, 1, 0, 1);
            }
        });
        spool.play(spoolId, f, f, 1, 0, 1);
    }


    private void initializeVariables() {
        textView = (TextView) findViewById(R.id.textView1);
    }

    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch(id) {
            case CONFIRM_DIALOG:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.pausa);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.continuare, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        counter= new Operativa_elettroporazione_db.MyCount(s1,1000);
                        counter.start();
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton(R.string.esci, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(getApplicationContext(), MenuPrinc.class);
                        startActivity(i);
                        finish();
                    }
                });
                dialog = builder.create();
                break;
            default:
                dialog = null;
        }
        return dialog;
    }


    /*funzioni fisse per tutti */
    public void back(View view) {
        finish();
    }

    public void informazioni(View view) {
        counter.cancel();
        Context context = getBaseContext();
        Intent CauseNelleVic = new Intent(context, Informazioni.class);
        startActivityForResult(CauseNelleVic, 0);
    }


}
