package com.topquality.velvetbite;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

//import com.dwin.navy.serialportapi.SerialPortOpt;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.LinkedList;

import android_serialport_api.SerialPort;


public class Test extends Activity {

    Boolean test_res = false;
    public int k = 0;
    public Integer[] tmp = {-1, -1, -1, -1};
    int pass;

    private static final String TAG = "MainActivity";
    String msg = null;
    protected OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;
    private LinkedList<byte[]> byteLinkedList = new LinkedList<byte[]>();

    private boolean m_bShowDateType = false;
    private SerialPort mSerialPort = null;


    public SerialPort getSerialPort() throws SecurityException, IOException {
        if (mSerialPort == null) {
            /* Read serial port parameters */
            SharedPreferences sp = getSharedPreferences("android_serialport_api.sample_preferences", MODE_PRIVATE);
            String path = "/dev/ttymxc0"; //sp.getString("DEVICE", "");
            int baudrate = 57600; //Integer.decode(sp.getString("BAUDRATE", "-1"));

            /* Check parameters */
            if ((path.length() == 0) || (baudrate == -1)) {
                throw new InvalidParameterException();
            }

            /* Open the serial port */
            mSerialPort = new SerialPort(new File(path), baudrate, 0);

        }
        Log.i("uart port operate", "Mainactivity.java==>start ReadThread");
        mReadThread = new Test.ReadThread();
        mReadThread.start();
        Log.i("uart port operate", "Mainactivity.java==>ReadThread started");
        return mSerialPort;
    }

    public void closeSerialPort() {
        if (mReadThread != null) {
            mReadThread.interrupt();
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
    }


    TextView testo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hardceck);
        testo = findViewById(R.id.title);
        testo.setText("...");
        try {
            mSerialPort = getSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ck_com();
        avvia_macchina();
        Button stop = (Button) findViewById(R.id.rf);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                avvia_macchina();
            }
        });
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }


    private class ReadThread extends Thread {
        byte[] buf_read = new byte[24];

        @Override
        public void run() {
            super.run();
            Log.i(TAG, "ReadThread==>buffer:" + buf_read.length + " .,.,.,.,. " +mInputStream);
            String to_check = "";
            int size = buf_read.length;
            while (!isInterrupted()) {
                if (mInputStream == null)
                    return;
                if (size>0) {
                    try {
                        mInputStream.read(buf_read);
                        String stampa = new String(buf_read);
                        System.out.println(stampa);
                        for (k = 0; k < buf_read.length; k++) {
                            System.out.println(buf_read[k]);
                        }
                        to_check = bytesToHexString(buf_read, 24);
                        System.out.println("LEGGO da thread - YEAH: " + to_check + " size" + size);
                        //hard_ck(to_check);
                        analizza_pacchetto(size);
                    } catch (IOException e) {

                    }
                }
            }

        }

        public void hard_ck(String stringa) {
            if (stringa.length() == 0) {
                test_res = true;
            }else{
                test_res = false;
            }
        }


    }

    protected void onDataReceived() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (msg != null) {
                    if (!byteLinkedList.isEmpty()) {
                        byte[] buf = byteLinkedList.poll();
                        int size = buf.length;
                        if (m_bShowDateType) {
                            msg = "";
                            String msg1 = msg + (new String(buf, 0, size));
                            Log.i("eTextShowMsg ASIIC",
                                    new String(buf, 0, size));
                            System.out.println("MESSAGGIIO " + msg1);
                            //ck = ck + 1;
                        } else {
                            //eTextShowMsg.append(bytesToHexString(buf, size));
                            Log.i("eTextShowMsg HEX",
                                    bytesToHexString(buf, size));
                        }
                    }
                }
            }
        });
    }

    public static String bytesToHexString(byte[] src, int size) {
        String ret = "";
        if (src == null || size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            String hex = Integer.toHexString(src[i] & 0xFF);
            // String hex = String.format("%02x", src[i] & 0xFF);
            //Log.i("bytesToHexString", hex);
            if (hex.length() < 2) {
                hex = "0" + hex;
            }
            hex += " ";
            ret += hex;
        }
        return ret.toUpperCase();
    }

    private class SendThread extends Thread {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "SendThread==>run");
            super.run();
            //SendMsg();
        }
    }

    private class TimerSendThread extends Thread {

        private long m_lTimer = 100; // default 100ms
        private boolean m_bRunFlag = true;

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Log.i(TAG, "TimerSendThread==>run");
            super.run();
            while (m_bRunFlag) {
                Log.i(TAG, "TimerSendThread==>" + m_lTimer);
                //SendMsg();
                if (m_lTimer <= 0) { // must over 0ms
                    m_lTimer = 100;
                }
                try {
                    Thread.sleep(m_lTimer);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        public void setSleepTimer(long timer) {
            m_lTimer = timer;
        }

        public void stopThread() {
            m_bRunFlag = false;
        }
    }

    private void SendMsg(String mess) {
        OutputStream outputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();

        String n1 = mess;
        byte[] array = new byte[24];
        int j = 0;
        for (int i = 0; i <= n1.length(); i = i + 3) {
            array[j] = (byte) Integer.parseInt(n1.substring(i, i + 2), 16);
            j++;
        }

        try
        {
            try {
                Thread.sleep(30);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            outputStream.write(array);
            outputStream.flush();
            System.out.println(TAG + "ho inviato il pacchetto: " + mess);

        }
        catch (IOException e)
        {

        }
    }


    public static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}))
                .byteValue();
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}))
                .byteValue();
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    public static byte[] HexString2Bytes(String src) {
        byte[] ret = new byte[src.length() / 2];
        byte[] tmp = src.getBytes();
        for (int i = 0; i < tmp.length / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    /***********************
     * SERIALE
     **********************/

    public int ck_com(){
        int t = 0;
        SendMsg("3E-FE-01-51-02-4D-01-00-00-00-00-00-00-00-00-00-00-00-00-00-00-2B-E7-3C");
        try {
            Thread.sleep(150);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        if (pass == 1){
            t = 1;
        }else {
            t = 0;
        }
        System.out.println("TEST=" + t);
        return t;
    }

    public void analizza_pacchetto(int size){
        pass = 0;

        if(size > 0){
            pass = 1;
            this.runOnUiThread(new Runnable() {
                public void run() {
                    //pass = 1;
                }
            });

        }else{
            pass = 0;
            this.runOnUiThread(new Runnable() {
                public void run() {
                    //pass = 0;
                }
            });
        }
    }


    public void avvia_macchina() {
        int pass = ck_com();


        if(pass == 1){

            this.runOnUiThread(new Runnable() {
                public void run() {
                    testo.setText("OK");
                }
            });

        }else {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    testo.setText("KO");
                }
            });
        }

    }


}